\documentclass[11pt]{article}
\usepackage[spanish]{babel} % Español
\usepackage[utf8]{inputenc} % Codificacion de caracteres
\usepackage{graphicx} % graficos
\usepackage{listings} % Codigo y texto en bloque preformateado


\title{\textbf{Informe de Pasantía: MODELOS Y ALGORITMOS DE BÚSQUEDA + REDES SOCIALES PARA APLICACIONES VERTICALES DE RECUPERACIÓN DE INFORMACIÓN}}
\author{Tomas Delvechio (Pasante)
\and
Gabriel Tolosa (Director del proyecto)}
\date{}
\usepackage{graphicx}
\begin{document}

\maketitle

\section{Introducción}

El imparable crecimiento de datos no estructurados plantea el desafío de disponer de esquemas de procesamiento masivos a costos razonables y con prestaciones adecuadas. Soluciones especificas para llevar a cabo el procesamiento de este tipo de información pueden servir durante una etapa inicial, pero cuando el volumen de información supere los limites de forma continua, se plantea un compromiso entre la cantidad de datos disponibles y la cantidad que efectivamente pueden ser procesados, limitados por la infraestructura disponible y los algoritmos elegidos.

Una de las áreas que aborda estos problemas es la de Big Data, cuyo  modelo de desarrollo que se ha establecido como estándar de facto es \mbox{MapReduce}. El software que implementa dicho modelo y mas adopción a tenido por parte de la industria y la academia es Hadoop. MapReduce permite el tratamiento de grandes cantidades de datos en clusters de Hardware dedicado y "barato" (commodity hardware), haciendo uso de conceptos derivados de la programación funcional (map y reduce) y estrategias de paralelización y distribución de carga de trabajo.

Una de las áreas donde la cantidad de datos no estructurados a crecido de forma acelerada es la recuperación de información relacionada con la Web. En consecuencia, se adaptaron los procesos de IR para que trabajen de forma masivamente distribuida. MapReduce\cite{Dean2004} es una respuesta a esto desde hace una década.

En un esquema simplificado, la recuperación de información en la web tiene 3 etapas: Recolección de la información o crawling, Indexado de los datos recolectados y recuperación de información del índice para responder a consultas\cite{Lin2010}.

En la etapa de indexación específicamente, se construye una estructura conocida como Índice Invertido, cuya organización básica esta compuesta por cada termino de la colección como clave, y tiene asociado una lista de documentos donde dicho termino aparece, conocida como lista de posteos o posting list.

El objetivo del presente trabajo es abordar la problemática de la indexación distribuida mediante Hadoop. Se formularan algoritmos necesarios para lograrla y se explicaran los pasos y configuración de un cluster para poder realizar la indexación. Además se discutirán cuestiones relacionadas al modelo Map Reduce y se explicaran las decisiones tomadas al momento de definir los parámetros principales de una aplicación Hadoop.

A partir de lo planteado anteriormente, se realizaran lecturas y experimentación en torno a las siguientes problemáticas: 

\begin{itemize}
	\item Sistemas Distribuidos
	\item Indexación de documentos no estructurados
	\item Big Data
	\item Indexación distribuida
	\item Algoritmos MapReduce
\end{itemize}

\section{Relevamiento Bibliográfico}

En esta sección se desarrollan las lecturas que se realizaron para entender la problemática planteada y explorar soluciones a dichos problemas. Se trabajaron 6 documentos de las áreas de Big Data y Indexación de colecciones.

\subsection{Faster and Smaller Inverted Indices with Treaps\cite{Konow2013}}

El artículo presenta una estructura de Índice Invertido, el Treap que ayuda a realizar de forma eficiente Uniones e intersecciones de postings utilizando menos espacio que las soluciones previas conocidas. El Treap permite realizar intersecciones/mezclas de id de documentos, a la vez que se establece un umbral de frecuencia. Como estructura, Treap combina las ventajas de un árbol binario de búsqueda (para los DocId) y un Heap binario (para los pesos o frecuencias).

Un Treap es un árbol binario que representa simultáneamente los DocId en orden si lo recorremos de izquierda a derecha (soportando intersecciones booleanas rápidas), y por el peso de los términos si recorremos de arriba hacia abajo (que soporta un umbral de resultados en simultáneo con el proceso de intersección). Usando esta estructura, es posible obtener el top-k de resultados sin la necesidad de realizar completamente la unión/intersección booleana. Otra ventaja de Treap es que permite una codificación separada entre id doc y pesos, lo que lo vuelve un representación muy eficiente desde el punto de vista del espacio que requiere la estructura para ser almacenada.

El articulo aporta una nueva representación de postings list en memoria que, por un lado, logra una mejor comprensión porque permite una codificación diferencial de DocId y frecuencia, y por otro lado, realiza un ranking exacto en intersecciones y uniones directa y nativamente sin tener que hacer primero una intersección/unión y luego el ranking.

Los alcances de los aportes mencionados por los autores son:

\begin{itemize}
	\item Cuando interesa un conjunto limitado de resultados (top-k).
	\item Sistemas distribuidos donde cada nodo contribuye de forma parcial al resultado final.
	\item Consultas Booleanas.
\end{itemize}

Conclusiones a las que arriba el articulo: Treap es una elegante y flexible herramienta para representar posting lists ordenadas de forma simultánea por frecuencia y por DocId. Se utilizó para diseñar algoritmos eficientes en ranking de uniones e intersecciones, que filtran de forma simultánea por DocId y frecuencia. También se representaron dichos valores en forma diferencial, contribuyendo a una mejor comprensión de las postings lists.

Las pruebas mostraron una significativa mejora en el espacio usado y en el tiempo, respecto al estado del arte:

\begin{itemize}
	\item De 13 a 18\% menos de espacio que las mejores estructuras previas.
	\item Mayor rapidez (incluso del doble) para el top-20 en intersecciones y para el top-100 en uniones.
\end{itemize}

\subsection{Parallel Generation of Inverted Files for Distributed Text Collections\cite{Ribeiro-Neto1998}}

El articulo aborda la problemática de generar Archivos invertidos (IF) en un contexto distribuido, es decir, donde la colección se encuentra distribuida a lo largo de un cluster de computadoras.

El objetivo es lograr un índice invertido que pueda mantenerse en memoria principal (distribuida en el cluster también), por lo que la compresión de las posting es un requerimiento.

Además, esto permite ahorrar ancho de banda si una posting debe transferirse para resolver una consulta.

Se plantea la generación de un índice invertido que se construya en forma distribuida, con un algoritmo que permita a cada nodo del cluster generar la parte correspondiente a la colección que posea localmente. El articulo plantea además que un conjunto de nodos relativamente económicos conectados mediante una red con ancho de banda alto puede competir con un supercomputador paralelo en rendimiento.

Asume que los documentos se encuentran distribuidos en los nodos de forma previa.

El uso de memoria principal responde al hecho de que se puede mejorar la velocidad de la aplicación usando Memoria + Red que usando I/O al almacenamiento secundario. Lo único que se lee son los documentos al inicio, y se escribe el índice al final del proceso.

\subsection{Efficient Distributed Algorithms to Build Inverted Files\cite{Ribeiro-Neto1999}}

El articulo se propone desarrollar estrategias para la construcción de índices que ya no puedan ser mantenidos en la memoria principal y deban necesariamente ser grabados en almacenamiento secundario, lo que en \cite{Ribeiro-Neto1998} simplificaba el modelo.

Como en el articulo anterior, se asume que el índice no es generado de manera incremental cuando se encuentren actualizaciones en la colección. En cambio, el proceso regenera completamente el índice incluyendo los nuevos documentos mediante un proceso batch.

La propuesta del articulo son 3 métodos de indexación nuevos, llamados LL, LR y RR. Los algoritmos son escalables, y trabajan de forma distribuida en la generación de índice basado en disco (no mantienen el índice en memoria principal).

Además el índice generado se comprime para ahorrar espacio en disco. Además el índice se genera mas rápido y esto ayuda a mejorar la performance cuando los datos son transmitidos a través de la red.

La metodología seguida fue la siguiente. Usando el algoritmo secuencial clásico de Indexación Invertida\cite{Witten1999}, establecieron que estructuras podían ser distribuidas, y probaron combinaciones entre ellas. Los algoritmos desarrollados fueron los siguientes:

\begin{itemize}
	\item (LL) Local Buffer and Local Lists.
	\item (LR) Local Buffer and Remote Lists.
	\item (RR) Remote Buffer and Remote Lists.
\end{itemize}

Las diferencias principales entre los algoritmos se encuentran en la localización de los datos parciales y las postings generadas. Cada enfoque tiene su trade-off en base a lo que se pierde por el uso de I/O o transferencia de datos a través de la red.

\subsection{Faster top-k document retrieval using block-max indexes\cite{Ding2011}}

El articulo presenta una nueva estructura de índice invertido que tiene por objetivo mejorar el rendimiento en algoritmos de Early-Termination (ET).

Estos algoritmos plantean dividir la recuperación en dos etapas, una primera con un conjunto de resultados pequeño y rápido, y mientras el usuario consulta dicho primeros resultados, realizar una segunda etapa de procesamiento que sea exhaustiva respecto a la consulta planteada al motor de búsqueda. Los algoritmos de ET resuelven la primer etapa planteada, y la velocidad y precisión con la que realicen la recuperación es el principal motivo de estudio.

Los autores proponen Block-Max Indexes, una estructura que se integra con el enfoque WAND\cite{Broder2003}, aportando mejoras en el rendimiento de consultas disyuntivas.

Las propiedades de este índice son:

\begin{itemize}
	\item Se integra al enfoque WAND agregando estructuras simples al índice.
	\item Recorrido de índice DAAT.
	\item Postings ordenadas por DocID, y estructura de capas en base al Impact Score
	\item División de listas en bloques.
	\item Early-Termination 
\end{itemize}
	
Las postings lists son divididas en bloques, los cuales contienen postings. Cada bloque también guarda información sobre el mayor impact score del bloque. WAND exige guardar el mayor impact score por posting list. Ese valor también se conserva.

Como se menciono previamente, este índice tiene la propiedad de ser ET seguro, lo que implica que para los resultados devueltos, el orden, score y conjuntos de documentos se corresponden con el baseline.

En lo que respecta a la estructura, el cambio respecto al algoritmo de WAND es que se agrega una estructura adicional que contiene, para cada bloque, el Impact Score correspondiente. La unidad para compresión es a nivel de bloque, pero no se necesita descomprimir cada uno cuando se realiza la recuperación, sino que la misma se puede realizar evaluando el Impact Score del bloque, a partir de la cual se puede descartarlo completamente o determinar que contiene postings que se encuentran en el top-k y recién ahí es descomprimido el bloque.

Los principales aportes del articulo son:

\begin{itemize}
	\item Modificación del índice planteado en \cite{Broder2003}, con un ligero incremento en el tamaño del índice debido a la estructura adicional, el Block-Max Index. Esta estructura siga manteniendo un enfoque WAND y permite un ET seguro.
	\item Permite índice por capa, reorganización de índice y procesamiento de consultas conjuntiva.
	\item Se mejora las mejores técnicas conocidas al momento de redacción del articulo.
\end{itemize}

Las pruebas de mejoras sobre la evaluación de consultas exceden el marco de este resumen, donde se enfoco el análisis del articulo en aquellas cuestiones relacionadas con la construcción del Índice Invertido.

\subsection{Comparing Distributed Indexing: To MapReduce or Not?\cite{Mccreadie2009}}

El articulo propone el estudio de estrategias de indexación distribuida con MapReduce. Para ello, los autores construyen 3 implementaciones de Índice. La primera es una interpretación del articulo original de MapReduce\cite{Dean2004} (MR), la segunda es la implementación de Nutch y la tercera es una propuesta del estudio.

La motivación del articulo esta en relación al gran crecimiento de la información en escala masiva en la web. A partir de esta realidad, contar con métodos eficientes de indexación recobra importancia.

El aporte del articulo es un estudio de estrategias de indexado con MapReduce que mejora las soluciones existentes, distribuido sobre la plataforma Terrier y usando Hadoop como motor MapReduce.

Los tópicos que fundamentan el estudio son Estructuras de indexado; Indexación de paso único e Indexación distribuida (MapReduce en particular). Como ya se menciono, 3 estrategias de indexación con MapReduce son estudiadas:

\textbf{Dean \& Ghemawat's MR Strategy}: Es la estrategia propuesta por los autores originales de la propuesta de MR. El articulo plantea una propuesta de pseudo-codigo de los metodos Map y Reduce. Ver Figura ~\ref{fig:dgStrategy}. 

\begin{figure}[htp]
\includegraphics[width=15cm]{img/dgStrategy.png}
\caption{Indexacion con Dean \& Ghemawat's strategy}
\label{fig:dgStrategy}
\end{figure}

\textbf{Nutch's MR Strategy}: La estrategia de este motor de indexado, que utiliza Hadoop como plataforma de distribución de procesamiento, difiere de la comentada anteriormente. Un estudio del código de Nutch en su versión 9, muestra que la salida del método Map es un par <Doc-Id,Documento-Procesado>. La estructura Documento-Procesado contiene los términos del documento con su frecuencia correspondiente. La fase Reduce es la encargada de construir todo el índice final. La estrategia comentada reduce la cantidad de datos a emitir la salida intermedia de los maps, pero tiene como contrapartida que cada map tiene una salida mayor. 

\textbf{Single-pass MR Strategy}: Esta es la propuesta de los autores del articulo. Se centra en el hecho de que como cada tarea map procesa un subconjunto de documentos, se puede retrasar la emisión de pares clave-valor hasta que la memoria este llena. Ver Figura ~\ref{fig:singlePassStrategy}.

\begin{figure}[htp]
\centering
\includegraphics[width=15cm]{img/singlePassStrategy.png}
\caption{Indexacion con Single Pass strategy}
\label{fig:singlePassStrategy}
\end{figure}

Basado en el concepto original de Dean \& Ghemawat, los autores proponen un proceso de indexación donde la clave de los maps sera el termino, pero el valor de los mismo no serán Identificadores de documento, sino posting list parciales. El trabajo que resta realizar en el proceso de reduce consta de realizar la mezcla de las posting parciales en las posting definitivas.

Para la estrategia de indexación Single-Pass, los autores realizan también una implementación centralizada. Los experimentos realizados mostraron las ventajas que ofrece la distribución del contenido previo al procesamiento mediante HDFS.

Por otro lado implementan la estrategia de Dean \& Ghemawat y encuentran que la misma emite demasiados datos a la salida del map, lo que provoca que el proceso de indexado se retrase. Por ultimo realizan un estudio de como evoluciona el procesamiento en la medida que mas tareas map y reduces son agregadas.

\subsection{The Unreasonable Effectiveness of Data\cite{Halevy2009}}

El articulo plantea el problema que tienen disciplinas complejas, como aquellas relacionadas con las ciencias que involucran seres humanos, tienen tantos problemas para plantear teorías elegantes y sencillas. Por ejemplo, la física posee expresiones sencillas y directas como f = m a, pero en cambio, áreas como reconocimiento de voz humana necesiten algoritmos sumamente complejos para abordar las soluciones.

El articulo es una opinión de expertos y no una investigación, pero plantea un cambio de enfoque para los problemas que pueden parecer complicados de abordar cuando los modelos teóricos se vuelven complejos. En muchos de esos casos, un enfoque basado en la recopilación de una gran cantidad de datos, seguido de análisis relativamente sencillos de carácter estadísticos proporcionaron iguales o mejores resultados que modelos mas complejos con una cantidad modesta de ejemplos en los conjuntos de datos de muestra.

La hipótesis es que la complejidad del problema esta expresada en la gran cantidad de ejemplos diferentes de los datos, haciendo mas sencillo o innecesario que sean los algoritmos los que expresen estos casos. En palabras del articulo "... Invariablemente, modelos simples con una gran cantidad de datos triunfan sobre elaborados modelos basados en menos datos".

Dependiendo del área, la disponibilidad de cantidades de datos de entrenamiento puede ser un problema. Además, la hipótesis posteriormente sera relativizada, debido a que hay casos donde puede funcionar mejor un modelo complejo pero ya conocido, en cuyo caso un esquema híbrido de muchos datos y un modelo complejo combinados son un enfoque mas conveniente que el uso por separado de alguna técnica descartando la otra.

Por ultimo, sugiere que se planteen representaciones que puedan usar aprendizaje no supervisado en datos no estructurados cuando estos son mayoría (Como es el caso de la web), representar los datos con modelos no paramétricos, debido a que en grandes conjuntos de datos son estos los que contienen gran cantidad de detalles.

\section{Experimentación}

En la primer parte del proyecto se trabajo en la construcción de la infraestructura para poder desarrollar las pruebas, cuyas subtareas fueron:

\begin{enumerate}
	\item Construcción de un dataset para pruebas
	\item Construcción de un cluster para ejecutar los algoritmos
\end{enumerate}

Luego se trabajo en la construcción de un Indexador básico para comprobar experimentalmente el trabajo, y por ultimo se realizaron pruebas con dos Indexadores, cuyos resultados se muestran en un trabajo adjunto a este.

\subsection{Construcción del conjunto de datos para prueba}

Para la construcción de índices invertidos, se utilizan colecciones de archivos de texto no estructurado, que pueden pertenecer a diversas fuentes.

En particular para los estudios relacionados con la Web, la primera opción para obtener un dataset es solicitar una copia de las colecciones Trec Web\cite{University2011}. Esta colección se encuentra ampliamente divulgada entre los grupos de investigación en IR, y posee la cualidad de ser comparable y tener un formato y contenido conocido y estudiado en la literatura. Sin embargo no siempre es trivial obtener una copia de estas colecciones, debido a que debe ser solicitada por las instituciones y no por particulares.

La segunda opción para armar un dataset de forma rápida es realizar un proceso de crawling propio con algún software preparado para tal efecto o incluso desarrollando scripts de crawling específicos para esta tarea.

El proceso de crawling conlleva resolver todo un conjunto de problemas en si mismos, entre los que se destacan el formato de los datos, el encoding, el saneamiento de html u otro tipos de tags que no forman parte del documento. El desarrollo de scripts propios es desaconsejado por sobre la utilización de una herramienta preparada y probada de forma previa.

Aun en el caso de usar alguna herramienta pensada para la recuperación de sitios webs de forma automática, el proceso de crawling no siempre es repetible y comparable, debido a que los sitios web evolucionan (cambian), otros son eliminados de Internet, otros son subidos y linkeados. Entonces, dos crawlers ejecutados en diferentes momentos del tiempo podrían recuperar diferentes datasets, y los resultados entre ambos estudios no serian comparables.

En el caso de tener que realizar un crawling propio es aconsejable ofrecer la colección de prueba completa por algún medio, para poder hacer que los estudios sean repetibles, o al menos realizar estudios básicos sobre las características del Dataset para demostrar que el comportamiento de los datos es el esperable (Cumplimiento de la Ley de Zipf, Ley de Heaps, etc....).

Para comenzar con las pruebas, al no disponer de datasets de gran tamaño, se utilizo una guía ofrecida por el Project Gutenberg\footnote{https://www.gutenberg.org/} para descargar los libros y documentos de texto de su sitio. Cuando se llego a una cantidad de documentos equivalentes a 10Gb de datos, se armo un script ad-hoc que recorre la colección y copia todos los documentos a un archivo con formato conocido como TREC, el cual se detalla a continuación:

\begin{verbatim} 
# Cada documento TREC se codifica con el siguiente formato
<DOC>
<DOCID>NroDocId</DOCID>
...
Content of document
...
</DOC>
\end{verbatim} 

Para simplificar el procesamiento vía Hadoop, a este archivo además se le quito los fin de linea y retorno de carro (caracteres \verb!\r! y \verb!\n!). Con esto se fuerza que cada Task Map en Hadoop procesa un documento sin la necesidad de tener la necesidad de reimplementar la clase InputSplit.

El siguiente paso es lograr dos Aplicaciones MapReduce, que a partir de una colección con el formato anterior, logren construir dos índices respectivamente. Un primer índice llamado Básico donde la posting list de cada termino contiene solo el DocId de los documentos donde aparece. A continuación se construirá un segundo índice en el cual, cada registro de la posting list además de incluir el DocId contendrá la frecuencia del termino en el documento referido.

\subsection{Construcción de Cluster para pruebas}

La tarea de construcción de un cluster tiene por objetivo generar una infraestructura para poder realizar las pruebas de los algoritmos desarrollados. Para las necesidades del presente trabajo, se divide el problema en dos sub tareas:

\begin{itemize}
	\item Armar la infraestructura física.
	\item Instalación y configuración de un cluster Hadoop.
\end{itemize}

\subsubsection{Infraestructura física}

Esta tarea implica conseguir, instalar y conectar un equipo o conjunto de equipos que servirán de base para la instalación del cluster Hadoop y la ejecución de los algoritmos a desarrollar, además de almacenar la información usada como entrada, y los resultados de los procesos ejecutados.

Al momento, se consiguieron 6 equipos informáticos para que actúen de nodos en un cluster, cada uno con las siguientes características:

\begin{verbatim}
Procesador: Intel(R) Core(TM) i5-2400 CPU @ 3.10GHz (4 Cores)
Memoria: 7955MiB (8Gb).
Placa de red: Gbit Ethernet.
\end{verbatim}

Estos equipos se encuentran conectadas mediante el siguiente switch:

\begin{verbatim}
Marca: DLink
Modelo: DGS-3120-24TC-SI
Managed 24-ports Gigabit Stackable L2/L3 Switch, 20-ports Gigabit.
\end{verbatim}

Una vez obtenido el Hardware, se procedió a instalar un Sistema Operativo en cada uno de los nodos. El Sistema Operativo seleccionado fue Ubuntu 14.04.

\subsubsection{Cluster Hadoop}

Una vez obtenido Hardware del cluster e instalado el Sistema Operativo de base, se procedió a realizar una instalación de Hadoop en modo cluster. Uno de los nodos fue seleccionado como Master. Esta selección es arbitraria, debido a que todos los nodos poseen las mismas características. El resto de los nodos son asignados como Workers.

Se generó documentación para la instalación en modo single-node (Para desarrollo de aplicaciones) y para instalación en modo cluster (Para pruebas y producción).

El cluster Hadoop instalado y configurado con los valores por defecto realiza una utilización sub-óptima de los recursos disponibles. Para mejorar esto, se consulto la documentación en linea disponible respecto a la utilización de memoria en los Workers Hadoop. Las variables configuradas inicialmente son las siguientes:

\begin{verbatim}
# Archivo: yarn-site.xml
name: yarn.nodemanager.resource.memory-mb
value: 6144 (En Mb, 6 Gb)
name: yarn.scheduler.minimum-allocation-mb
value: 1536
name: yarn.nodemanager.vmem-pmem-ratio
value: 1.5


# Archivo: mapred-site.xml
name: mapreduce.map.memory.mb
value: 3072
name: mapreduce.reduce.memory.mb
value: 6144
name: mapreduce.map.java.opts
value: -Xmx2048m
name: mapreduce.reduce.java.opts
value: -Xmx4096m
\end{verbatim}

Estas son las variables que la documentación recomienda ajustar para mejorar la utilización de recursos del cluster. En base a estos parámetros, Hadoop determinará de forma automática cuantos Contenedores creara por Worker. Un contenedor es el proceso que ejecutarán posteriormente las tasks Map o Reduce, según Hadoop requiera para cumplir con los objetivos de la Aplicación que se esta ejecutando.

Con la instalación y la configuración comentada, se ejecutaron los ejemplos que Hadoop trae incorporados y se comprobó que todos los nodos workers ejecutaban parte del procesamiento.

\subsection{Construcción del Indexador}

El siguiente paso fue desarrollar una aplicación que logre indexar vía MapReduce una colección con formato TREC. Se decidió implementar un indexador básico, siguiendo la estrategia de indexación propuesta por Dean \& Ghemawat en el articulo original de MapReduce (2004).

Se procuro generalizar funciones que se consideran útiles, haciendo que el resultado de desarrollar el indexador fueran dos paquetes, uno con objetos comunes y reutilizables, y otro con los Mapper y Reducers específicos que implementan la estrategia especifica de Dean \& Ghemawat.

Todos los desarrollos fueron realizados con el lenguaje Java, para poder tener acceso completo a la API de Hadoop y todas las opciones disponibles. La versión utilizada de Hadoop es 2.5.0. El repositorio de código esta públicamente disponible\footnote{https://github.com/tomasdelvechio/YarnExamples} y el código es licenciado bajo GPLv3.

En la versión 1 del Indexador que implementa la estrategia de indexado de Dean \& Ghemawat se incluyeron las siguientes características:

\begin{itemize}
	\item Tratamiento de colecciones TREC.
	\item Generación de índice invertido ordenado por Términos.
	\item Generación de postings lists ordenadas por Identificador de documento, donde cada posting contiene DocId y frecuencia del termino.
	\item Delta Gap Encoding para los DocIds de las posting lists.
	\item Tokenización básica de términos y validación de criterios básicos (Largo de termino, exclusión de Stopwords)
	\item Uso se Stemming para idioma ingles (Via Snowball).
\end{itemize}

\section{Conclusiones y resultados}

Al final de este proyecto se lograron los siguientes objetivos:

\begin{itemize}
	\item Construcción de un Cluster Hadoop completamente funcional.
	\item Construcción de una colección de tamaño medio que permite la evaluación de algoritmos de Big Data, con formato estándar TREC.
	\item Construcción de un Indexador básico para pruebas de cluster, de colección y de distribución de tareas en MapReduce.
	\item Pruebas de rendimiento de varios algoritmos de construcción de Índices Invertidos. Ver Adjunto a este trabajo.
\end{itemize}

Una conclusión del proyecto es que la instalación y configuración de un cluster Hadoop no es una tarea trivial. Sin embargo, una vez que se resolvieron los problemas iniciales, se dispone de un entorno estable para pruebas. Durante el proyecto también se pudo comprobar que la incorporación de equipos nuevos al cluster no representa un problema.

La construcción de algoritmos para Big Data presenta algunos problemas cuya solución no son triviales. Si bien existen patrones de diseño para aplicaciones MapReduce\cite{Lin2010}, el uso incorrecto de la API disponible en Hadoop puede resultar en la utilización ineficiente del entorno. Por ejemplo, una emisión excesiva de datos puede incurrir en cuellos de botella en la red y/o disco. Por otro lado, acumular de forma parcial en tareas intermedias puede resultar en problemas de falta de memoria en los nodos esclavos. 

El área de Big Data aun no tiene la maduración adecuada para tratar a estos problemas desde la programación, y las estructuras de datos involucradas no siempre son eficientes. El desarrollo de estructuras de datos adecuadas es un campo donde aun queda mucho por desarrollar. La solución que existe al momento consiste en diferentes modos de lograr escalabilidad vía Hardware, lo que puede ser económicamente valido para la industria, pero ocasiona otro tipo de problemas (físicos, energéticos, por citar algunos de ellos).

Como otro trabajo futuro, queda la optimización de la configuración del cluster, así como la prueba de diferentes patrones de distribución de tareas en MapReduce, para identificar cuellos de botella en Entrada/Salida de Disco, de Red o uso de RAM y CPU.

\bibliography{Tesis.bib}{}
\bibliographystyle{plain}
\end{document}
