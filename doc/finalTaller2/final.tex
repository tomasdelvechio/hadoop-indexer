% tesis.tex: Archivo principal
\documentclass[a4paper,10pt]{article}
\usepackage[spanish]{babel} % Español
\usepackage[utf8]{inputenc} % Codificacion de caracteres
\usepackage{graphicx} % graficos

\title{\textbf{Rendimiento de Indexadores Distribuidos en aplicaciones MapReduce}}
\author{Tomas Delvechio}
\date{Diciembre de 2014}

% Comienzo del documento
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {img/} }
\begin{document}

\maketitle

\section{Objetivos del trabajo}

Determinar que parámetros influyen en la escalabilidad de aplicaciones Hadoop, en relación a un problema de indexación distribuida de una colección. En particular para este trabajo, se determino que los parámetros de interés para ser medidos serian los siguientes:

\subsection{Algoritmos}

Se desarrollaron 2 algoritmos de construcción de Índices con Hadoop para comparar el rendimiento en el cluster.

\begin{itemize}
 
	\item Un algoritmo de Indexación en MapReduce Básico, tal como se plantea en Dean\cite{Dean:2004:MSD:1251254.1251264}, donde cada termino es emitido desde el Mapper como un par clave-valor con el formato \textless Termino,DocId\textgreater, y serán emitidos para un mismo termino tantos pares como términos haya en dicho Documento. El algoritmo sera identificado en las pruebas como DG.

	\item Un algoritmo basado en el desarrollo de McReadie\cite{Mccreadie:2012:MIS:2337605.2337723}, donde en lugar de emitir un par \textless Termino,DocId\textgreater ni bien sea procesado, se puede almacenar dicha información de manera temporal en memoria del dispositivo, armando una posting temporal. Luego dicha posting puede ser emitida al final del Map. Se realizo una variación al algoritmo original, donde en lugar de emitir la estructura posting se emiten un par clave-valor de la siguiente manera \textless termino,par(DocId,Freq)\textgreater. Esta decisión puede aumentar la cantidad de mensajes a través de la red, pero se tiene la ventaja de utilizar estructuras de datos simples, lo cual se considero adecuado para una primer aproximación al problema. Además, se desarrollo el mapper utilizando el concepto de in mapper-combine, explicado por \cite{Lin:2010:DTP:1855013}, en lugar de agregar un Combiner. Este algoritmo sera referenciado posteriormente como DGC.

\end{itemize}

\subsection{Cantidad de Workers}

La cantidad de nodos de cómputos disponibles es una variable de importancia dentro de las aplicaciones distribuidas. Resulta claro que disponer de mas o menos workers puede afectar de diversas formas la velocidad de ejecución del trabajo, aunque se puede decir que para cantidades de datos adecuadas y algoritmos correctamente desarrollados, mayor cantidad de workers mejora el speedup general de un trabajo distribuido.

En nuestro caso, se dispone de un cluster de un master y 5 workers. Se tomo la decisión de probar los algoritmos DG y DGC para configuraciones del cluster con 1, 3 y 5 workers trabajando.

\subsection{Cantidad de reducers}

Dentro del esquema de ejecución de Hadoop, el Reducer es la ultima fase del proceso, y tiene por objetivo realizar un procesamiento integrado de los resultados emitidos de forma distribuida por el mapper y reunidos y ordenados en las fases de shuffle y sort.

A diferencia de la cantidad de Mappers, que esta dirigido por el tamaño de la entrada, la cantidad de reducers es proporcionada por el desarrollador o al momento de ejecutar los trabajos. De esta manera, ignorar el parámetro puede dar lugar a un cuello de botella en el procesamiento, debido a que deben procesarse todos los pares emitidos por todos los mappers por un único proceso Reducer.

Para un input y algoritmo dado en una configuración de clusters, existe una cantidad adecuada de procesos Reducer para minimizar el tiempo de procesamiento en dicha fase. Dicha cantidad es experimental y debe buscarse realizando pruebas de rendimiento para cada tarea. En el presente trabajo se realizaron ejecuciones para aplicación con las siguientes cantidades de reducers: 1, 2, 4, 8, 16, 32, 64.

\section{Pruebas}

Según lo explicado anteriormente, se realizaron la siguiente ejecuciones:

2 Algoritmos x 3 configuraciones de nodos x 7 cantidades de reducers = 42 pruebas. Se ha comprobado que la salida de ambos algoritmos es idéntica en su resultado (mismo archivo con idéntico tamaño en bytes de todas las salidas).

\subsection{Colección y algoritmos}

Se utilizo una colección de textos (recuperada desde proyecto Gutenberg)\footnote{https://www.gutenberg.org/} la cual se construyo en formato TREC, dando lugar a una colección de 40Gb aproximadamente.

Los algoritmos DG y DGC se encuentran públicamente disponibles\footnote{https://github.com/tomasdelvechio/YarnExamples}.

\subsection{Cluster}

El cluster utilizado dispone de hosts que cuentan con las siguientes características de Hardware:

\begin{itemize}
	\item 6 PC con las siguientes características:
	\item Procesador: Intel(R) Core(TM) i5-2400 CPU @ 3.10GHz (4 Cores)
	\item Memoria: 7955MiB (8Gb).
	\item Placa de red: Gbit Ethernet.
\end{itemize}

Estos equipos se encuentran conectadas mediante el un switch que tiene las siguientes caracteristicas:

\begin{itemize}
	\item Marca: DLink
	\item Modelo: DGS-3120-24TC-SI
	\item Managed 24-ports Gigabit Stackable L2/L3 Switch, 20-ports Gigabit.
\end{itemize}

Una vez obtenido el Hardware, se procedió a instalar un Sistema Operativo en cada uno de los nodos. El Sistema Operativo seleccionado fue Ubuntu 14.04.

La versión instalada y utilizada de Hadoop es la 2.5.0.

\subsection{Ejecución de las pruebas}

El proceso de pruebas fue el siguiente:

\begin{enumerate}
	\item Configurar el cluster para la cantidad de nodos elegida.
	\item Subir nuevamente el archivo de prueba para evitar problemas de desbalance de bloques en HDFS explicado en \cite{White:2009:HDG:1717298}, Cap 3, Sección "Keeping an HDFS Cluster Balanced".
	\item Ejecutar las pruebas para ambos algoritmos y todas las configuraciones de reducers.
\end{enumerate}

Esto se repitió para configuraciones de 1, 3 y 5 nodos.

\section{Pruebas}

Como se pretende medir la escalabilidad de Hadoop en el cluster, se midieron tiempos de procesamiento por fase, especialmente en la fase Reducer, donde como hipótesis se esperan mejoras del algoritmo DGC sobre DG debido a que el primero genera muchos menos bytes como entrada a las tareas Reduce.

En el caso de la cantidad de reducers, no necesariamente es esperable una mejora directa del tiempo con el aumento de los reducers, debido a que pueden existir factores no contemplados, como limitaciones de IO.

\subsection{Influencia del numero de nodos}

Resulta razonable pensar que en la medida que se incrementa la cantidad de nodos, se observen mejoras en el tiempo global de los procesos. En particular, para la fase Reduce, se muestran los tiempos observados según cantidad de nodos disponibles (Ver figura \ref{fig:tiemponodos}).

\begin{figure}[htp]
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_1reducer}
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_2reducer}
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_4reducer}
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_8reducer}
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_16reducer}
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_32reducer}
	\includegraphics[scale=0.5]{3.1/tiempo_nodo_64reducer}
	\caption{Tiempo de construcción de II segun el numero de nodos empleados}
	\label{fig:tiemponodos}
\end{figure}

En todos los casos, se percibe una ligera ventaja en los tiempos del Algoritmo DGC por sobre DG. Esto se debe principalmente al ahorro de tiempo que produce la ejecución distribuida de los reducers. Sin embargo, esta mejora se ve afectada por una mala deficiencia en el proceso Map del algoritmo DGC, con lo que en el proceso global, las mejoras son mínimas. Un estudio que separe los tiempos promedios de ambas fases muestra esto de manera mas clara (Ver figura \ref{fig:tiempofase}).

\begin{figure}[htp]
	\includegraphics[width=200pt]{3.1/tiempo_fasemap}
	\includegraphics[width=200pt]{3.1/tiempo_fasered}
	\caption{Tiempo promedio de construcción de II segun fase}
	\label{fig:tiempofase}
\end{figure}

Lo que se puede observar es que la fase Map de DGC es superada en un caso por el algoritmo DG para 3 nodos y en otro caso se mantiene similar. En el caso de la fase Reduce, se ve que en la ambas configuraciones el Algoritmo DGC es claramente superior a DG.

\subsection{Influencia del numero de reducers}

Se estudió también que influencia tiene en la fase de Reduce aumentar el numero de reducers disponibles. Se muestran resultados en los gráficos, separados por cantidades de nodos disponibles (Ver figura \ref{fig:tiempored}).

\begin{figure}[htp]
	\includegraphics[width=200pt]{3.2/tiempo_red_1nodo}
	\includegraphics[width=200pt]{3.2/tiempo_red_3nodos}
	\includegraphics[width=200pt]{3.2/tiempo_red_5nodos}
	\caption{Tiempo de construcción de II segun cantidad de reducers}
	\label{fig:tiempored}
\end{figure}

Tomando como base las observaciones de \cite{Mccreadie:2012:MIS:2337605.2337723}, vemos que se repite el fenómeno de que con pequeños aumentos de la cantidad de reducers (Mas de uno y menos de 20), se obtienen mejoras sustanciales en el tiempo final de procesamiento para ambos algoritmos. Se observa también, que a mayor cantidad de nodos, se mejoro el tiempo de procesamiento. Sin embargo, no es necesariamente la misma cantidad de reducers para diferentes configuraciones del cluster. Para 5 nodos, DG Obtiene su mejor tiempo en 16 reducers, y en el caso de DGC es idéntico para 8 o 16.

Sin embargo, a partir de ahí, nuestras pruebas difieren de lo visto en \cite{Mccreadie:2012:MIS:2337605.2337723}, ya que en dicha investigación, a partir de un punto determinado aumentar el numero de reducers tenia un efecto negativo en el tiempo global de la aplicación. En nuestro caso, en cambio, no se observa dicho comportamiento. Sino que se mantiene estable el tiempo para valores de reducers mayores a 20 (con algunas variaciones).

Es motivo de un futuro trabajo investigar el porque de estas diferencias para cantidades de Reducers superiores a las 20 entre los resultados de \cite{Mccreadie:2012:MIS:2337605.2337723} y los nuestros.

\subsection{Speedup}

\subsubsection{Speedup según el numero de reducers}

Se midió el Speedup de los algoritmos contra el teórico en función del incremento de reducers. Para las cantidades de Reducers 2 y 4, El speedup de DG y DGC fue superlineal, es decir, el speedup real supero al teórico. Esto se explica según entendemos porque ambas aplicaciones, al escribir en diferentes discos, mejoro en el uso del IO, que suele ser un cuello de botella en estas aplicaciones.

\begin{figure}[htp]
	\includegraphics[width=200pt]{3.3.1/speedup_red_1nodo}
	\includegraphics[width=200pt]{3.3.1/speedup_red_3nodo}
	\includegraphics[width=200pt]{3.3.1/speedup_red_5nodo}
	\caption{Speedup del algoritmo segun cantidad de reducers}
	\label{fig:speedupred}
\end{figure}

La meseta que se percibe para valores de reducers iguales o mayores a 8, se puede explicar por una sub optima configuración del cluster, sumado a limitaciones en la cantidad de discos secundarios disponibles. Lo que genera nuevamente un cuello de botella a la hora de escribir resultados (intermedios y definitivos).

En cuanto a la sub optima configuración, se encontró que todo el cluster nunca supero el 40\% de utilización de la CPU disponible y algo similar ocurrió con el uso de RAM.

\subsubsection{Speedup según el numero de nodos disponibles}

Se comparo el speedup de cada configuración de reducers para todos los nodos (Ver figura \ref{fig:speedupnodos}). Salvo para 1 reducer, en el resto de los casos se ven resultados cercanos al speedup óptimo.

\begin{figure}[htp]
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_1red}
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_2red}
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_4red}
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_8red}
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_16red}
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_32red}
	\includegraphics[width=200pt]{3.3.1/speedup_nodos_64red}
	\caption{Speedup del algoritmo segun cantidad de nodos}
	\label{fig:speedupnodos}
\end{figure}

Como se puede observar, en todos los casos, el speedup mejoró en la medida que se adicionan nodos de procesamiento, y su mejora acompaño a la óptima estimada.

\section{Conclusiones}

En el presente trabajo se estudió el impacto de la cantidad de tareas reducers y cantidad de nodos presentes en un cluster, utilizando un algoritmo MapReduce desarrollado en Hadoop, que construye un Índice Invertido de una colección en formato TREC construida específicamente para este trabajo.

\begin{itemize}
	\item Minimizar la entrada en los reducers logró una mejora global del tiempo de la aplicación. La causas son variadas: El Map minimizó los "spills" a disco local, con lo cual se reduce el IO de la fase MAP. El Shuffle y Sort trabajan sobre menor cantidad de datos. El Reducer finalmente procesa menos datos y por lo tanto, obtiene los resultados con menor procesamiento. Detrás de esto esta el motivo por el cual DGC obtuvo mejor rendimiento que DG en todas las fases Reduce. Se recuerda que en ambos algoritmos, el índice invertido obtenido es exactamente el mismo.
	\item Existe un numero optimo de reducers, que depende del tamaño de la aplicación pero que no debe ser demasiado elevado. Esto en general depende del tamaño de la entrada a procesar y la cantidad de nodos, pero para configuraciones de cluster de 5 slaves y una entrada de 40Gb, parece que un numero de reducers que ronde entre los 8 y 16 es un numero adecuado.
	\item La cantidad de discos disponibles afecto positiva o negativamente el speedup. Cuando la cantidad de reducers fue menor a la de discos se vio favorecido, pero para valores mayores, esto afecto el rendimiento general del Indexador.
	\item Una configuración sub optima del cluster generó efectos negativos en el speedup del proceso.
\end{itemize}

\section{Trabajos Futuros}

\begin{itemize}
	\item Estudiar los efectos de aplicar compresión intermedia, que es un mecanismo de Hadoop para comprimir los resultados a la salida de la fase Map y descomprimirlos a la entrada del Reduce. De esta forma, en aplicaciones limitadas por el IO de almacenamiento secundario se pueden lograr mejoras a costa de un gasto adicional de procesamiento.
	\item Estudiar los motivos por los cuales para números elevados de reducers (mayores a 20), en el caso de \cite{Mccreadie:2012:MIS:2337605.2337723} se obtienen peores tiempos y en nuestras pruebas se obtienen tiempos muy similares.
Medir de manera efectiva como afecta la cantidad de discos y la velocidad de IO a los procesos de indexación DG y DGC.
	\item Comparar los tiempos de ejecución de los algoritmos con un cluster óptimamente configurado.
\end{itemize}

\bibliographystyle{plain}
\bibliography{referencias.bib}
\end{document}