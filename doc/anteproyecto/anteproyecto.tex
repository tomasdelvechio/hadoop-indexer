\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{graphicx}

\title{\textbf{Propuesta de Plan de Tesina:}\\ \textbf{Análisis comparativo de rendimiento en estructuras de datos de Índices Invertidos}}
\author{Tomas Delvechio}
\date{Noviembre de 2013}

\begin{document}

\maketitle

\section{Introducción}

Los sistemas de Recuperación de Información (IR) modernos se enfrentan a la problemática de tratar crecientes volúmenes de datos en contextos donde la velocidad y precisión en las respuestas son cada vez mas requeridas. Existe un compromiso entre la velocidad de respuesta y la precisión, pero estos a su vez se ven condicionados por el tamaño del corpus que el Sistema de IR deba manejar.

Frente a esta realidad, las optimizaciones y mejoras de los algoritmos y estructuras se transforman en un campo de investigación donde la academia tiene muchos aportes por realizar. Uno de los posibles tópicos se encuentra en las estructuras de datos que dan soporte a las consultas realizadas sobre los buscadores, así como a las optimizaciones y compresiones que se puedan realizar sobre los índices de búsqueda generados a partir del Corpus en cuestión.

Esta propuesta abarca el estudio de dos estructuras de datos  concretas que representan los índices de una colección de documentos, las cuales buscan obtener un subconjunto de resultados precisos sin la necesidad de recorrer todo el índice de la colección. Ambas estructuras requieren ser evaluadas en contextos centralizados y distribuidos, siendo comparadas entre si y con un sistema que utilice un Índice de búsqueda convencional, que constituye la linea base del estudio.

\section{Justificación}

En IR se trabaja con estructuras de datos que prestan soporte a las búsquedas de información. La estructura fundamental de todo sistema de IR es el Índice Invertido (II), el cual consta, en su forma mas sencilla, de un índice de palabras (o términos para ser mas precisos) y un listado de los documentos donde cada uno de estos términos aparecen. Los términos del II están limitados a aquellos que forman parte del vocabulario de la colección. El listado de documentos construido para cada termino es conocido como Posting list.

Estos índices pueden ser muy extensos, y es fundamental definir formas óptimas de recorrerlos a fin de lograr optimizar el procesamiento de consultas. Existen diversas técnicas de compresión de II y de Posting lists. Además, dependiendo del tamaño del corpus y del II asociado al mismo, los diseñadores de sistemas de IR deben determinar si el índice puede mantenerse en memoria o debe ser alojado en almacenamiento secundario. En este contexto, técnicas de compresión eficientes tanto en el ahorro de espacio así como también en el acceso a los datos son un desafío actual dentro de la disciplina.

Por otro lado, existen necesidades de información que no requieren la totalidad de resultados posibles en la respuesta. P.e. Si un usuario realiza una consulta en un motor de búsqueda solicitando el horario de un evento, no necesita todas las paginas web que tengan la información, tan solo con la primera que posea la información correcta es suficiente. Tanto en este como en otros ejemplos, la exhaustividad no es importante en el resultado final. Estos escenarios mostraron la necesidad de generar un conjunto de algoritmos y técnicas que se conocen como “early-termination techniques” (Técnicas de terminación temprana), que tienen por objetivo lograr un conjunto de k resultados en vez de cubrir todos los resultados posibles. A la idea de recuperar los k primeros resultados de una consulta se le conoce como obtener el Top-k de la misma.

A raíz de los tópicos planteados anteriormente, surgieron técnicas que buscan integrarlos dentro de mecanismos avanzados, mejorando el compromiso entre las variables existentes. Por ejemplo, aplicando las técnicas básicas de compresión de índice se genera un costo de procesamiento (para descomprimir) que no siempre resulta conveniente si se busca optimizar el tiempo de recuperación de resultados.

Treap\cite{6} y Block-Max Indexes\cite{3} son estructuras avanzadas para Índices Invertidos que tienen la propiedad de lograr resolver uniones e intersecciones booleanas rankeadas de forma rápida para un top-k definido. La manera en que construyen los índices les permiten realizar operaciones de forma eficiente, comparado con los métodos tradicionales. Además, ambas técnicas incluyen compresión y recuperación de documentos en base a un umbral a determinar.

Debido a las exigencias planteadas por el gran aumento de datos y la cantidad de usuarios que realizan consultas, las aplicaciones modernas que requieren alta capacidad de procesamiento y almacenamiento se despliegan en entornos distribuidos. Una de las herramientas mas adoptadas es el Framework Hadoop\cite{8}, que es una implementación libre de MapReduce\cite{9}. Hadoop permite construir un cluster que realice procesamientos basados en la técnica MapReduce, y es una herramienta que esta siendo adoptada como un estándar en procesamiento distribuido por una gran cantidad de empresas y organizaciones\cite{10}.

\section{Objetivos}

El objetivo del trabajo es realizar mediciones de velocidad de respuesta a consultas y uso de espacio  en contextos centralizados y distribuidos de algoritmos de recuperación basados en 3 estructuras diferentes:

\begin{itemize}
	\item Algoritmo de IR con Índice Invertido básico (Baseline)
	\item Algoritmo de IR con soporte de Block-Max Indexes
	\item Algoritmo de IR con soporte de Treaps
\end{itemize}

La comparación abarca evaluar el comportamiento de cada uno de los algoritmos para diferentes  top-k de resultados.

De la evaluación de los resultados se espera obtener una caracterización de los escenarios donde cada una de las técnicas se desempeña con mejor rendimiento, determinando cuales son convenientes para aplicar cada una de las técnicas.

\section{Alcance}

El alcance de este trabajo esta limitado a los siguientes punto:

\begin{itemize}
	\item Construcción o reutilización de 3 algoritmos de IR en contexto centralizado con soporte de los siguientes II:
	\begin{itemize}
		\item Un II con información mínima
		\item Un II con soporte de Block-Max Indexes
		\item Un II con soporte de Treap
	\end{itemize}
	\item Construcción o reutilización de 2 algoritmos de IR en contexto distribuidos con soporte de los siguientes II:
	\begin{itemize}
		\item Un II con soporte de Block-Max Indexes
		\item Un II con soporte de Treap
	\end{itemize}
	\item Obtención o construcción de una colección de tamaño adecuado para la realización de las pruebas.
	\item Obtención o construcción de las consultas de usuario necesarias para la realización de las pruebas.
	\item Construcción de scripts y estructuras necesarios para el tratamiento de los datos generados como resultados de las pruebas.
	\item Redacción de informe final con los resultados hallados en las pruebas.
\end{itemize}

\section{Técnicas y Herramientas}

\subsection{Técnicas}

\begin{itemize}
	\item Lenguaje de desarrollo de algoritmos de estructuras: C.
	\item Desarrollo de prototipos y scripts de procesamiento de resultados: Python o Perl.
	\item Sistemas Operativos: Ubuntu Linux, Debian GNU/Linux.
	\item Soporte distribuido: Hadoop.
	\item Gestión de versiones de código fuente: Git y Github.
	\item Gestión bibliográfica y notas: Mendeley 1.10 y Evernote.
\end{itemize}

\subsection{Herramientas}

Para la recolección del Corpus, se procurara obtener una colección de documentos estandarizada y probada. En caso de no ser posible, se realizaran técnicas de Crawleo de documentos en la web para obtener una mínima cantidad de documentos que hagan factible las pruebas de los algoritmos.

Para la implementación de los algoritmos, en los casos de los algoritmos de Treap o Block-Max se intentara obtener los algoritmos que se usaron en las investigaciones respectivas, en los casos que dichos algoritmos estén públicamente disponibles. En caso contrario, se procederá a realizar el desarrollo en función de la información disponibles según las publicaciones actuales. Para el algoritmo de Baseline, se procederá a re-implementar técnicas estudiadas en la asignatura Taller Libre I. La re-implementación se hará en lenguaje C.

\section{Cronograma}

Etapas:

\begin{itemize}
	\item Recolección y actualización bibliográfica – 2 Semanas.
	\item Obtención y/o construcción de colección para pruebas – 1 Semana.
	\item Obtención y/o construcción de consultas para pruebas – 1 Semana.
	\item Construcción de II con 3 algoritmos centralizados – 2 Semanas.
	\item Construcción de buscadores para cada tipo de II – 3 Semanas.
	\item Construcción de entorno distribuido – 1 Semana.
	\item Adaptación de algoritmos en entorno distribuido – 1 mes.
	\item Realización de pruebas – 3 semanas.
	\item Procesamiento de resultados – 3 semanas.
	\item Revisión y últimos detalles en redacción de informe final – 3 semanas.
\end{itemize}

\begin{figure}[htp]
	\centering
	\includegraphics[scale=0.7]{cronograma.png}
	\caption{Cronograma}
	\label{fig:crono}
\end{figure}

Dedicación semanal: 10 Hs.

Duración total planificada: Entre 6 y 7 Meses.

\section{Análisis de riesgos}

Este análisis comprende todas las cuestiones que podrían llegar a retrasar el proyecto o eventualmente hacer cambiar el enfoque o preguntas.

\begin{itemize}
	\item Las estructuras de datos en el ámbito de IR están en constante evolución, debido a los avances realizados por la comunidad académica. Por esto un riesgo es que aparezcan investigaciones que repliquen las pruebas propuestas en este trabajo, o que surjan nuevas estructuras que valga la pena tener en cuenta. Por esto un factor importante es no retrasar el trabajo de forma excesiva.
	\item El lenguaje de programación necesario para realizar estas pruebas (C). Este lenguaje es un estándar para que los algoritmos sean óptimos y comparables contra otras investigaciones. Sin embargo, a lo largo de la carrera fueron pocas las oportunidades de obtener experiencia con el uso del mismo. Entonces el riesgo reside en utilizar un lenguaje sin conocerlo ampliamente. Como contraposición, es un lenguaje ampliamente usado por la comunidad académica y existe mucha documentación sobre el mismo disponible de forma publica.
	\item Hadoop es una plataforma de procesamiento distribuido ampliamente difundida en la industria. Para el caso particular de este proyecto, el conocimiento sobre esta herramienta es teórico aun. Esto puede generar problemas eventualmente en la adaptación de los algoritmos que fueron pensados para un entorno centralizados. Al igual que el caso de C, al ser una herramienta libre y muy utilizada, la documentación al respecto de su uso e implementación esta actualizada y disponible de forma publica.
\end{itemize}

\begin{thebibliography}{99}
\bibitem{1} R. Baeza-Yates and B. Ribeiro-Neto, Modern Information Retrieval. New York, 1999.
\bibitem{2} W. B. Croft, D. Metzler, and T. Strohman, Search engines: Information retrieval in practice. 2010, p. 	283. 
\bibitem{3} S. Ding and T. Suel, “Faster top-k document retrieval using block-max indexes,” Proc. 34th Int. ACM 	SIGIR Conf. Res. Dev. Inf. - SIGIR  ’11, p. 993, 2011. 
\bibitem{4} W. B. Frakes and R. Baeza-Yates, Information retrieval: Data structures \& algorithms (Version 	Digital). 1992, p. 630. 
\bibitem{5} R. Konow and G. Navarro, “Faster Compact Top-k Document Retrieval,” 2013 Data Compression 	Conf., pp. 351–360, Mar. 2013. 
\bibitem{6} R. Konow, G. Navarro, C. L. A. Clarke, and A. López-Ortíz, “Faster and Smaller Inverted Indices with 	Treaps,” SIGIR  ’13 Proc. 36th Int. ACM SIGIR Conf. Res. Dev. Inf. Retr., vol. 1, pp. 193–202, 2013. 
\bibitem{7} C. D. Manning and P. Raghavan, “An Introduction to Information Retrieval,” 2009.
\bibitem{8} A. Foundation, “Apache Hadoop 2.2.0,” 2013. [Online]. Available: 	http://hadoop.apache.org/docs/current/.
\bibitem{9} J. Dean and S. Ghemawat, “MapReduce: simplified data processing on large clusters,” Commun. 	ACM, pp. 1–13, 2008.
\bibitem{10} A. Foundation, “Powered By.” [Online]. Available: http://wiki.apache.org/hadoop/PoweredBy. Noviembre 2013.
\end{thebibliography}

\end{document}
