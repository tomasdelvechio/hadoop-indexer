\chapter{Hadoop} 

Hadoop es el estándar de hecho para las aplicaciones de BigData. La plataforma 
alcanzo una cuota de mercado donde difícilmente se pueda encontrar competencia
en los próximos años, utilizado por una amplia gama de organizaciones, desde
pequeñas Startups hasta empresas de primer nivel (Como Amazon, IBM, Microsoft, 
Oracle, por nombrar algunas).

La propuesta inicial de Hadoop era una implementación abierta de 
MapReduce\cite{Dean2004}, pero en la actualidad ha evolucionado para convertirse
en una herramienta flexible y con una gran cantidad de soluciones para 
diferentes problemas.

El gran cambio se establece al pasar de la versión 1 a la 2, donde el framework 
de procesamiento es dividido en 2 grandes sub-sistemas: un sistema de 
planificación y comunicación genérico, llamado YARN, y el modelo de 
procesamiento MapReduce idéntico al provisto por la versión 1 del framework. 
Este cambio generó la posibilidad de construir nuevos frameworks que no dependan 
de los limites de MapReduce, sino que se enfoquen en otros problemas. Por 
nombrar algunos tenemos Spark (Para procesamiento de datos con memoria 
distribuida) y Storm (procesamiento en real-time de stream).

Teniendo en cuenta de la diversidad de tecnologías, frameworks, y sistemas que 
se encuentran en la orbita de Hadoop, en este trabajo nos referimos como Hadoop 
a los servicios de bajo nivel que vienen con el framework por defecto: HDFS y 
MapReduce. No se utilizan frameworks adicionales, ni herramientas externas que 
influyan en la ejecución de las aplicaciones desarrolladas.

En el presente capitulo se abordara de forma conceptual la arquitectura que 
Hadoop propone de forma nativa (MapReduce y HDFS), ventajas e inconvenientes del
modelo propuesto, y una evaluación general de la plataforma. En capítulos 
posteriores se analizara el impacto del modelo de procesamiento en la generación
de un índice invertido.

\section{Arquitectura de Hadoop}

Hadoop propone una arquitectura master-worker para el funcionamiento de sus 
servicios. Esto significa que los procesos worker no tienen autonomía de 
decisión en el contexto de los trabajos ejecutados dentro del framework. Cada 
acción a tomar debe ser solicitada al master, este acepta la solicitud si es 
que existen recursos disponibles, y notifica al worker que puede iniciar la 
tarea.

En una instalación convencional conviven 2 servicios de forma permanente: HDFS 
y YARN. HDFS es el sistema de archivos distribuido. YARN es el planificador de 
tareas. Cada uno de estos servicios opera de forma autónoma, pero coordinan sus 
tareas para lograr el objetivo de ejecutar una aplicación. Sobre YARN se 
construye el framework MapReduce.

Tanto YARN como HDFS poseen una arquitectura master-worker. Se puede entender 
mejor mirando la imagen \label{fig:HadoopMasterSlave}.

\begin{figure}[htb]
  \begin{center}
    \includegraphics{../img/HadoopMasterSlave.png}
    \caption{Arquitectura de los servicios de Hadoop}
    \label{fig:HadoopMasterSlave}
  \end{center}
\end{figure}

Antes de hacer un repaso por las características de cada uno de estos 
servicios, es importante señalar que que hay conceptos específicos de la 
implementación de MapReduce realizada por Hadoop y otros que son de la 
propuesta original de MapReduce. A lo largo del trabajo se utilizaran de forma 
indistinta, debido a que no existe otra implementación masiva y disponible de 
MapReduce que no sea la de Hadoop. Por ende, no vemos posibilidad de incurrir 
en errores de interpretación.

\subsubsection{HDFS}

HDFS son las siglas de Hadoop Distributed File System, y es una implementación 
abierta de sistema de archivos distribuidos, con claras influencias de
GFS\cite{Shvachko2010}, el sistema distribuido propuesto por Google. 

HDFS propone un Filesystem jerárquico el cual intenta ser muy similar al 
Filesystem de los sistemas UNIX/Linux clásicos:

\begin{itemize}
  \item Raíz del sistema identificado con /
  \item Elementos: Archivos y Directorios
  \item Esquema de permisos con Usuarios, Grupos y Otros
  \item Granularidad de permisos de 3 niveles: Lectura, Escritura, Ejecución
  \item Primitivas de comandos estándar: ls, mkdir, cat, touch, cp, mv, rm
\end{itemize}

Todo el sistema se opera a través de un cliente construido en Java que dialoga 
con el NameNode para conocer los metadatos y con los DataNodes para las 
operaciones sobre la información. También es posible operar sobre HDFS desde 
aplicaciones MapReduce a través de la API que Hadoop expone para ello. Nos 
referiremos como cliente de HDFS a cualquiera de estas dos posibilidades, 
haciendo distinciones solo en aquellos casos que tenga importancia resaltar una 
diferencia en la forma de operación.

La abstracción que propone el Filesystem permite visualizar los datos 
con una estructura de archivos y directorios. Sin embargo, HDFS maneja 
internamente los datos con una subestructura conocida como bloque. Un bloque es 
una unidad de datos interna de HDFS, y cuando un archivo es creado o subido a 
HDFS, es particionado en N bloques de tamaño S. Estos bloques pueden a su vez 
ser replicados R veces. Tanto el tamaño S como la cantidad de replicas R puede 
ser especificado para cada archivo, y en caso de que nada sea especificado, S 
se asumen como 64 Megabytes y R se toma como 3 replicas por bloque.

En la operación cotidiana de HDFS, el usuario no necesita saber absolutamente 
nada de estos bloques, solo necesita conocer el path o ruta a nivel de espacio 
de nombres ofrecido por el Filesystem. El NameNode se encarga de 
almacenar toda la información de la estructura del Sistema de archivos y como 
fue particionado y replicado cada uno de los archivos (Es decir, cantidad de 
bloques y ubicación física en los DataNodes). Cuando un cliente realiza una 
operación sobre un archivo, el NameNode recibe la consulta, controla 
disponibilidad de bloques, carga de trabajo de los workers, distancia 
administrativa, entre otras métricas, y le indica al cliente que bloques debe 
solicitar a los DataNodes para poder construir el archivo requerido.

El esquema master-slave de HDFS implica que el NameNode (Nodo master de HDFS) 
es un punto único de falla. La división de tareas en Hadoop es que todos los 
metadatos del Filesystem se encuentran en el NameNode, y los bloques (los 
datos en si) se encuentren en los DataNodes. Por eso existen esquemas de 
backup, federación y replicación de NameNodes para poder ofrecer HA (Alta 
disponibilidad).

Cuando se discuta el framework MapReduce, se abordara como es que esta 
centralización de los metadatos y partición y replicación de alto nivel ayuda a 
generar un esquema paralelizable y distribuible de forma transparente para el 
desarrollador.

\subsection{MapReduce}

MapReduce es un framework de procesamiento de datos que ofrece escalabilidad 
de forma nativa permitiendo ejecutar cierto tipo de tareas de forma 
distribuida. Para esto se vale de un conjunto de restricciones a la hora de 
definir una aplicación, establecidas por el modelo de desarrollo.

MapReduce es presentado en el año 2004\cite{Dean2004}, como propuesta para el 
tratamiento de conjuntos de datos masivos. Hadoop desarrolla la implementacion
alrededor del año 2006.

MapReduce permite al desarrollador definir una funcion map y una 
funcion reduce (inspirados en las ideas analogas en lenguajes de programacion 
funcional). Ambas funciones tienen como datos de entradas una tupla 
<clave,valor>, y deben generar salida en el mismo formato. El objetivo que 
cumple un programa MapReduce viene dado por el procesamiento realizado en cada 
funcion map y reduce, y la salida de lal mismo es el conjunto de tuplas 
<clave,valor> obtenidas como salida de la funcion reduce.

El flujo de trabajo de una aplicacion MapReduce se puede ver en la figura 
\label{fig:MapReduceWorkflow}. Cada una de las fases se describen a 
continuacion:

\begin{itemize}
  \item Fase Input: Esta fase se encarga de recopilar los archivos que seran 
entradas de la aplicacion y definir como se dividen (Split) en funcion de la 
cantidad de procesos Map a ejecutar.
  \item Fase Map: Cada proceso Map recibe un conjunto de registros a procesar 
segun la fase de Input le haya asignado. El proceso Map es desplegado en tantos 
workers como haya disponibilidad, y procesaran los bloques correspondientes. 
Hadoop realizará el mejor esfuerzo para que los procesos Map se ejecuten sobre 
bloques que esten ubicados físicamente en el mismo worker. El Map realiza el 
procesamiento de los registros asignados, y emite resultados en la forma <K,V>, 
donde K es interpretado como clave, y V es un valor asociado a dicha clave.
  \item Fase Intermedia: Un proceso automatico es ejecutado sobre la totalidad 
de pares <K,V> emitidas por todos los procesos Map. Este proceso cumple dos 
funciones primordiales: Reune todos los pares <K,V> que posean la misma clave 
K, generando un par <K,[V1, V2, V3...]>, donde [V1, V2, V3...] es el listado de 
todos los valores que fueron emitidos desde la fase Map con una misma clave. La 
otra funcion es la de ordenar de forma ascendente todos los pares a partir de 
la clave.
  \item Fase Reduce: El proceso Reduce recibe los pares <K,[V1, V2, V3...]>, y 
realiza el procesamiento segun la funcion provista por el desarrollador. La 
fase Reduce exige una nueva salida <K,V>, que puede o no coincidir con el tipo 
o valor de las K y V de entrada del mismo.
  \item Fase Output: El conjunto de los pares <K,V> emitidos por todos los 
procesos Reduce llegan a una ultima fase que se encarga de gestionar la 
persistencia de dicha información en HDFS.
\end{itemize}

\begin{figure}[htb]
  \begin{center}
    \includegraphics{../img/MapReduceWorkflow.png}
    \caption{Flujo de trabajo de una aplicación MapReduce}
    \label{fig:MapReduceWorkflow}
  \end{center}
\end{figure}

\subsection{Ventajas del modelo MapReduce}

Como se menciono previamente, MapReduce ofrece como modelo una serie de 
ventajas que nos ayudan a 

\subsection{Desventajas del modelo MapReduce}