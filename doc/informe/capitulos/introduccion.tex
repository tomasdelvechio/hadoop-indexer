\chapter{Introducción}\label{ch:intro}

En la actualidad, las organizaciones de todo tipo y tamaño tienen a su disposición grandes volúmenes de información a muy bajo costo. Por ejemplo, la web es un repositorio de documentos que tiene una escala sin precedente y con alcance mundial. Asimismo, las organizaciones aumentan su capacidad de generar datos y, por consiguiente, una necesidad intrínseca de almacenarlos. Algunas estimaciones indican que los conjuntos de datos crecen exponencialmente~\cite{Manyika2011}. Los datos almacenados (analógicos y digitales) suman 2,6 exabytes para el año 1986, 15,8 en 1993, 54,5 en el 2000 y 295 hacia el 2007. Dentro de estos valores, el 99\% de los datos en 1986 eran analógicos. Para 2007, los datos digitales conforman el 94\% de los 295 exabytes estimados~\cite{Hilbert2011}.

Como un ejemplo ilustrativo, durante el primer Simposio Argentino de Grandes Datos\footnote{\url{http://44jaiio.sadio.org.ar/agranda}}, se muestra como el uso del Sistema SUBE por parte de usuarios de colectivos del AMBA (Área Metropolitana de Buenos Aires) genera un conjunto de datos que no puede ser recolectado por otros medios (como encuestas). Un estudio presentado en dicho simposio reporta que 3 años de datos de una empresa del sector que cuenta con el 1\% de los colectivos del sistema, reúne 40 millones de boletos vendidos y 150 millones de posiciones de GPS~\cite{Melani2016}.

Realizar operaciones de procesamiento y recuperación de información en este contexto no es una tarea trivial y los motores de búsqueda comerciales no tienen alcance en el ámbito interno de las organizaciones. La industria ofrece una plataforma conocida como computación en la nube (también referida como Cloud Computing o ``la nube'') como una forma de abordar el problema de la escalabilidad de los procesos, del almacenamiento y sus costos asociados\footnote{\url{https://www.wired.com/2015/03/amazon-unlimited-everything-cloud-storage/}}. Sin embargo, esta opción no es viable en todos los casos (por ejemplo, por problemas económicos o legales respecto de los datos a manipular). Por eso, las técnicas de recuperación de información de gran escala son de alto interés para la industria y la academia.

La manipulación de datos a escala masiva también genera que las técnicas tradicionales de procesamiento y búsqueda de información no sean tan eficientes como podían serlo hace una década o menos. Es necesario que los procesos de recuperación de información clásicos se adapten a esta nueva escala y evolucionen conforme crece la cantidad de datos disponibles (es decir, generar algoritmos escalables). Una opción explorada en la disciplina es la elaboración de algoritmos de cómputo distribuido junto con plataformas de ejecución de alto nivel.

El presente trabajo surge como necesidad de integrar dos áreas: La Computación Distribuida y la Recuperación de Información. La integración de estas áreas no es, ni por lejos, algo novedoso. Existe amplia literatura al respecto que data de varias décadas~\cite{Mccreadie2009}~\cite{Dean2004}. La industria también posee algoritmos y servicios que se pueden considerar maduros en este ámbito. Sin embargo, la amplitud de métodos, herramientas y técnicas proporciona siempre nuevas ideas para explorar enfoques que permiten elaborar implementaciones alternativas que pueden resultar de interés. A lo largo de este capítulo introductorio se establece el contexto general asumido y se explican las motivaciones y objetivos del trabajo.

\section{El Problema de la Recuperación de Información}

La Recuperación de Información (abreviado como RI) es una disciplina que tiene su origen en la necesidad de contar con herramientas que permitan el acceso a documentos digitales de naturaleza no estructurada, mediante alguna interfaz ofrecida al usuario para que este pueda expresar sus necesidades de información en un lenguaje de consultas (\textit{queries}) con una sintaxis determinada. Una opción, entre otras que existen~\cite{Witten1999}, son las consultas de texto libre, populares en los modernos motores de búsqueda comerciales.

Baeza-Yates~\cite{Baeza-Yates2011} plantea que la Recuperación de Información trata con la representación, almacenamiento, organización y acceso a ítems de información. Es un área que abarca desde la recuperación de documentos hasta la forma en que el usuario accede a los mismos, pasando en el medio por muchos procesos para que esto suceda de forma adecuada:

\begin{itemize}
    \item Recuperación y normalización de los documentos.
    \item Generación de estructuras auxiliares que soporten la recuperación.
    \item Cálculo de rankings de documentos.
    \item Visualización de respuestas de consultas de usuario.
    \item Compresión de datos.
\end{itemize}

El proceso de la recuperación de información tiene varias etapas, que se pueden resumir a grandes rasgos en los siguientes pasos:

\begin{itemize}
    \item El Usuario envía una consulta al motor de búsqueda a través de una interfaz, utilizando un lenguaje de consulta.
    \item El motor de búsqueda recibe la consulta, la evalúa y utiliza las estructuras que tiene disponibles para definir cuales son los documentos que resultan relevantes para responder la solicitud de información.
    \item Una vez definidos los documentos a ser recuperados, el motor genera un ranking en base a la información disponible, y devuelve el listado de documentos al Usuario.
\end{itemize}

\begin{figure}[htb]
  \begin{center}
    \includegraphics[scale=0.3]{img/ProcesoRecuperacionInformacion.png}
    \caption{El Proceso de la Recuperación de Información}
    \label{fig:ProcesoRecuperacionInformacion}
  \end{center}
\end{figure}

Para que los pasos anteriores funcionen eficiente y eficazmente, el motor de búsqueda debe contar con un corpus o colección de documentos que resulten de interés para los Usuarios. También es necesario realizar algunos procesos que tienen por objetivo construir estructuras auxiliares para ser utilizadas durante la recuperación. Este proceso de creación de las estructuras es previo a la resolución de consulta y consta de los siguientes pasos:

\begin{itemize}
    \item Recuperar o definir la colección de documentos.
    \item Ejecutar un proceso que recorra la colección y genere las estructuras de datos que dan soporte a la recuperación.
\end{itemize}

La estructura mas popular y que a mostrado ser mas flexible en los sistemas de RI de propósito general es el índice invertido~\cite{Baeza-Yates2011}~\cite{Witten1999}~\cite{Croft2010}. El proceso de generación de dicho índice se conoce como indexación o construcción del índice, y es el caso de estudio objetivo utilizado para implementar los algoritmos y medir el comportamiento de los mismos en este trabajo.

\subsection{La Necesidad de la construcción de Índices}

Un índice invertido es una estructura de datos que permite obtener un listado de documentos a partir de un conjunto de términos dado~\cite{Baeza-Yates2011}. Los usuarios de los motores de búsqueda proporcionan términos al sistema para resolver sus necesidades de información. De esta manera, los índices invertidos permiten resolver las consultas de los usuarios a partir de los términos de las mismas haciendo uso del mencionado índice~\cite{Zobel2006}.

En la literatura pueden encontrarse diferencias entre el concepto de índice invertido y archivo invertido~\cite{Manning2009}. En el contexto de este trabajo, se hablara de índice invertido.

Hace décadas se estableció que utilizar un índice para resolver consultas en motores de búsqueda mejora en varios ordenes de magnitud la eficiencia de la búsqueda de información manual por parte del usuario~\cite{Frakes1992}.

Witten explica que ``El objetivo de un índice es resolver el problema de como la información debe estar organizada para que las consultas sean resueltas de forma eficiente y las porciones relevantes de los datos sean rápidamente extraídas''~\cite{Witten1999}. El índice invertido es la estructura que permite esto.

Si bien la construcción de un índice tiene costos de tiempo y almacenamiento asociados, también resulta claro que en determinados contextos, dicho costo se compensa en función de las mejoras que proporciona el mismo para la velocidad de procesamiento de las consultas en un motor de búsqueda~\cite{Baeza-Yates2011}. Esto es válido para colecciones de documentos que sobrepasen un tamaño determinado. Además, en relación al procesamiento secuencial del índice frente a cada consulta, la construcción y el mantenimiento del mismo se convierte en una tarea de bajo costo~\cite{Baeza-Yates2011}, y en si mismo, es de complejidad baja. De hecho, en la literatura se establece que el proceso de indexado es un problema cuyas principales restricciones están asociadas al hardware disponible en los equipos que construyen el índice~\cite{Manning2009}. Resulta claro entonces que cuando el tamaño de la colección sobrepasa los recursos disponibles en un único equipo (en general, memoria principal y CPU), se requiere cambiar el enfoque con el cual el problema se aborda. 

\section{Big Data}

El imparable crecimiento de datos no estructurados en los últimos años plantea el desafío de disponer de esquemas de procesamiento masivos a costos razonables y con prestaciones adecuadas. El sitio World Wide Web Size\footnote{\url{http://www.worldwidewebsize.com/} - Consultado en abril de 2017} reporta una estimación del índice de Google de entre 46 y 47 miles de millones de páginas web.

El crecimiento de los datos no es exclusivo del ámbito de la Web. La compañía Farecast predecía para el 2008 el precio de los boletos de aerolíneas y ofrecía dichos servicios a los consumidores basándose en la información de los vuelos comerciales adquiridos a las mismas empresas de vuelo. En un año procesaba cerca de doscientos mil millones de registros. El fundador afirmaba que diez años antes hubiera sido imposible ofrecer este tipo de servicios, debido tanto a la capacidad de almacenamiento y procesamiento disponible como al enfoque en el uso de los registros transaccionales que existía en las organizaciones~\cite{Schonberger2013}.

Soluciones especificas para llevar a cabo el procesamiento de este tipo de información pueden servir durante una etapa inicial o exploratoria, pero cuando el volumen de información supera los límites estimados de forma continua, se plantea un compromiso entre la cantidad de datos disponibles y el volumen que efectivamente puede ser procesado. Esto esta limitado principalmente por la infraestructura disponible y los algoritmos seleccionados.

Una de las áreas donde la cantidad de datos no estructurados crece de forma acelerada es la recuperación de información relacionada con la web. En consecuencia, los procesos de RI se adaptaron para que trabajen de forma masivamente distribuida. MapReduce~\cite{Dean2004} es una respuesta a esto desde hace más de una década introduciéndose, de forma progresiva y conjunta, el término Big Data, para hacer referencia a este tipo de problemas, donde la cantidad de datos disponibles obliga a cambiar el enfoque y los algoritmos utilizados.

La Unión Internacional de Telecomunicaciones (ITU) define Big Data como ``Un paradigma para permitir la recolección, almacenamiento, gestión, análisis y visualización de conjuntos de datos masivos, de características heterogéneas, bajo restricciones potenciales de tiempo real''~\cite{ITU-T2015}.

Dentro del área de Big Data, MapReduce es considerado central~\cite{Rajaraman2011}. El software que implementa dicho modelo y que mayor adopción ha tenido por parte de la industria y la academia es Hadoop\footnote{\url{https://wiki.apache.org/hadoop/PoweredBy}. Compañías como Facebook y Yahoo aportan desarrolladores activamente al proyecto o proyectos relacionados, mientras que otras grandes organizaciones reportan tener clusters sobre los que ejecutan trabajos en Hadoop. Entre dichas organizaciones se puede encontrar a Adobe, Ebay, IBM y PARC entre otras}. MapReduce permite el tratamiento de grandes cantidades de datos en clusters de hardware dedicado y ``económico'' (\textit{commodity hardware}), haciendo uso de conceptos derivados de la programación funcional (map y reduce) y estrategias de paralelización y distribución de carga.

En el contexto de este trabajo, se utilizan herramientas y algoritmos usados en el ámbito de Big Data para resolver un problema de construcción de índices invertidos para motores de búsqueda. La combinación de estas dos áreas ya fue explorada previamente~\cite{Dean2004}~\cite{Lin2010}~\cite{Croft2010}. Sin embargo, el foco de este trabajo se encuentra en medir la escalabilidad de diferentes soluciones, variando parámetros tanto de los datos a procesar como de la plataforma. Además, en esta propuesta se asume y utiliza hardware económico usualmente encontrado en cualquier organización, diferenciándose significativamente de la utilizada en la gran mayoría de la literatura. Cabe destacar que al momento de llevar a cabo este trabajo, no se tiene información de que dicho enfoque exista.

\subsection{Commodity Hardware en un Contexto de Limitaciones}

El presente trabajo utiliza para los experimentos un cluster\footnote{Un cluster es una configuración de varios equipos que trabajan de forma coordinada a través de una red de datos. Cada equipo dentro del cluster es conocido como nodo. Un cluster suele ser construido como alternativa económica a utilizar equipos de super computo o super computadoras, donde la escalabilidad se realiza de forma vertical (es decir, se agrega mas cantidad de recursos de hardware al equipamiento disponible). Por el contrario, un cluster escala de forma horizontal. Esto significa que no se trata de agregar recursos a cada uno de los elementos del cluster, sino agregar mayor cantidad de nodos.} de equipos con especificaciones de hardware para usuarios de escritorio, conocidas como computadores personales o PC (por ejemplo, procesador Intel Core i5, 8 GB de memoria RAM y 400 GB de disco rígido). Este tipo de equipamiento es referido como commodity hardware.

Buscar la escalabilidad horizontal de la infraestructura no es algo original. Debido al desarrollo de la industria de hardware, es mas optimo en relación al costo tener muchos equipos pequeños y económicos unidos por una red, que construir grandes supercomputadoras. Esta tendencia se ve favorecida, entre otros aspectos, por el hecho de que los insumos para redes de alta velocidad son cada vez mas accesibles, y por el contrario, optimizar otros componentes, como el acceso a la memoria RAM en un mismo equipo, es cada vez mas costoso~\cite{Drepper2007}.

Es habitual en la literatura encontrar experiencias con clusters de características técnicas superiores a las descriptas anteriormente, ya sea por cantidad de nodos o hardware disponible por nodo. Por ejemplo, hardware de especificaciones mas modestas a nivel de cada nodo (2 GB de RAM y 300 GB de almacenamiento), pero disponibilidad de 80 nodos~\cite{Kimball2008}. Estas y otras configuraciones similares son habituales en la bibliografía que desarrolla temas de Big Data~\cite{Lin2011}.

También existe el enfoque de usar servicios basados en la nube, donde frente a la problemática de los límites de recursos, la solución es contratar mayor cantidad del recurso que cause el cuello de botella~\cite{Jiang2010}. En el caso de Gunther et. al~\cite{Gunther2015}, se reportan dos configuraciones para el estudio en cuestión. La primera configuración posee gran cantidad de memoria RAM (34,2 GB) pero poco espacio de almacenamiento y latencia de red moderada, y 4 CPUs para procesamiento. La segunda instancia está optimizada para cómputo (8 CPUs), posee baja cantidad de memoria principal (7 GB en total), gran cantidad de espacio de almacenamiento y baja latencia de red.

Las configuraciones descriptas anteriormente son completamente válidas y no se intenta expresar algo en otro sentido. Sin embargo, el punto de este trabajo consiste en estudiar un contexto donde escalar vertical u horizontalmente es complejo o directamente imposible. Frente a las limitaciones de almacenamiento, memoria o procesamiento, se opta por optimizar las configuraciones o algoritmos antes de elegir alternativas que impliquen incorporación de mayor cantidad de hardware. De esta manera, se considera que la plataforma es aprovechada de forma mas eficiente. Esto es opuesto a la tendencia clásica donde el enfoque de desarrollar sobre MapReduce, al ser simple y ``barato''~\cite{Mccreadie2009}, genera soluciones sub-optimas porque al alcanzar los limites, se soluciona escalando horizontalmente.

Existen varios motivos por los cuales utilizar MapReduce es económico y sencillo frente otras alternativas (como clusters de ``High Performance Computing'' o HPC por ejemplo). Estos son algunos de ellos~\cite{Katal2013}~\cite{White2015}:

\begin{itemize}
	\item Almacenamiento: Los nodos de un cluster para HPC suelen ser exclusivamente para computo, y la organización dispone de algún tipo de plataforma de almacenamiento de alta disponibilidad y con una red de altas velocidades que es un cuello de botella al transmitir los datos a cada uno de los nodos. Hadoop utiliza un concepto de localidad de datos, y consiste en que los nodos de computo son parte a su vez del almacenamiento del cluster, y esto genera que no se deba tener un servidor de almacenamiento dedicado.
	\item Interfaz de control de flujo de datos: MPI es muy flexible, y delega en el desarrollador el control del flujo de datos de la aplicación distribuida. MapReduce propone un esquema de flujo de datos predefinido, una representación uniforme de los mismos para el desarrollador y esto hace que sea menos flexible pero mas sencillo de desarrollar, al no tener que ocuparse de dicho flujo.
	\item Coordinación de tareas: El desarrollador de aplicaciones para HPC debe tener un control y conocimiento de como se coordinan las mismas. Un desarrollador de aplicaciones MapReduce no necesita entender esta coordinación, y en ningún caso debe implementarla o lidiar con su complejidad para hacer aplicaciones funcionales sobre Hadoop (Planificación, reserva de recursos, despliegue, recuperación ante errores), porque es responsabilidad del ``framework''. Solo debe implementar las interfaces necesarias que son sencillas de comprender. Esto, que es un modelo de desarrollo muy restringido~\cite{White2015} es a su vez una de las ventajas de la plataforma.
\end{itemize}

Los puntos anteriores muestran que los conocimientos de la plataforma de un desarrollador de aplicaciones MapReduce puede ser muy inferior a sus pares de HPC. El efecto de esto es que resulta mas económico encontrar o entrenar desarrolladores para Hadoop que para HPC. Resulta claro que un efecto negativo es que tener desarrolladores con un conocimiento superficial de la plataforma puede derivar en muchos casos en soluciones sub-optimas. A tal punto esto afecta al ecosistema, que existe toda un área de trabajo en propuestas de soluciones de optimización de tareas sobre Hadoop~\cite{Kim2013}.

White~\cite{White2015} hace diferencia entre hardware ``commodity'' y ``low-end''. La definición de ``commodity'' para White es referida a equipos con 2 procesadores octo-core, memoria principal con tamaños entre 64 y 512Gb y 12 a 24 discos de 1 a 4Tb cada uno.

La infraestructura utilizada el presente trabajo se asemeja mas a la definida como ``low-end'', que son equipos construidos con componentes baratos.

\section{Motivaciones del Trabajo}

Este trabajo tiene varias motivaciones, que combinan temas clásicos (como recuperación de información) con temas más novedosos (infraestructura de Big Data). A nivel académico, resulta motivador la exploración de una disciplina en pleno auge como lo es Big Data, y su combinación con la Recuperación de Información.

A nivel institucional, es una deuda empezar a generar conocimiento y experiencia en las herramientas y técnicas de Big Data, así como construir un cluster y utilizarlo de manera regular para investigación, y que permita medir las capacidades y limitaciones del mismo.

La mayor parte de la literatura consultada que utiliza Hadoop para construir índices invertidos, suele tomar en cuenta de forma marginal el problema de la escalabilidad de dichos algoritmos. Por ejemplo, se optimiza de forma sencilla el código base para no sobrecargar la entrada/salida de almacenamiento secundario o red~\cite{Mccreadie2009}. Hay casos donde se implementan patrones de diseño de codigo MapReduce que representan una mejora tangible en el flujo de información, pero no se proveen mediciones de dichas implementaciones~\cite{Lin2010}. En este trabajo se busca implementar un índice simple (tomado como baseline) y dos índices avanzados sobre Hadoop utilizando el framework MapReduce. 

\section{Objetivos del Trabajo} \label{sec:objetivosTrabajo}

El presente trabajo persigue una diversidad de objetivos. Los principales están vinculados a la investigación académica. También hay un conjunto de objetivos secundarios que plantean la generación y difusión de los conocimientos de este trabajo dentro de la Universidad, en el contexto de la carrera Licenciatura en Sistemas de Información y se plantea transferir parte del conocimiento adquirido como infraestructura aprovechable por parte de grupos académicos y de investigación.

\subsection{Objetivos Principales}

Como se estableció, un conjunto de objetivos principales son buscados en este trabajo, a saber:

\begin{itemize}
    \item Implementar algoritmos de indexación en un entorno distribuido de hardware económico y prestaciones limitadas.
    \item Probar el comportamiento de un cluster con plataformas usadas en Big Data en tareas intensivas de creación de índices.
    \item Medir la eficiencia de un algoritmo diseñado para MapReduce para procesar una colección de documentos con diferentes configuraciones de la plataforma.
\end{itemize}

\subsection{Objetivos Secundarios}

Los objetivos secundarios surgen a raíz de no contar con experiencia en Hadoop ni en tecnologías de Big Data, ni con la infraestructura necesaria para las pruebas. Se plantea poder construir un servicio basado en Hadoop, configurarlo adecuadamente, y dejarlo a disposición de la comunidad de investigación luego de finalizar el trabajo.

\begin{itemize}
    \item Construcción de un cluster Hadoop para desarrollar las pruebas con PCs convencionales.
    \item Establecer una configuración adecuada para el uso de recursos en la plataforma Hadoop. La complejidad de un sistema distribuido genera que la mejor optimización se logre mediante mediciones experimentales para determinar la configuración adecuada en base al hardware disponible y los procesos ejecutados.
\end{itemize}

\section{Estructura del Documento}

Este trabajo se encuentra organizado en capítulos. Estos capítulos están pensados para ordenar la exposición y darle un sentido. Los temas se abordan de forma iterativa. Es decir, se plantean de forma general. En la medida que se avanza en cada uno de ellos, los mismos son profundizados y desarrollados.

El capítulo \ref{ch:background} introduce las lineas teóricas y practicas sobre la que se soporta el trabajo. Aborda y explica en profundidad la Recuperación de Información y la construcción de índices como forma de organización de los motores de búsqueda. Explica ademas técnicas utilizadas para la compresión de datos, y se dan detalles generales de su uso dentro de las implementaciones realizadas para este trabajo. También se aborda la problemática de Big Data y se dan detalles del funcionamiento general de Hadoop.

El capítulo \ref{ch:implementaciones} describe en profundidad las implementaciones de constructores de índices implementados. Se realiza primero una explicación conceptual y al final se explica el formato físico de almacenamiento de dichos índices. Ademas, se describe como se utilizo la plataforma y Hadoop para que la implementación de los índices sea construida en base a la lógica de procesamiento de la plataforma y no hacer un uso forzado o poco practico de la misma.

El capítulo \ref{ch:experimentos} describe en profundidad las pruebas realizadas. Define la infraestructura construida y las configuraciones y optimizaciones realizadas al mismo. Describe y caracteriza las colecciones utilizadas para las pruebas. Y por ultimo define los experimentos que se realizaron, la entrada utilizada, los parámetros usados y los resultados medidos. Luego expone de forma exhaustiva los resultados. Primero se explican que criterios fueron utilizados para analizar los resultados, y luego, por cada experimento realizado, se exponen las métricas de forma detallada y se explican en el contexto del trabajo, se analiza si corresponde con las hipótesis de trabajo y se compara con los objetivos propuestas para corroborar su cumplimiento o no.

Al final, el capítulo de conclusiones~\ref{ch:conclusiones} aborda el análisis final sobre el cumplimiento de los objetivos del trabajo.