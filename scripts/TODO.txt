# Scripts faltantes, documentacion, etc...

## Script que genere estadisticas de coleccion

INPUT: Una coleccion en formato trec
OUTPUT: Un archivo con las estadisticas de la coleccion (json?)

### INPUT

La onda es que sea un archivo TREC. Deberia poder ser un Trec o TrecOL y que no
haya que indicarselo.

### Descripcion de las Estadisticas

Diferentes estadisticas necesitan recolectarse. A continuacion una descripcion:

 1. Tamaño de la coleccion (bytes)
 2. Cantidad de documentos
 3. Cantidad de terminos o tokens en la coleccion
 4. Cantidad de terminos unicos (Es identico a cantidad de terminos en el vocabulario)
    Debe tokenizar, stemmizar, etc...
 5. Cantidad de terminos unicos por documento

#### Detalles de calculo de metricas

1. Tamaño de la coleccion (bytes)

    Sencillo, la suma total de bytes, o el tamaño del archivo que contiene la coleccion

2. Cantidad de documentos

    Facil, contar todos los documentos. No sirve max(docId)+1 por que no se si hay saltos en la numeracion de la coleccion. Salvo que se utilice el TrecRenumerate script.

3. Cantidad de terminos o tokens en la coleccion

    Cada termino o token de la coleccion, sumando todas sus frecuencias de aparicion

4. Cantidad de terminos unicos (Es identico a cantidad de terminos en el vocabulario)
    Debe tokenizar, stemmizar, etc...

    Todos los termino en un conjunto, el size del conjunto.

5. Cantidad de terminos unicos por documento

    contar los pares (docId, termino).

### Politicas del tokenizador

Hay que implementar un tokenizador identico al que se implementa en

    com.tomasdel.tesis.indexer.commons.IndexTokenizer

El tokenizador define cual token es valido y cual no. Estrategia de tokenizacion:


def construirToken(token):
    token = trim(token)
    return Stemmer.stemWord(token)


def esTokenValido(token):
    if noEsStopWord(token) and tieneLargoAdecuado(token) and noEsNulo(token)
        return True
    return False


def tokenizar(textoToTokenize):
    tokensValidos = ListOfTokensValidos

    textoToTokenize = lowerCase(textoToTokenize)
    textoToTokenize = replace(textoToTokenize, caracteresNoAlfanumericos, " ")
    tokensCandidatos = split(textoToTokenize, " ")

    for token in tokensCandidatos:
        candidato = construirToken(token)
        if esTokenValido(candidato):
            agregar(tokensValidos, candidato)

    return tokensValidos
