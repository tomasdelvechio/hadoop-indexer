#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    crawler.py [-h,--help] [-v,--verbose] [--version] -d,--datasets-file ds.csv

DESCRIPTION

    Descarga archivos de metrica 'uso de cpu' desde una instalacion de ganglia.

EXAMPLES

    TODO: Show some examples of how to use this script.

AUTHOR

    Tomas Delvechio <tdelvechio@unlu.edu.ar>

LICENSE

    GPLv3

VERSION

    1.0.0
"""

import sys
import os
import traceback
import argparse
import time
import re

# Global parameters
fieldSeparator = ';'
hostFieldSeparator = ','

## Ganglia URL Builder
metrica = 'cpu_user'
gangliaBaseUrl = "http://cidetic.unlu.edu.ar/ganglia/graph.php?"
gangliaParameters = [   "c=stampy", "h={host}", "r=custom", "z=small",
                        "cs={start_time}", "ce={end_time}", "m={metrica}",
                        "ti=CPU%20User", "csv=1" ]
gangliaUrl = gangliaBaseUrl + '&'.join(gangliaParameters)

# Wget Builder
wgetBaseCommand = "wget --user {user} --password {password} '{url}' -O {output}"
outputDirectory = "./output"


def loadPruebas(dataset):
    """
    Levanta el archivo csv en forma de dic con todos los datos necesarios
    """
    pruebas = {}
    with open(dataset) as fh:
        for stringPrueba in fh:
            pruebaNombre, hosts, horaInicio, horaFin = stringPrueba.strip().split(fieldSeparator)
            pruebas[pruebaNombre] = {   'horaInicio': horaInicio,
                                        'horaFin': horaFin,
                                        'hosts': hosts.split(hostFieldSeparator) }

    return pruebas


def generarEnlaces(pruebas):
    """
    A partir del dic de pruebas genera los enlaces de descarga
    """

    for nombre,prueba in pruebas.iteritems():
        enlaces = buildEnlaces(prueba)
        pruebas[nombre]['enlaces'] = enlaces
    return pruebas


def buildEnlaces(prueba):
    """
    Construye el enlace a partir de los parametros de la prueba
    """
    enlaces = {}
    for host in prueba['hosts']:
        enlace = gangliaUrl.format( start_time=prueba['horaInicio'],
                                    end_time=prueba['horaFin'],
                                    metrica=metrica,
                                    host=host )
        enlaces[host] = enlace
    return enlaces


def generarScript(pruebas):
    """
    Genera las lineas wget y las almacena en un archivo
    """
    user='cidetic'
    password='p455w0rdcidetic4455'
    with open('descargarDatasets.sh', 'w') as script:
        script.write('mkdir -p {dir}\n'.format(dir=outputDirectory))
        for nombre,prueba in pruebas.iteritems():
            for host,enlace in prueba['enlaces'].iteritems():
                script.write(wgetBaseCommand.format(user=user, password=password, url=enlace, output=outputDirectory+'/'+nombre+'-'+host+'.csv')+'\n')


def main ():

    global args

    pruebas = loadPruebas(args.datasets_file)
    enlaces = generarEnlaces(pruebas)

    generarScript(enlaces)



if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(usage=globals()['__doc__'], version='1.0.0')
        parser.add_argument ('--verbose', action='store_true', default=False, help='verbose output')
        parser.add_argument('-d', '--datasets-file', action='store', help='Datasets File', required=True)
        args = parser.parse_args()
        if args.verbose: print time.asctime()
        main()
        if args.verbose: print time.asctime()
        if args.verbose: print 'TOTAL TIME IN MINUTES:',
        if args.verbose: print (time.time() - start_time) / 60.0
        sys.exit(0)
    except KeyboardInterrupt, e: # Ctrl-C
        raise e
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
