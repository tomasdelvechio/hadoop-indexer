#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    procesar-archivos.py [-h,--help] [-v,--verbose] [--version] --input-directory <input_directory>

DESCRIPTION

    Este script procesa una carpeta con salidas json de Ganglia (una por nodo y
    por experimento), y retorna una salida que mide diversas metricas de interes
    para la Tesis.

EXAMPLES

    TODO: Show some examples of how to use this script.

AUTHOR

    Tomas Delvechio <tdelvechio@unlu.edu.ar>

LICENSE

    GPLv3

VERSION

    1.0.0
"""

import sys
import os
import traceback
import argparse
import time
import re
import logging
import errno
import uuid
from itertools import chain


# Creacion de Logger
loglvl = logging.INFO
logger = logging.getLogger(__name__)
logger.setLevel(loglvl)
handler = logging.FileHandler('trace.log')
handler.setLevel(loglvl)
#handler.setLevel(logging.WARNING)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# logLine Format: {nombre experimento} - {jobID} - {inicio job} - {fin job}
#logLine = '{nombre} - {id} - {start} - {end}'
#logger.info(logLine.format( nombre=nombreExperimento,
#                            id=candidateJob,
#                            start=inicio,
#                            end=fin))

## Other globals vars

FASES = ('m', 'r')

archivosReporte = 5000 # Cantidad de archivos para que muestre informe via log cuando no es debug

basePrompt = ">"
jobPrompt = basePrompt*2
taskPrompt = basePrompt*3
taskAttemptPrompt = basePrompt*4


# Object model


class JobManager:
    """Contiene todos los jobs de una secuencia de corridas Hadoop, y permite
        gestionarlos y realizar operaciones en lotes sobre los mismos"""

    def __init__(self):
        self.jobs = {}


    def addJob(self, aJob):
        """Agrega un job al manager"""
        if self.esNuevoJob(aJob):
            self.jobs[aJob.getId()] = aJob


    def countJobs(self):
        """Retorna la cantidad de jobs actualmente en el manager"""
        return len(self.jobs)


    def esNuevoJob(self, aJob):
        """Verifica si es un nuevo Job o ya existe en el manager"""
        return aJob.getId() not in self.jobs


    def createObjectsFromMetadata(self, metadata):
        """Metodo que hace magia a partir de la metada proporcionada por un
            filename. Esto evita tener que hacer muchas instancias de objetos
            a mano y el manager se encarga de encapsular toda la logica
            necesaria para crear el arbol de objetos."""
        logger.debug("%s Creando objetos" % basePrompt)
        job = self.createJobFromMetadata(metadata)
        task = self.createTaskFromMetadata(job, metadata)
        taskAttempt = self.createTaskAttemptFromMetadata(task, metadata)
        logger.debug("%s Objetos Creados" % basePrompt)


    def createJobFromMetadata(self, metadata):
        """Metodo que construye un objeto Job a partir de la metadata recibida.
            Si el Job ya existe en el manager, retorna el objeto en cuestion y
            no lo crea nuevamente"""
        jobid = metadata["jobid"]
        logger.debug('%s Creando Job %s' % (jobPrompt, jobid))
        if self.existe(jobid=jobid):
            job = self.getJob(jobid)
            logger.debug('%s Job %s ya existe' % (jobPrompt, jobid))
        else:
            experimento = metadata["experimento"]
            job = Job(jobid=jobid, experimento=experimento)
            self.addJob(job)
            logger.debug('%s Job %s creado correctamente' % (jobPrompt, jobid))

        return job


    def createTaskFromMetadata(self, job, metadata):
        taskid = metadata["taskid"]
        logger.debug('%s Creando Task %s' % (taskPrompt, taskid))

        if job.existe(taskid=taskid):
            task = job.getTask(taskid)
            logger.debug('%s Task %s ya existe en Job %s' % (taskPrompt, taskid, job.getId()))
        else:
            task = Task(taskid=taskid, job=job, fase=metadata["fase"])
            job.addTask(task)
            logger.debug('%s Task %s creada correctamente en Job %s' % (taskPrompt, taskid, job.getId()))
        return task


    def createTaskAttemptFromMetadata(self, task, metadata):
        taskattemptid = metadata["taskattemptid"]
        logger.debug('%s Creando Task Attemp %s' % (taskAttemptPrompt, taskattemptid))

        if task.existe(taskattemptid=taskattemptid):
            taskAttempt = task.getTaskAttempt(taskattemptid)
            logger.debug('%s TaskAttemp %s Ya existe en Task %s' % (taskAttemptPrompt, taskattemptid, task.getId()))
        else:
            nodo = metadata["nodo"]
            taskAttempt = TaskAttempt(taskattemptid=taskattemptid, nodo=nodo, task=task)
            task.addTaskAttempt(taskAttempt)
            logger.debug('%s TaskAttemp %s creada correctamente en Task %s' % (taskAttemptPrompt, taskattemptid, task.getId()))

        return taskAttempt


    def getJob(self, jobid):
        try:
            return self.jobs[jobid]
        except Exception:
            logger.error("El job %s no existe" % jobid)
        return None


    def existe(self, jobid=None, taskid=None, taskattemptid=None):
        """Controla si un job, task o attempt ya existe o es nuevo"""
        if (jobid is not None):
            return jobid in self.jobs

        if (taskid is not None):
            taskExiste = False
            for jobName, job in self.jobs.items():
                taskExiste = job.existe(taskid=taskid)
                if taskExiste:
                    break
            return taskExiste

        if (taskattemptid is not None):
            taskAttemptExiste = False
            for jobName, job in self.jobs.items():
                taskAttemptExiste = job.existe(taskattemptid=taskattemptid)
                if taskAttemptExiste:
                    break
            return taskAttemptExiste


    def procesarArchivo(self, filename):
        metadata = self.getMetadata(filename)
        self.createObjectsFromMetadata(metadata)
        #logLine = '{prompt} Procesando archivo {filename}'
        #logger.info(logLine.format( prompt=basePrompt,
        #                            filename=filename))
        self.getDataFromFile(filename, metadata)


    def getMetadata(self, filename):
        """Toma la metadata a partir del nombre del archivo"""
        campos = filename.split('-')
        experimento = '-'.join(campos[:6])
        nodo = campos[-1].split('.')[0] # Elimina el .csv
        md = {  "experimento": experimento,
                "jobid": "-".join([campos[6], campos[7]]),
                "fase": campos[8],
                "taskid": "-".join([campos[8], campos[9]]),
                "taskattemptid": campos[10],
                "nodo": nodo }
        return md


    def getFileContent(self, directory, filename):
        return open(os.path.join(directory,filename)).read()


    def getDataFromFile(self, filename, metadata):
        global args
        fileContent = self.getFileContent(args.input_directory,filename)
        taskAttempt = self.getJob(metadata['jobid'])\
                            .getTask(metadata['taskid'])\
                            .getTaskAttempt(metadata['taskattemptid'])
        taskAttempt.setContent(fileContent)


    def logStats(self):
        """Resumen del contenido del manager, principalmente para propositos de
            debug y control"""
        logline = "{prompt} Procesando {type} {id} ({counter}) - Registros: {registros}"
        logger.debug("=== ESTADISTICAS DEL MANAGER ========================")

        jobs = 0
        for jobid, job in self.jobs.items():
            logger.debug(logline.format(prompt=jobPrompt, type="Job", id=jobid, counter=jobs, registros=job.getCantidadRegistros()))
            tasks = 0
            jobs += 1
            for taskid, task in job.tasks.items():
                logger.debug(logline.format(prompt=taskPrompt, type="Task", id=taskid, counter=tasks, registros=task.getCantidadRegistros()))
                taskAttempts = 0
                tasks += 1
                for taskattemptid, taskAttempt in task.taskAttempts.items():
                    taskAttempts += 1
                    logger.debug(logline.format(prompt=taskAttemptPrompt, type="TaskAttempt", id=taskattemptid, counter=taskAttempts, registros=taskAttempt.getCantidadRegistros()))


class Job:


    def __init__(self, jobid=None, experimento=None):
        self.tasks = {}
        self.jobid = jobid
        self.experimento = experimento
        self.cantidadRegistros = 0


    def setJobId(self, jobid):
        self.jobid = jobid
        return self


    def getId(self):
        return self.jobid


    def setExperimento(self, experimento):
        self.experimento = experimento
        return self


    def addTask(self, aTask):
         if self.esNuevaTask(aTask):
            self.tasks[aTask.getId()] = aTask


    def esNuevaTask(self, aTask):
        """Verifica si es una nueva Task o ya existe en el job"""
        return aTask.getId() not in self.tasks


    def countTasks(self):
        return len(self.tasks)


    def existe(self, taskid=None, taskattemptid=None):
        """Controla si task o attempt ya existe o es nuevo"""
        if (taskid is not None):
            logger.debug("%s Verificando si Task %s existe en Job %s: %s" % (taskPrompt, taskid, self.jobid, taskid in self.tasks))
            return taskid in self.tasks

        if (taskattemptid is not None):
            taskAttemptExiste = False
            for taskName, task in self.tasks.items():
                taskAttemptExiste = task.existe(taskattemptid=taskattemptid)
                if taskAttemptExiste:
                    break
            return taskAttemptExiste


    def getTask(self, taskid):
        try:
            return self.tasks[taskid]
        except Exception:
            logger.error("La task %s no existe" % taskid)
        return None


    def getNodos(self):
        """Retorna todos los nodos involucrados en la realizacion de este job"""
        nodosJob = []
        for task in self.tasks:
            for nodo in task.getNodos():
                if nodo not in nodosJob:
                    nodosJob.append(nodo)


    def addCantidadRegistros(self, cantidadRegistros):
        self.cantidadRegistros += cantidadRegistros


    def getCantidadRegistros(self):
        """Recorre todas las tasks y retorna la cantidad de registros del Job"""
        registros = 0
        for taskId, task in self.tasks.items():
            registros += task.getCantidadRegistros()
        return registros


    def getRegistros(self):
        """Devuelve todos los registros relacionados con el job actual"""
        registros = []
        for taskId, task in self.tasks.items():
            registros += task.getRegistros()
        return registros


    def getRegistrosPorFase(self):
        registros = {key: [] for key in FASES}
        for fase in FASES:
            registros[fase] += list(set(list(chain.from_iterable([task.getRegistros() for taskid, task in self.tasks.items() if task.fase == fase]))))
        return registros


    def getFilename(self, extension='.csv', suffix=None):
        """Construye un filename para el presente job"""
        partes = [self.experimento, self.jobid]
        if suffix is not None:
            partes += [suffix]
        return '-'.join(partes) + extension


class Task:


    def __init__(self, taskid=None, fase=None, job=None):
        self.taskAttempts = {}
        self.taskid = taskid
        self.fase = fase
        self.job = job


    def setTaskId(self, taskid):
        self.taskid = taskid
        return self


    def getId(self):
        return self.taskid


    def getFase(self):
        return self.fase


    def setFase(self, fase):
        self.fase = fase
        return self


    def addTaskAttempt(self, aTaskAttempt):
        if self.esNuevaTaskAttempt(aTaskAttempt):
            self.taskAttempts[aTaskAttempt.getId()] = aTaskAttempt


    def esNuevaTaskAttempt(self, aTaskAttempt):
        """Verifica si es una nueva Task o ya existe en el job"""
        return aTaskAttempt.getId() not in self.taskAttempts


    def countTaskAttempts(self):
        return len(self.taskAttempts)


    def existe(self, taskattemptid=None):
        """Controla si attempt ya existe o es nuevo"""
        if (taskattemptid is not None):
            logger.debug("%s Verificando si TaskAttempt %s existe en Task %s: %s" % (taskAttemptPrompt, taskattemptid, self.taskid, taskattemptid in self.taskAttempts))
            return taskattemptid in self.taskAttempts


    def getTaskAttempt(self, taskattemptid):
        try:
            return self.taskAttempts[taskattemptid]
        except Exception:
            logger.error("La task attempt %s no existe" % taskattemptid)
        return None


    def getNodos(self):
        """Retorna todos los nodos involucrados en la realizacion de esta task"""
        nodos = []
        for taskAttemptId, taskAttempt in self.taskAttempts.items():
            for nodo in taskAttempt.getNodo():
                if nodo not in nodos:
                    nodos.append(nodo)
        return nodos


    def getCantidadRegistros(self):
        """Recorre todos los taskAttempts y retorna la cantidad de registros de
            la task"""
        registros = 0
        for taskAttemptId, taskAttempt in self.taskAttempts.items():
            registros += taskAttempt.getCantidadRegistros()
        return registros


    def getRegistros(self):
        """Devuelve todos los registros relacionados con la task actual"""
        registros = []
        for taskAttemptId, taskAttempt in self.taskAttempts.items():
            registros += taskAttempt.getRegistros()
        return registros


    def getFilename(self, extension='.csv'):
        """Construye un filename para la presente task"""
        return '-'.join((self.job.getFilename(extension=''), self.taskid, self.fase)) + extension


class TaskAttempt:


    def __init__(self, taskattemptid=None, nodo=None, task=None):
        self.taskattemptid = taskattemptid
        self.nodo = nodo
        self.content = {}
        self.task = task


    def getId(self):
        return self.taskattemptid


    def getJobId(self):
        """Retorna el id del job al cual pertenece la TaskAttempt"""
        return self.task.job.getId()


    def getNodo(self):
        return self.nodo


    def setContent(self, fileContent):
        """Recibe un string con el contenido del archivo y lo almacena.
            Ignora la primer linea (El nombre de las columna), y la ultima linea
            que es una linea en blanco."""
        registros = fileContent.split('\n')[1:-1]
        if len(registros) > 0:
            for registro in registros:
                if registro not in self.content:
                    self.content[registro] = None
            self.task.job.addCantidadRegistros(self.getCantidadRegistros())


    def getCantidadRegistros(self):
        return len(self.content)


    def getRegistros(self):
        """Retorno los registros del attempt actual"""
        return self.content.keys()


    def getFilename(self, extension='.csv'):
        """Construye un filename para el presente taskAttempt"""
        return '-'.join((self.task.getFilename(extension=''), self.taskattemptid, self.nodo)) + extension


class OutputGenerator:
    """Clase que recibe un JobManager y genera salidas csv en base a diferentes
        cortes de control (por job, por task, por fase)"""


    def __init__(self, aJobManager, mode='job', dirname='output'):
        self.mode = mode
        self.manager = aJobManager
        self.salidasPorJobNodo = {}

        # Create safe output folder
        try:
            os.makedirs(dirname)
        except OSError as e:
            if e.errno == errno.EEXIST:
                # pseudo - safe string generator
                suffix = str(uuid.uuid4()).split('-')[0]
                dirname = os.path.normpath(dirname) + suffix
                os.makedirs(dirname)
            else:
                raise e

        self.dirname = dirname


    def crearSalida(self, item):
        """Genera un archivo de salida de acuerdo al tipo del item"""
        filename = item.getFilename()
        pathFile = os.path.join(self.dirname, filename)
        fh = open(pathFile, 'w')
        # Linea copiada tal cual sale en los csv de GANGLIA
        fh.write("Timestamp,%s last custom   \n" % item.getId())
        return fh


    def crearSalidaPorFase(self, item):
        salidas = {}
        for fase in FASES:
            filename = item.getFilename(suffix=fase)
            pathFile = os.path.join(self.dirname, filename)
            fh = open(pathFile, 'w')
            # Linea copiada tal cual sale en los csv de GANGLIA
            fh.write("Timestamp,%s last custom   \n" % item.getId())
            salidas[fase] = fh
        return salidas


    def crearSalidaPorJobNodo(self, job, nodo):
        """Crea una salida por cada combinacion Job + Nodo. Si la salida fue
            previamente creada no la crea, la devuelve."""
        jobid = job.getId()
        key = jobid+nodo
        if key not in self.salidasPorJobNodo:
            filename = job.getFilename(suffix=nodo)
            pathFile = os.path.join(self.dirname, filename)
            fh = open(pathFile, 'w')
            fh.write("Fase,Timestamp,%s last custom   \n" % job.getId())
            self.salidasPorJobNodo[key] = fh
        return self.salidasPorJobNodo[key]


    def registrosParaGuardar(self, registros):
        """Realiza las validaciones necesarias para saber si hay registros
            que deban ser escritos a disco"""
        return len(registros) > 0


    def escribirRegistros(self, registros, fileHandler):
        """Escribe los registros al disco"""
        fileHandler.write('\n'.join(sorted(registros)))
        fileHandler.close()


    def borrarArchivo(self, fileHandler):
        """Borra un archivo de salida creado"""
        fileHandler.close()
        os.remove(fileHandler.name)


    def salidaPorJob(self):
        """Genera csv's agrupados por un mismo job"""
        for jobid, job in self.manager.jobs.items():
            registros = job.getRegistros()
            fileHandler = self.crearSalida(job)
            fileHandler.write('\n'.join(registros))
            fileHandler.close()

        print("Salida generada en %s" % self.dirname)


    def salidaPorFase(self):
        """Genera csv's agrupados por job y fase"""
        for jobid, job in self.manager.jobs.items():
            salidas = self.crearSalidaPorFase(job)
            registros = job.getRegistrosPorFase()
            for fase,archivo in salidas.items():
                if (self.registrosParaGuardar(registros[fase])) :
                    self.escribirRegistros(registros[fase], archivo)
                else:
                    self.borrarArchivo(archivo)
        print("Salida generada en %s" % self.dirname)


    def salidaPorNodo(self):
        """Genera csv's agrupados por job y nodo, agrega la fase como columna"""
        for jobid, job in self.manager.jobs.items():
            registros = {}
            for taskid, task in job.tasks.items():
                fase = task.getFase()
                for taskattemptid, taskAttempt in task.taskAttempts.items():
                    _registros = taskAttempt.getRegistros()
                    # Agregar la fase como columna de los registros y estos al
                    #   dic discrimando por nodo
                    nodo = taskAttempt.getNodo()
                    try:
                        registros[nodo] += [fase + "," + registro for registro in _registros if fase + "," + registro not in registros[nodo]]
                    except KeyError as ke:
                        registros[nodo] = [fase + "," + registro for registro in _registros]

            # Guardar los registros en la salida
            for nodo, registrosNodo in registros.items():
                salida = self.crearSalidaPorJobNodo(job, nodo)
                if (self.registrosParaGuardar(registrosNodo)) :
                    self.escribirRegistros(registrosNodo, salida)
                else:
                    self.borrarArchivo(salida)

        print("Salida generada en %s" % self.dirname)

# Formato del nombre de archivo para extraer la metadata
#   BL-B-CC-4N-1R-C2-1481393351385-0022-r-000000-0-slave4.csv
#
#
#   experimento     BL-B-CC-4N-1R-C2
#   Job Id          1481393351385-0022
#   Fase            r
#   TaskId          000000
#   TaskAttempId    0
#   Nodo            slave4
#   Formato         .csv

# Que hace este script?
#
#   Resumen de stats:
#       - Cantidad de jobs
#       - Por cada job:
#           - Job Id
#           - Cantidad de puntos muestrales fase Map
#           - Cantidad de puntos muestrales fase Reduce
#           - Cantidad de nodos involucrados
#           - Lista de nodos involucrados


def main ():

    global args, archivosReporte

    jobManager = JobManager()
    _, _, files = next(os.walk(args.input_directory))
    cantidadArchivos = len(files)
    archivosProcesados = 0

    logger.info("===== Start Processing ================================")
    for filename in os.listdir(args.input_directory):
        archivosProcesados += 1
        logger.debug("Procesando archivo %s/%s: %s" % (archivosProcesados, cantidadArchivos, filename))
        if archivosProcesados % archivosReporte == 0:
            logger.info("Procesando archivo %s/%s: %s" % (archivosProcesados, cantidadArchivos, filename))
        try:
            jobManager.procesarArchivo(filename)
        except Exception as e:
            logger.error("Error procesando archivo " +  filename)
            logger.error(e)
            traceback.print_exc()
        logger.debug("Archivo procesado: %s" % filename)

    # stats para testing
    jobManager.logStats()
    jobsTotales = jobManager.countJobs()
    logger.info("Files / Jobs: %s/%s" % (archivosProcesados, jobsTotales))

    # Aca se pueden empezar a computar las stats, porque fueron procesados todos
    #   los archivos.
    generator = OutputGenerator(jobManager, dirname='salida')
    if args.corte == 'job':
        generator.salidaPorJob()
    elif args.corte == 'fase':
        generator.salidaPorFase()
    elif args.corte == 'nodo':
        generator.salidaPorNodo()

if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(usage=globals()['__doc__'])
        parser.add_argument ('--verbose', action='store_true', default=False, help='verbose output')
        parser.add_argument('-i', '--input-directory', action='store', help='Input File with Ganglia files', required=True)
        parser.add_argument('-c', '--corte', action='store', help='Criterio para corte de control', default='job')
        args = parser.parse_args()
        if args.verbose: print(time.asctime())
        main()
        if args.verbose: print(time.asctime())
        if args.verbose: print('TOTAL TIME IN MINUTES:',)
        if args.verbose: print((time.time() - start_time) / 60.0)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
