#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script para convertir la salida del script procesar-archivos.py a un formato
csv separado por tabs.

Lo que hace este script es tomar la carpeta de salida del script
procesar-archivos.py y arma un nuevo csv unico con todas las columnas necesarias
para poder procesar los datos.
"""

import sys
import os
from operator import itemgetter
from collections import defaultdict

OUTPUT_PREFIX = '40-'

def getMetadata(filename):
	"""Toma la metadata a partir del nombre del archivo"""
	campos = filename.split('-')
	experimento = '-'.join(campos[:6])
	nodo = campos[-1].split('.')[0] # Elimina el .csv
	md = {  "experimento": experimento,
			"jobid": "-".join([campos[6], campos[7]]),
			"nodo": nodo }
	return md

def calcularImbalance(maximo, promedio):
	return ((maximo / float(promedio)) - 1) * 100

if len(sys.argv) < 2:
	print("Falta el path para el directorio a ser procesado")
	sys.exit(1)
else:
	directory = sys.argv[1]

# Raw output
outputFile = open(OUTPUT_PREFIX + 'salida-nueva.csv', 'w')
cabeceraArchivo = ['Experimento', 'Nodo', 'Job', 'Fase', 'Timestamp', 'CPU']
datosArchivo = []

# Stat output
#statsFile = open(OUTPUT_PREFIX + 'estadisticas.csv', 'w')
#cabeceraArchivoStatsPromedio = ['Experimento', 'Nodo', 'Job', 'Fase', 'Promedio', 'Cantidad muestras']
#cabeceraArchivoStatsImbalance = ['Experimento', 'Nodo', 'Job', 'Fase', 'Imbalance']
cabeceraArchivoStatsImbalanceExperimento = ['Experimento', 'Fase', 'Avg Imbalance']

# Experimentos procesados
expFile = open(OUTPUT_PREFIX + 'experimentos.csv', 'w')
experimentos = []


for filename in os.listdir(directory):
	filePath = os.path.join(directory, filename)
	metadata = getMetadata(filename)
	fileData = open(filePath).read()
	fileData = fileData.split('\n')[1:]
	for registros in fileData:
		if not registros:
			continue
		datos = registros.split(',')
		datos = [metadata['experimento'], metadata['nodo'], metadata['jobid']] + datos
		datosArchivo += [datos]

		# Recopilacion de experimentos
		if metadata['experimento'] not in experimentos:
			experimentos.append(metadata['experimento'])

experimentos = sorted(experimentos)
expFile.write('\n'.join(experimentos))
expFile.close()
print("Creado archivo 40-experimentos.csv")

# Ordenar los datos por experimento, nodo y fase
datosArchivoOrdenado = sorted(datosArchivo, key=itemgetter(0,1,3))
datosArchivoString = [] # Copia para ser guardada en el archivo, son los campos unidos en un solo string

# Uso de CPU por experimento, fase y nodo
cpu = {}
for registro in datosArchivoOrdenado:
	experimento, nodo, jobid, fase, ts, usoCpu = registro

	if experimento not in cpu:
		cpu[experimento] = {}

	if fase not in cpu[experimento]:
		cpu[experimento][fase] = {}

	if nodo not in cpu[experimento][fase]:
		cpu[experimento][fase][nodo] = []

	cpu[experimento][fase][nodo] += [float(usoCpu)]

#print(repr(cpu))

avg = {}
maxAvg = {}
for experimento, datosExperimento in cpu.items():
	for fase, datosFase in datosExperimento.items():

		# Promedios por exp, fase y nodo
		for nodo, datosNodo in datosFase.items():

			if experimento not in avg:
				avg[experimento] = {}

			if fase not in avg[experimento]:
				avg[experimento][fase] = {}

			avg[experimento][fase][nodo] = sum(datosNodo) * 1.0 / len(datosNodo)

		# Maximo por experimento, fase
		if experimento not in maxAvg:
			maxAvg[experimento] = {}

		maxAvg[experimento][fase] = max(avg[experimento][fase].values())

#print(repr(avg))
#print(repr(maxAvg))

# Imbalance por experimento / fase / Nodo
imbalance = {}
for experimento, datosExperimento in avg.items():
	for fase, datosFase in datosExperimento.items():
		for nodo, datosNodo in datosFase.items():

			if experimento not in imbalance:
				imbalance[experimento] = {}

			if fase not in imbalance[experimento]:
				imbalance[experimento][fase] = {}

			maximo = maxAvg[experimento][fase]
			promedio = avg[experimento][fase][nodo]
			imbalance[experimento][fase][nodo] = calcularImbalance(maximo, promedio)

#print(repr(imbalance))

# Imbalance promedio por experimento / fase
avgImbalance = {}
print('\t'.join(cabeceraArchivoStatsImbalanceExperimento))
for experimento, datosExperimento in imbalance.items():
	for fase, datosFase in datosExperimento.items():

		if experimento not in avgImbalance:
			avgImbalance[experimento] = {}
		# if experimento == 'BL-B-CC-2N-1R-C1':
		# 	import pdb; pdb.set_trace()
		avgImbalance[experimento][fase] = sum(datosFase.values()) * 1.0 / len(datosFase.values())
		print('\t'.join((experimento, fase, str(avgImbalance[experimento][fase]))))















############# CALCULO VIEJO

# Calcular imbalance por nodo, fase y experimento
# faseActual = None
# nodoActual = None
# jobidActual = None
# experimentoActual = None
# cpuFaseActual = []
# estadisticas = []
# #maxValorPorFase = { 'm': 0, 'r': 0 }
# maxValorPorFase = {}
# for datos in datosArchivoOrdenado:
# 	experimento, nodo, jobid, fase, ts, cpu = datos
# 	if (faseActual == fase) and (nodoActual == nodo) and (jobid == jobidActual):
# 		cpuFaseActual.append(float(cpu))
# 	else:
# 		if cpuFaseActual:
# 			promedioCpu = sum(cpuFaseActual) / float(len(cpuFaseActual))
# 			estadisticas.append([experimentoActual, nodoActual, jobidActual, faseActual, str(promedioCpu), str(len(cpuFaseActual))])

# 			# Establece el max value por experimento y fase
# 			if experimento in maxValorPorFase:
# 				maxValorPorFase[experimento][fase] = max(promedioCpu, maxValorPorFase[experimento][fase])
# 			else:
# 				maxValorPorFase[experimento] = { 'm': 0, 'r': 0 }
# 				maxValorPorFase[experimento][fase] = promedioCpu

# 		faseActual = fase
# 		nodoActual = nodo
# 		jobidActual = jobid
# 		experimentoActual = experimento
# 		cpuFaseActual = [float(cpu)]

# 	datosArchivoString += ['\t'.join(datos)]

# # Toma estadisticas para el calculo del imbalance por experimento y fase
# imbalance = {}
# for experimento, nodo, jobid, fase, promedioCpu, cantidad in estadisticas:
# 	imbalanceNodo = calcularImbalance(maxValorPorFase[experimento][fase],promedioCpu)
# 	if not experimento in imbalance:
# 		imbalance[experimento] = { 'm': [], 'r': [] }
# 	imbalance[experimento][fase] += [imbalanceNodo]

# # Imbalance promedio por fase
# avgImbalance = {}
# avgXFase = {'m': 0, 'r': 0 }
# for experimento in imbalance.keys():
# 	for fase in imbalance[experimento].keys():
# 		if not experimento in avgImbalance:
# 			avgImbalance[experimento] = { 'm': 0, 'r': 0 }
# 		if len(imbalance[experimento][fase]) == 0:
# 			avgImbalance[experimento][fase] = 0
# 		else:
# 			avgImbalance[experimento][fase] = sum(imbalance[experimento][fase]) * 1.0 / len(imbalance[experimento][fase])

# # Print stats
# statsFile.write("=== Estadisticas ==========\n")
# statsFile.write("--- Promedio por Experimento - Nodo - Fase -----------\n")
# statsFile.write('\t'.join(cabeceraArchivoStatsPromedio))
# statsFile.write('\n')
# statsFile.write('\n'.join(['\t'.join([experimento, nodo, jobid, fase, promedioCpu, cantidad]) for experimento, nodo, jobid, fase, promedioCpu, cantidad in estadisticas]))

# statsFile.write("\n--- Max valor por Fase -----------\n")
# statsFile.write(repr(maxValorPorFase))

# statsFile.write("\n--- Imbalance por nodo -----------\n")
# statsFile.write('\t'.join(cabeceraArchivoStatsImbalance))
# statsFile.write('\n')
# #statsFile.write('\n'.join(['\t'.join([experimento, nodo, jobid, fase, str(calcularImbalance(maxValorPorFase[experimento][fase],promedioCpu)) ]) for experimento, nodo, jobid, fase, promedioCpu, cantidad in estadisticas]))
# for nombre, experimento in imbalance.items():
# 	for fase, valor in experimento.items():
# 		statsFile.write('\t'.join([nombre, fase, str(valor)]))
# 		statsFile.write('\n')

# # statsFile.write("\n--- Imbalance promedio por experimento -----------\n")
# # statsFile.write('\t'.join(cabeceraArchivoStatsImbalanceExperimento))
# # statsFile.write('\n')
# #statsFile.write('\n'.join(['\t'.join([nombre, fase, str(sum(imbalances) / float(len(imbalances))) if len(imbalances) > 0 else "0"]) for nombre, experimento in imbalance.items() for fase,imbalances in experimento.items()]))
# statsFile.write(repr(avgImbalance))

# statsFile.write("\n--- Imbalance por experimento -----------\n")
# statsFile.write('\t'.join(cabeceraArchivoStatsImbalanceExperimento))
# statsFile.write('\n')

# #import pdb; pdb.set_trace();
# for nombre, experimento in avgImbalance.items():
# 	for fase, valor in experimento.items():
# 		statsFile.write('\t'.join([nombre, fase, str(valor)]))
# 		statsFile.write('\n')

# #statsFile.write('\n'.join(['\t'.join([[nombre, fase, valor] for nombre, experimento in avgImbalance.items() for fase,valor in experimento.items()])]))
# statsFile.close()
# print("Creado archivo 40-estadisticas.csv")

# # Convertir cada registro a su version joined con tabs, luego escribir datos en
# #	el archivo final.
# datosArchivoString = ['\t'.join(cabeceraArchivo)] + datosArchivoString
# outputFile.write('\n'.join(datosArchivoString))
# outputFile.close()
# print("Creado archivo 40-salida-nueva.csv")



