import sys
import operator

hist = {}

with open(sys.argv[1]) as f:
    for line in f:
        term,postingList = line.strip().split('\t')
        # Cuenta bytes
        #hist[term] = len(postingList)
        # Cuenta cantidad de postings (1)
        #hist[term] = len(postingList.split(';'))
        # Cuenta cantidad de postings (2)
        hist[term] = postingList.count(';')

k = max(hist.iteritems(), key=operator.itemgetter(1))[0]
print "Clave: ", k, " -> Valor: ", hist[k]

