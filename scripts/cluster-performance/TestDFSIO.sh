#!/bin/bash
usage(){
	echo "Uso: $0 --nrFiles <number of files> --fileSize <size of each file (bytes)>"
	exit 1
}

# Si no pasa parametros, muestro la forma de uso
[[ $# -eq 0 ]] && usage


while [[ $# > 1 ]]
do
key="$1"

case $key in
    -nf|--nrFiles)
    NRFILES="$2"
    shift # past argument
    ;;
    -fs|--fileSize)
    FILESIZE="$2"
    shift # past argument
    ;;
    --default)
    NRFILES=100
    FILESIZE=10000
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done


#echo NUMBER OF FILES  = "${NRFILES}"
#echo SIZE OF EACH FILE     = "${FILESIZE}"

hadoop jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.5.2-tests.jar TestDFSIO -write -nrFiles $NRFILES -fileSize $FILESIZE
hadoop jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.5.2-tests.jar TestDFSIO -read -nrFiles $NRFILES -fileSize $FILESIZE
hadoop jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.5.2-tests.jar TestDFSIO -clean -nrFiles $NRFILES -fileSize $FILESIZE

