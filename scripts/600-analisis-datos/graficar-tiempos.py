import sys
import os
import csv
import matplotlib.pyplot as plt


def loadDataset(datasetPath):
	dataset = []
	with open(datasetPath) as filedata:
		reader = csv.DictReader(filedata, delimiter='\t')
		#reader = csv.reader(filedata, delimiter='\t')
		for row in reader:
			dataset += [row]
	return dataset[0], dataset[1:]


def getValueFromIndex(nombre, indice):
	return nombre.split('-')[indice]

def agruparPorIndice(dataset, indice):
	grupos = {}
	for registro in dataset:
		grupo = getValueFromIndex(registro["Experimento"], indice)

		if grupo not in grupos:
			grupos[grupo] = []

		grupos[grupo] += [registro["Tiempo"]]

#def getMatriz(headers, dataset, ejex, ejey):

# Construir una funcion que genere listas por indice del nombre del experimento.
#	Por ejemplo, si se le pasa 5, que agrupe los tiempos por los diferentes C1, C2 y C3
# Indices
#  1 2  3  4  5  6
# BL B CC 4N 2R C1

headers, dataset = loadDataset(sys.argv[1])
registros = agruparPorIndice(dataset, 5)
#print(repr(headers), repr(dataset))
#matriz = getMatriz(headers, dataset)

plt.plot([1,2,3,4], [1,4,9,16], 'ro')
plt.axis([0, 6, 0, 20])
plt.show()
