# Listado de pruebas a ejecutarse

BL	B	SC	0N	1R	C1
BL	B	SC	0N	1R	C2
BL	B	SC	0N	1R	C3
BL	B	CC	0N	1R	C1
BL	B	CC	0N	1R	C2
BL	B	CC	0N	1R	C3

BL	B	SC	1N	1R	C1
BL	B	SC	1N	1R	C2
BL	B	SC	1N	1R	C3
BL	B	CC	1N	1R	C1
BL	B	CC	1N	1R	C2
BL	B	CC	1N	1R	C3

BL	B	SC	2N	1R	C1
BL	B	SC	2N	1R	C2
BL	B	SC	2N	1R	C3
BL	B	CC	2N	1R	C1
BL	B	CC	2N	1R	C2
BL	B	CC	2N	1R	C3
BL	B	SC	2N	2R	C1
BL	B	SC	2N	2R	C2
BL	B	SC	2N	2R	C3
BL	B	CC	2N	2R	C1
BL	B	CC	2N	2R	C2
BL	B	CC	2N	2R	C3

BL	B	SC	4N	1R	C1
BL	B	SC	4N	1R	C2
BL	B	SC	4N	1R	C3
BL	B	CC	4N	1R	C1
BL	B	CC	4N	1R	C2
BL	B	CC	4N	1R	C3
BL	B	SC	4N	2R	C1
BL	B	SC	4N	2R	C2
BL	B	SC	4N	2R	C3
BL	B	CC	4N	2R	C1
BL	B	CC	4N	2R	C2
BL	B	CC	4N	2R	C3
BL	B	SC	4N	4R	C1
BL	B	SC	4N	4R	C2
BL	B	SC	4N	4R	C3
BL	B	CC	4N	4R	C1
BL	B	CC	4N	4R	C2
BL	B	CC	4N	4R	C3

BL	B	SC	6N	1R	C1
BL	B	SC	6N	1R	C2
BL	B	SC	6N	1R	C3
BL	B	CC	6N	1R	C1
BL	B	CC	6N	1R	C2
BL	B	CC	6N	1R	C3
BL	B	SC	6N	2R	C1
BL	B	SC	6N	2R	C2
BL	B	SC	6N	2R	C3
BL	B	CC	6N	2R	C1
BL	B	CC	6N	2R	C2
BL	B	CC	6N	2R	C3
BL	B	SC	6N	4R	C1
BL	B	SC	6N	4R	C2
BL	B	SC	6N	4R	C3
BL	B	CC	6N	4R	C1
BL	B	CC	6N	4R	C2
BL	B	CC	6N	4R	C3
BL	B	SC	6N	6R	C1
BL	B	SC	6N	6R	C2
BL	B	SC	6N	6R	C3
BL	B	CC	6N	6R	C1
BL	B	CC	6N	6R	C2
BL	B	CC	6N	6R	C3


BM	B	SC	0N	1R	C1
BM	B	SC	0N	1R	C2
BM	B	SC	0N	1R	C3
BM	B	CC	0N	1R	C1
BM	B	CC	0N	1R	C2
BM	B	CC	0N	1R	C3

BM	B	SC	1N	1R	C1
BM	B	SC	1N	1R	C2
BM	B	SC	1N	1R	C3
BM	B	CC	1N	1R	C1
BM	B	CC	1N	1R	C2
BM	B	CC	1N	1R	C3

BM	B	SC	2N	1R	C1
BM	B	SC	2N	1R	C2
BM	B	SC	2N	1R	C3
BM	B	CC	2N	1R	C1
BM	B	CC	2N	1R	C2
BM	B	CC	2N	1R	C3
BM	B	SC	2N	2R	C1
BM	B	SC	2N	2R	C2
BM	B	SC	2N	2R	C3
BM	B	CC	2N	2R	C1
BM	B	CC	2N	2R	C2
BM	B	CC	2N	2R	C3

BM	B	SC	4N	1R	C1
BM	B	SC	4N	1R	C2
BM	B	SC	4N	1R	C3
BM	B	CC	4N	1R	C1
BM	B	CC	4N	1R	C2
BM	B	CC	4N	1R	C3
BM	B	SC	4N	2R	C1
BM	B	SC	4N	2R	C2
BM	B	SC	4N	2R	C3
BM	B	CC	4N	2R	C1
BM	B	CC	4N	2R	C2
BM	B	CC	4N	2R	C3
BM	B	SC	4N	4R	C1
BM	B	SC	4N	4R	C2
BM	B	SC	4N	4R	C3
BM	B	CC	4N	4R	C1
BM	B	CC	4N	4R	C2
BM	B	CC	4N	4R	C3

BM	B	SC	6N	1R	C1
BM	B	SC	6N	1R	C2
BM	B	SC	6N	1R	C3
BM	B	CC	6N	1R	C1
BM	B	CC	6N	1R	C2
BM	B	CC	6N	1R	C3
BM	B	SC	6N	2R	C1
BM	B	SC	6N	2R	C2
BM	B	SC	6N	2R	C3
BM	B	CC	6N	2R	C1
BM	B	CC	6N	2R	C2
BM	B	CC	6N	2R	C3
BM	B	SC	6N	4R	C1
BM	B	SC	6N	4R	C2
BM	B	SC	6N	4R	C3
BM	B	CC	6N	4R	C1
BM	B	CC	6N	4R	C2
BM	B	CC	6N	4R	C3
BM	B	SC	6N	6R	C1
BM	B	SC	6N	6R	C2
BM	B	SC	6N	6R	C3
BM	B	CC	6N	6R	C1
BM	B	CC	6N	6R	C2
BM	B	CC	6N	6R	C3
