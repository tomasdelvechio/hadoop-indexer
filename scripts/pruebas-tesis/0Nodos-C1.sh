### Ejecutar este script asi
#
#
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#
#   tail -f nohup.out

## 1 Reducers
echo "=== 1 Reducer ==================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC0N1RC1" /user/hduser/data/C1.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC0N1RC1" /user/hduser/data/C1.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC0N1RC1" /user/hduser/data/C1.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC0N1RC1" /user/hduser/data/C1.txt output

