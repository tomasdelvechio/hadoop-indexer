## Modo Secuencial (Hadoop Local)

export HADOOP_CLIENT_OPTS="-XX:-UseGCOverheadLimit -Xmx4096m"

### Baseline

#echo "=== BSBSC ========================================================"
#/usr/local/hadoop/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true /home/hduser/TrecOL.txt /home/hduser/output

echo "=== BSBCC ========================================================"
/usr/local/hadoop/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true /home/hduser/TrecOL.txt /home/hduser/output


### Block-Max

#echo "=== BMBSC ========================================================"
#/usr/local/hadoop/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true /home/hduser/TrecOL.txt /home/hduser/output

#echo "=== BMBCC ========================================================"
#/usr/local/hadoop/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true /home/hduser/TrecOL.txt /home/hduser/output
