# Create output directory if not exists
#OUTPUTDIR="output"
#[ -d $OUTPUTDIR ] || mkdir $OUTPUTDIR

### Ejecutar este script asi
#
#
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#
#   tail -f nohup.out

## 1 Reducers
echo "=== 1 Reducer ==================================================="

# Baseline testing
#echo "=== BS-B-SC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BSBSC
#echo "=== BS-B-CC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BSBCC

# Block Max
#echo "=== BM-B-SC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BMBSC
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC

