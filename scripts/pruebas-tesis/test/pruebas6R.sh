# Baseline testing
echo "=== BS-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=6 data/TrecFileOl.txt BSTSC
echo "=== BS-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true data/TrecFileOl.txt BSTCC
echo "=== BS-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true data/TrecFileOl.txt BSBSC
echo "=== BS-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BSBCC

# Block Max
echo "=== BM-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=6 data/TrecFileOl.txt BMTSC
echo "=== BM-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true data/TrecFileOl.txt BMTCC
echo "=== BM-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true data/TrecFileOl.txt BMBSC
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC


