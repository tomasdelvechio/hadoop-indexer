echo "=== BS-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=4 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BSBCC

echo "=== BM-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=4 data/TrecFileOl.txt BMTSC
