### Ejecutar este script asi
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#   tail -f nohup.out

# Baseline testing
echo "=== BS-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BSBSC

# Block Max
echo "=== BM-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true data/TrecFileOl.txt BMTCC

# Controles
#   Todas las tareas que se ejecutaron completamente
#   grep "Written" output/* -Rn | wc -l
#   Deberia ser 12
