# Create output directory if not exists
#OUTPUTDIR="output"
#[ -d $OUTPUTDIR ] || mkdir $OUTPUTDIR

### Ejecutar este script asi
#
#
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#
#   tail -f nohup.out

# Baseline testing
echo "=== BS-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer data/TrecFileOl.txt BSTSC #> $OUTPUTDIR/BSTSC.output
echo "=== BS-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true data/TrecFileOl.txt BSTCC #> $OUTPUTDIR/BSTCC.output
echo "=== BS-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BSBSC #> $OUTPUTDIR/BSBSC.output
echo "=== BS-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BSBCC #> $OUTPUTDIR/BSBCC.output

# Treap testing
echo "=== TR-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.treap.TreapIndexer data/TrecFileOl.txt TRTSC #> $OUTPUTDIR/TRTSC.output
echo "=== TR-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.treap.TreapIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true data/TrecFileOl.txt TRTCC #> $OUTPUTDIR/TRTCC.output
echo "=== TR-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.treap.TreapIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt TRBSC #> $OUTPUTDIR/TRBSC.output
echo "=== TR-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.treap.TreapIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt TRBCC #> $OUTPUTDIR/TRBCC.output

# Block Max
echo "=== BM-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer data/TrecFileOl.txt BMTSC #> $OUTPUTDIR/BMTSC.output
echo "=== BM-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true data/TrecFileOl.txt BMTCC #> $OUTPUTDIR/BMTCC.output
echo "=== BM-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BMBSC #> $OUTPUTDIR/BMBSC.output
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC #> $OUTPUTDIR/BMBCC.output

#chmod 777 $OUTPUTDIR
#chmod 777 $OUTPUTDIR/*

# Controles
#   Todas las tareas que se ejecutaron completamente
#   grep "Written" output/* -Rn | wc -l
#   Deberia ser 12
