### Ejecutar este script asi
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#   tail -f nohup.out

# Baseline testing
echo "=== BS-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer data/TrecFileOl.txt BSTSC
echo "=== BS-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true data/TrecFileOl.txt BSTCC
echo "=== BS-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BSBSC
echo "=== BS-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BSBCC

# Block Max
echo "=== BM-T-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer data/TrecFileOl.txt BMTSC
echo "=== BM-T-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true data/TrecFileOl.txt BMTCC
echo "=== BM-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BMBSC
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC

#chmod 777 $OUTPUTDIR
#chmod 777 $OUTPUTDIR/*

# Controles
#   Todas las tareas que se ejecutaron completamente
#   grep "Written" output/* -Rn | wc -l
#   Deberia ser 12
