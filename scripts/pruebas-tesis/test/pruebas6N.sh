# Create output directory if not exists
#OUTPUTDIR="output"
#[ -d $OUTPUTDIR ] || mkdir $OUTPUTDIR

### Ejecutar este script asi
#
#
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#
#   tail -f nohup.out

## 6 Reducers
echo "=== 6 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true data/TrecFileOl.txt BLBLC
echo "=== BL-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BLBCC

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true data/TrecFileOl.txt BMBLC
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC

## 4 Reducers
#echo "=== 4 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=4 -D indexer.file.format.binary=true data/TrecFileOl.txt BLBLC
echo "=== BL-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=4 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BLBCC

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=4 -D indexer.file.format.binary=true data/TrecFileOl.txt BMBLC
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=4 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC

## 2 Reducers
echo "=== 2 Reducers =================================================="

# Baseline testing
#echo "=== BL-B-SC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=2 -D indexer.file.format.binary=true data/TrecFileOl.txt BLBLC
#echo "=== BL-B-CC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=2 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BLBCC

# Block Max
#echo "=== BM-B-SC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=2 -D indexer.file.format.binary=true data/TrecFileOl.txt BMBLC
echo "=== BM-B-CC ====================================================="
hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=2 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC


## 1 Reducers
#echo "=== 1 Reducer ==================================================="

# Baseline testing
#echo "=== BL-B-SC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BLBLC
#echo "=== BL-B-CC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BLBCC

# Block Max
#echo "=== BM-B-SC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true data/TrecFileOl.txt BMBLC
#echo "=== BM-B-CC ====================================================="
#hadoop jar Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true data/TrecFileOl.txt BMBCC

