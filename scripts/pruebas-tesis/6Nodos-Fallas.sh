### C1
echo "=== C1 - Coleccion 1 ============================================"

## 6 Reducers
echo "=== 6 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N6RC1" /user/hduser/data/C1.txt output

### C2
echo "=== C2 - Coleccion 2 ============================================"

## 6 Reducers
echo "=== 6 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N6RC2" /user/hduser/data/C2.txt output

## 1 Reducers
echo "=== 1 Reducer ==================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N1RC2" /user/hduser/data/C2.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC6N1RC2" /user/hduser/data/C2.txt output
