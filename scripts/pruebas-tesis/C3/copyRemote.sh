echo "COPY TO OTHER SLAVE" >> salida.out

# Copiar indice/2 y vocab/2 a slave6
echo "2 SCP @slave6 index " >> salida.out
/usr/bin/time -f '%e' -o salida.out -a scp split2indiceaa slave6:/tmp
echo "2 SCP @slave6 vocab " >> salida.out
/usr/bin/time -f '%e' -o salida.out -a scp split2vocabaa slave6:/tmp

# Copiar indice/4 y vocab/4 a slave6
echo "4 SCP @slave6 index " >> salida.out
/usr/bin/time -f '%e' -o salida.out -a scp split4indiceaa slave6:/tmp
echo "4 SCP @slave6 vocab " >> salida.out
/usr/bin/time -f '%e' -o salida.out -a scp split4vocabaa slave6:/tmp

# Copiar indice/6 y vocab/6 a slave6
echo "6 SCP @slave6 index " >> salida.out
/usr/bin/time -f '%e' -o salida.out -a scp split6indiceaa slave6:/tmp
echo "6 SCP @slave6 vocab " >> salida.out
/usr/bin/time -f '%e' -o salida.out -a scp split6vocabaa slave6:/tmp
