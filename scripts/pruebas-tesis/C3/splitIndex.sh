# Division de indice en 2 partes iguales
echo "2 Split Indice" >> salida.out
/usr/bin/time -f '%e' -o salida.out -a split --bytes=13082700274 indexFile-r-00000.txt split2indice
# Division de vocabulario en 2 partes iguales
echo "2 Split Vocabulario" >> salida.out
/usr/bin/time -f '%e' -o salida.out -a split --bytes=570190640 vocabulary-r-00000.txt split2vocab

# Division de indice en 4 partes iguales
echo "4 Split Indice" >> salida.out
/usr/bin/time -f '%e' -o salida.out -a split --bytes=6541350137 indexFile-r-00000.txt split4indice
# Division de vocabulario en 4 partes iguales
echo "4 Split Vocabulario" >> salida.out
/usr/bin/time -f '%e' -o salida.out -a split --bytes=285095320 vocabulary-r-00000.txt split4vocab

# Division de indice en 6 partes iguales
echo "6 Split Indice" >> salida.out
/usr/bin/time -f '%e' -o salida.out -a split --bytes=4360900092 indexFile-r-00000.txt split6indice
# Division de vocabulario en 6 partes iguales
echo "6 Split Vocabulario" >> salida.out
/usr/bin/time -f '%e' -o salida.out -a split --bytes=190063547 vocabulary-r-00000.txt split6vocab
