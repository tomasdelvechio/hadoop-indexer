### Ejecutar este script asi
#
#
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#
#   tail -f nohup.out

echo "=== Coleccion 2 ================================================="

## 2 Reducers
echo "=== 2 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=2 -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC2N2RC3" /user/hduser/data/C3.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=2 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC2N2RC3" /user/hduser/data/C3.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=2 -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC2N2RC3" /user/hduser/data/C3.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=2 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC2N2RC3" /user/hduser/data/C3.txt output


## 1 Reducers
echo "=== 1 Reducer ==================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC2N1RC3" /user/hduser/data/C3.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC2N1RC3" /user/hduser/data/C3.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC2N1RC3" /user/hduser/data/C3.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC2N1RC3" /user/hduser/data/C3.txt output

