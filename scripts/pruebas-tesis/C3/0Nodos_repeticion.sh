export HADOOP_CLIENT_OPTS="-XX:-UseGCOverheadLimit -Xmx4096m"

echo "=== 1 Reducer ==================================================="

echo "=== BM-B-CC-0N-1R-C3 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -D indexer.struct.block.compress=true -Dmapreduce.job.name="BMBCC0N1RC3" /home/hduser/C3.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BMBCC0N1RC3: $SORT_TIME milisegundos"
rm -rf /usr/local/hadoop/hadoop_store/tmp/*

