export HADOOP_CLIENT_OPTS="-XX:-UseGCOverheadLimit -Xmx4096m"

echo "=== 1 Reducer ==================================================="

# Baseline

## Sin compresion
echo "=== BL-B-SC-0N-1R-C1 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC0N1RC1" /home/hduser/C1.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BLBSC0N1RC1: $SORT_TIME milisegundos"

echo "=== BL-B-SC-0N-1R-C2 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC0N1RC2" /home/hduser/C2.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BLBSC0N1RC2: $SORT_TIME milisegundos"

## Con Compresion
echo "=== BL-B-CC-0N-1R-C1 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -D indexer.struct.block.compress=true -Dmapreduce.job.name="BLBCC0N1RC1" /home/hduser/C1.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BLBCC0N1RC1: $SORT_TIME milisegundos"

echo "=== BL-B-CC-0N-1R-C2 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -D indexer.struct.block.compress=true -Dmapreduce.job.name="BLBCC0N1RC2" /home/hduser/C2.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BLBCC0N1RC2: $SORT_TIME milisegundos"

# Block-Max

# Sin Compresion
echo "=== BM-B-SC-0N-1R-C1 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC0N1RC1" /home/hduser/C1.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BMBSC0N1RC1: $SORT_TIME milisegundos"

echo "=== BM-B-SC-0N-1R-C2 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC0N1RC2" /home/hduser/C2.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BMBSC0N1RC2: $SORT_TIME milisegundos"

# Con Compresion
echo "=== BM-B-CC-0N-1R-C1 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -D indexer.struct.block.compress=true -Dmapreduce.job.name="BMBCC0N1RC1" /home/hduser/C1.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BMBCC0N1RC1: $SORT_TIME milisegundos"

echo "=== BM-B-CC-0N-1R-C2 ============================================"
BEFORE_SORT=`date +%s%3N`
/home/hduser/hadoop-2.7.3/bin/hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -D indexer.struct.block.compress=true -Dmapreduce.job.name="BMBCC0N1RC2" /home/hduser/C2.txt output
AFTER_SORT=`date +%s%3N`
SORT_TIME=`expr $AFTER_SORT - $BEFORE_SORT`
echo "Tiempo Total para BMBCC0N1RC2: $SORT_TIME milisegundos"
