### Ejecutar este script asi
#
#
#
#   nohup time ./pruebas.sh &
#
# Controlar el log asi
#
#
#   tail -f nohup.out

## 6 Reducers
echo "=== 6 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N6RC1" /user/hduser/data/C1.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC6N6RC1" /user/hduser/data/C1.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=6 -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC6N6RC1" /user/hduser/data/C1.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=6 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC6N6RC1" /user/hduser/data/C1.txt output

## 4 Reducers
echo "=== 4 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=4 -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N4RC1" /user/hduser/data/C1.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=4 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC6N4RC1" /user/hduser/data/C1.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=4 -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC6N4RC1" /user/hduser/data/C1.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=4 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC6N4RC1" /user/hduser/data/C1.txt output

## 2 Reducers
echo "=== 2 Reducers =================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -Dmapreduce.job.reduces=2 -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N2RC1" /user/hduser/data/C1.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=2 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC6N2RC1" /user/hduser/data/C1.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -Dmapreduce.job.reduces=2 -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC6N2RC1" /user/hduser/data/C1.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -Dmapreduce.job.reduces=2 -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC6N2RC1" /user/hduser/data/C1.txt output


## 1 Reducers
echo "=== 1 Reducer ==================================================="

# Baseline testing
echo "=== BL-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBSC6N1RC1" /user/hduser/data/C1.txt output
echo "=== BL-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.baseline.BaselineIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BLBCC6N1RC1" /user/hduser/data/C1.txt output

# Block Max
echo "=== BM-B-SC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBSC6N1RC1" /user/hduser/data/C1.txt output
echo "=== BM-B-CC ====================================================="
hadoop jar /home/hduser/Tesis.jar com.tomasdel.tesis.indexbuilder.blockmax.BlockMaxIndexer -files /home/hduser/lib/JavaFastPFOR-0.1.7.jar -D indexer.struct.block.compress=true -D indexer.file.format.binary=true -Dmapreduce.job.name="BMBCC6N1RC1" /user/hduser/data/C1.txt output

