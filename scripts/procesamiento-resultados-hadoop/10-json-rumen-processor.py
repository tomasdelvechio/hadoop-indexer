#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    json-rumen-processor.py [-h,--help] [-v,--verbose] [--version]

DESCRIPTION

    Script para procesar la salida de RUMEN de los job history y obtener
    los datos necesarios para la tesis.

EXAMPLES

    json-rumen-processor.py -i rumen-output.json [ -o resultados.csv ]

AUTHOR

    Tomas Delvechio <tomasdelvechio17@gmail.com>

LICENSE

    GPLv3

VERSION

    0.1.0
"""

import sys
import os
import traceback
import argparse
import time
import re
import json
import logging


# Creacion de Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler('trace.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

validJobs = None

# Uso del log
#logger.info('Hello baby')

# GRANULARIDAD marca el nivel de granularidad del archivo de crawler.
#
#   Valores: 'job', 'phase', 'task', 'attempt'
#
#   Una granularidad nivel 'job' descarga desde ganglia las metricas en los
#   Hosts intevinientes en el job, sin distincion de fases (map, reduce), tasks
#   o tasks attempts.
#
#   'phase' descarga las metricas diferenciando por cada fase del job (Map y
#   Reduce).
#
#   'task' descargara las metricas en base a los timestamps de cada task,
#   tomando en cuenta los attempts intervinientes del mismo en los hosts que
#   correspondan.
#
#   'attempt' es el modo de descarga mas fino, generando una linea de descarga
#   por cada attempt, de cada task, en todas las fases.
#



def parseJsonStream(stream):
    """
    Construye un generador que toma un stream de objetos json para ser
    iterados. Adecuado para procesar la salida json de RUMEN y obtener
    en cada iteracion un Job diferente.
    """
    decoder = json.JSONDecoder()
    while stream:
        obj, idx = decoder.raw_decode(stream)
        yield obj
        stream = stream[idx:].lstrip()


def getAlgoritmo(job):
    """
    Recupera el algoritmo del Job en base al formato del nombre
    """
    return job['jobName'][0:2]


def getFormato(job):
    """
    Recupera el formato del archivo del Job en base al formato del nombre
    """
    return job['jobName'][2:3]


def getCompresion(job):
    """
    Recupera el uso de compresion a partir del nombre del job
    """
    return job['jobName'][3:5]


def getCantidadNodos(job):
    """
    Recupera la cantidad de nodos a partir del nombre del job
    """
    return job['jobName'][5:7]


def getCantidadReducers(job):
    """
    Recupera la cantidad de reducers a partir del nombre del job
    """
    return job['jobName'][7:9]


def getColeccion(job):
    """
    Recupera la coleccion a partir del nombre del job
    """
    return job['jobName'][9:11]


def getHeader(job, field):
    """
    Metodo generico para extraer un campo del header
    """
    if field == 'algoritmo':
        return getAlgoritmo(job)
    elif field == 'formato':
        return getFormato(job)
    elif field == 'compresion':
        return getCompresion(job)
    elif field == 'cantidadNodos':
        return getCantidadNodos(job)
    elif field == 'cantidadReducers':
        return getCantidadReducers(job)
    elif field == 'coleccion':
        return getColeccion(job)


def getJobHeader(job, stats):
    """
    Recupera los datos principales del Job
    """
    stats['Algoritmo'] = getHeader(job, 'algoritmo')
    stats['Formato'] = getHeader(job, 'formato')
    stats['Compresion'] = getHeader(job, 'compresion')
    stats['CantidadNodos'] = getHeader(job, 'cantidadNodos')
    stats['CantidadReducers'] = getHeader(job, 'cantidadReducers')
    stats['Coleccion'] = getHeader(job, 'coleccion')
    stats['jobID'] = job['jobID']
    return stats


def unixDateToHumanDate(unixDate, formato='%m/%d/%Y %H:%M'):
    return time.strftime(formato, time.localtime(unixDate))


def convertUnixDateString(unixDate):
    """
    Convierte fecha unix del tipo "1480279055830" a (float) 1480279055.830
    """
    #return float(unixDateString[:-3] + '.' + unixDateString[-3:]) # para string
    return unixDate / 1000.0


def getStartTimeJob(job):
    return convertUnixDateString(job['launchTime'])


def getEndTimeJob(job):
    return convertUnixDateString(job['finishTime'])


def getDurationJob(jobStats):
    """
    Retorna la duracion del job
    """
    return jobStats['Fin'] - jobStats['Inicio']


def getGlobalDuration(job, jobStats):
    """
    Recupera la duracion del job
    """
    jobStats['Inicio'] = getStartTimeJob(job)
    jobStats['Fin'] = getEndTimeJob(job)
    jobStats['Duracion'] = getDurationJob(jobStats)
    return jobStats


def getCompressFlag(job):
    """
    Determina si el parametro de comprimir fue seteado al momento de ejecucion
    """
    if "indexer.struct.block.compress" in job['jobProperties']:
        return "CC"
    else:
        return "SC"


def getJobTopStats(job, jobStats):
    jobStats['Estado'] = job['outcome']
    jobStats['NumeroMapers'] = job['totalMaps']
    jobStats['NumeroReducers'] = job['totalReduces']
    jobStats['Compress'] = getCompressFlag(job)
    return jobStats


def getTaskAttemp(taskAttemp):
    taskAttempData = {}
    taskAttempData['Estado'] = taskAttemp['result']
    taskAttempData['Inicio'] = convertUnixDateString(taskAttemp['startTime'])
    taskAttempData['Fin'] = convertUnixDateString(taskAttemp['finishTime'])
    # La estructura completa es 'rack/host' asi que en caso de tener varios
    #   racks, refactorizar y putearme
    taskAttempData['Host'] = taskAttemp['location']['layers'][1]
    taskAttempData['Duracion'] = taskAttempData['Fin'] - taskAttempData['Inicio']
    return taskAttempData


def getTaskAttemps(task):
    taskAttemps = {}
    for taskAttemp in task['attempts']:
        taskAttemps[taskAttemp['attemptID']] = getTaskAttemp(taskAttemp)
    return taskAttemps


def getTask(task):
    taskStat = {}
    taskStat['Inicio'] = convertUnixDateString(task['startTime'])
    taskStat['Fin'] = convertUnixDateString(task['finishTime'])
    taskStat['Duracion'] = taskStat['Fin'] - taskStat['Inicio']
    taskStat['Estado'] = task['taskStatus']
    taskStat['attempts'] = getTaskAttemps(task)

    return taskStat


def getTasksFromList(taskList):
    tasks = {}
    for task in taskList:
        tasks[task['taskID']] = getTask(task)
    return tasks


def getTasks(job, jobStats):
    jobStats['map'] = getTasksFromList(job['mapTasks'])
    jobStats['reduce'] = getTasksFromList(job['reduceTasks'])
    return jobStats


def getErrorsFromList(lista, statusIndex, successStatusVale='SUCCESS'):
    """
    Retorna los errors de una lista de objetos (jobs, tasks, attempts)
    """
    errors = 0
    for element in lista:
        if not (element[statusIndex] == successStatusVale):
            errors += 1
    return errors


def getTaskAttemptsErrors(attempts):
    """
    Retorna la cantidad de errores de una lista de Attempts
    """
    global args
    if args.verbose:
        print "start getErrors for attempts"
    return getErrorsFromList(attempts, 'result')


def getTaskErrors(tasks):
    """
    Retorna la cantidad de errores de una lista de task
    """
    global args
    if args.verbose:
        print "start getErrors for tasks"
    return getErrorsFromList(tasks, 'taskStatus')


def getJobErrors(job):
    global args
    if args.verbose:
        print "start getErrors for Job"
    if not job['outcome'] == 'SUCCESS':
        return 1
    return 0


def getErrors(job, jobStats):
    """
    Procesa errores del job y los carga en el dic de estadisticas

    job -> outcome
    task -> taskStatus
    taskAttemp -> result

    Values to search KILLED, FAILED
    """
    jobStats['attemptsErrors'] = 0
    for task in job['mapTasks']:
        jobStats['attemptsErrors'] += getTaskAttemptsErrors(task['attempts'])
    for task in job['reduceTasks']:
        jobStats['attemptsErrors'] += getTaskAttemptsErrors(task['attempts'])
    jobStats['taskErrors'] = getTaskErrors(job['mapTasks'])
    jobStats['taskErrors'] += getTaskErrors(job['reduceTasks'])
    jobStats['jobErrors'] = getJobErrors(job)

    return jobStats


def procesarJob(job):
    """
    Implementa el workflow principal que procesa el job.
    """
    jobStats = getJobHeader(job, {})
    jobStats = getGlobalDuration(job, jobStats)
    jobStats = getJobTopStats(job, jobStats)
    jobStats = getTasks(job, jobStats)
    jobStats = getErrors(job, jobStats)

    return jobStats


def iguales(job, otherJob):
    """
    Verifica y retorna verdadero si es el mismo job
    """
    return job['jobID'] == otherJob['jobID']


def mismoExperimento(job, otherJob):
    """
    Verifica si dos jobs corresponden a la ejecucion del mismo experimento
    """
    return  job['Algoritmo'] == otherJob['Algoritmo'] and \
            job['Formato'] == otherJob['Formato'] and \
            job['Compresion'] == otherJob['Compresion'] and \
            job['CantidadNodos'] == otherJob['CantidadNodos'] and \
            job['CantidadReducers'] == otherJob['CantidadReducers'] and \
            job['Coleccion'] == otherJob['Coleccion']


def getExperimento(job):
    return "-".join([   job['Algoritmo'],
                        job['Formato'],
                        job['Compresion'],
                        job['CantidadNodos'],
                        job['CantidadReducers'],
                        job['Coleccion'] ])


def getCantidadErrores(job):
    return  job['attemptsErrors'] + job['taskErrors'] + job['jobErrors']


def compararJobs(job, otherJob):
    """
    Para 2 jobs que no son el mismo y representan el mismo experimento,
    compara en base a diferentes criterios y toma el que mejor se adapta.

    Retorna el jobId del job que sea mejor
    """
    global logger

    # El que tiene menos errores
    # Si tienen misma cantidad de errores, el que dure menos
    # Si coincide en todas, el job seleccionado es el primero
    if getCantidadErrores(job) < getCantidadErrores(otherJob):
        return job['jobID']
    elif getCantidadErrores(job) > getCantidadErrores(otherJob):
        lineaLogger = '{experimento} Old Job: {idOldJob} - Errores: {erroresOldJob} :: New Job: {idNewJob} - Errores: {erroresNewJob}'
        logger.info(lineaLogger.format( experimento=getExperimento(job),
                                        idOldJob=job['jobID'],
                                        erroresOldJob=getCantidadErrores(job),
                                        idNewJob=otherJob['jobID'],
                                        erroresNewJob=getCantidadErrores(otherJob)))
        return otherJob['jobID']

    if getDurationJob(job) < getDurationJob(otherJob):
        return job['jobID']
    elif getDurationJob(job) > getDurationJob(otherJob):
        lineaLogger = 'Old Job: {idOldJob} - Duracion: {duracionOldJob} :: New Job: {idNewJob} - Duracion: {duracionNewJob}'
        logger.info(lineaLogger.format( idOldJob=job['jobID'],
                                        duracionOldJob=getDurationJob(job),
                                        idNewJob=otherJob['jobID'],
                                        duracionNewJob=getDurationJob(otherJob)))
        return otherJob['jobID']

    return otherJob['jobID']


def getOutputFileHandler():
    """
    Retorna un filehandler valido para generar la salida.

    Garantiza que una ejecución no borre un archivo existente generado
    previamente.
    """
    global args

    sufijo = 0
    candidateFilename = args.output
    while os.path.exists(candidateFilename):
        candidateFilename = args.output + "-" + str(sufijo)
        sufijo += 1

    return open(candidateFilename, 'w')


def generarLinea(experimento, idf, hosts, inicio, fin):
    return "{experimento}-{idf};{hosts};{inicio};{fin}\n".format(
                experimento=experimento,
                idf=idf,
                hosts=hosts,
                inicio=inicio,
                fin=fin)


def loadValidJobs():
    """
    Carga los jobs validos
    """
    global args
    if args.validJobs:
        with open(args.validJobs) as f:
            jobs = []
            for line in f:
                line = line.strip()
                jobs.append(line)
    else:
        jobs = None

    return jobs

def isValidJob(idJob):
    """
    En caso de tener seteado un archivo de jobs aceptados, verifica que el
    parametro se encuentre dentro de los mismos
    """
    global validJobs

    if validJobs is None or idJob in validJobs:
        print "{idJob} es un Job valido".format(idJob=idJob)
        return True
    print "{idJob} NO es un Job valido".format(idJob=idJob)
    return False


def generarCsvPorAttempts(jobs, jobsProcesables):
    """
    Recorre todos los attempts de un job, genera un nombre para los mismos,
    recupera los hosts pertinentes, formatea los timestamps y escribe al archivo
    """
    with getOutputFileHandler() as outputFileHandler:
        for nombreExperimento, idJob in jobsProcesables.iteritems():
            job = jobs[idJob]
            if isValidJob(idJob):
                jobsValidos = open('jobsProcesados.txt', 'a')
                jobsValidos.write(nombreExperimento + ' :: ' + idJob + '\n')
                for task in job['map'].values() + job['reduce'].values():
                    for idA, attempt in task['attempts'].iteritems():
                        if attempt['Estado'] == 'SUCCESS':
                            idAttempt = '-'.join(idA.split('_')[1:])
                            nombre = getExperimento(job)
                            host = attempt['Host']
                            inicio = unixDateToHumanDate(attempt['Inicio'])
                            fin = unixDateToHumanDate(attempt['Fin'])
                            linea = generarLinea(nombre, idAttempt,
                                                 host, inicio, fin)
                            outputFileHandler.write(linea)


def generarCsvParaCrawler(jobs, jobsProcesables):
    """
    Dado el listado de jobs y de jobs procesables por experimento, y genera
    una salida por csv que se utilizara como input de crawler.py

    El formato del archivo csv es:
    <Nombre Experimento>;<Lista de hosts>;<Hora Inicio>;<Hora Fin>\n

    Formato de la lista de hosts:
    <host 1>,<host 2>,...,<host N>

    Formato de los campos Hora:
    <Mes>/<Dia>/<Anio> <Hora>:<Minuto>
    """
    global args

    if args.granularidad == 'attempts':
        generarCsvPorAttempts(jobs, jobsProcesables)


def getJobsFromJson(jsonFile):
    """
    De un archivo json en formato rumen, retorna los jobs en un dic
    """
    jobs = {}
    jobs_stream = open(jsonFile).read()
    for job in parseJsonStream(jobs_stream):
        jobs[job['jobID']] = procesarJob(job)
    return jobs


def getJobsProcesables(jobs):
    """
    Toma todos los jobs y decide cuales son procesables. Para que un job sea
    procesable debe cumplirse 2 condiciones:

     * Ningun job que sea del mismo experimento debe ser identificado como
     procesable.
     * Si existe un job que es el mismo experimento y no es el mismo:
        * Debe contener menor cantidad de errores
        * Si tiene la misma cantidad de errores debe durar menos tiempo
    """
    global logger

    jobsProcesables = {}
    for idJob,job in jobs.iteritems():
        nombreExperimento = getExperimento(job)
        logger.info("Procesando experimento: " + nombreExperimento)
        if not nombreExperimento in jobsProcesables.keys():
            jobsProcesables[nombreExperimento] = job['jobID']
            continue

        otherJob = jobs[jobsProcesables[nombreExperimento]]
        if idJob == "job_1493174712304_0001":
            print "Procesando: job_1493174712304_0001"
        if not iguales(job, otherJob) and mismoExperimento(job, otherJob):
            jobsProcesables[nombreExperimento] = compararJobs(otherJob, job)
    return jobsProcesables


def writeJobProcesablesToLog(jobs, jobsProcesables):
    """
    Escribe en el logger todos los jobs que fueron identificados como procesables
    """
    global logger

    for nombreExperimento, candidateJob in jobsProcesables.iteritems():
        job = jobs[candidateJob]
        inicio = unixDateToHumanDate(job['Inicio'])
        fin = unixDateToHumanDate(job['Fin'])
        # logLine Format: {nombre experimento} - {jobID} - {inicio job} - {fin job}
        logLine = '{nombre} - {id} - {start} - {end}'
        logger.info(logLine.format( nombre=nombreExperimento,
                                    id=candidateJob,
                                    start=inicio,
                                    end=fin))


def generarSalidaConTiempos(jobs):
    """
    Genera una salida de tiempos por job
    """
    import json
    with getOutputFileHandler() as outputFileHandler:
        line = '{nombre};{id};{start};{end};{duracion};{cantidadMappers};{cantidadReducers};{estado};{compress};{erroresJob};{erroresTask};{erroresAttempts}\n'
        outputFileHandler.write(line)
        for job in jobs.values():
            #print json.dumps(job, indent=3, sort_keys=True)
            #exit()
            inicio = unixDateToHumanDate(job['Inicio'], '%d/%m/%Y %H:%M')
            fin = unixDateToHumanDate(job['Fin'], '%d/%m/%Y %H:%M')
            outputFileHandler.write(line.format(nombre=getExperimento(job),
                                                id=job['jobID'],
                                                start=inicio,
                                                end=fin,
                                                duracion=job['Duracion'],
                                                cantidadMappers=job['NumeroMapers'],
                                                cantidadReducers=job['NumeroReducers'],
                                                estado=job['Estado'],
                                                compress=job['Compress'],
                                                erroresJob=job['jobErrors'],
                                                erroresTask=job['taskErrors'],
                                                erroresAttempts=job['attemptsErrors']))


def main():

    global args, logger, validJobs

    jobs = getJobsFromJson(args.input)
    jobsProcesables = getJobsProcesables(jobs)

    validJobs = loadValidJobs()

    if args.log:
        writeJobProcesablesToLog(jobs, jobsProcesables)

    if(args.salida == 'crawler'):
        # Tengo en jobsProcesables todos los que quiero procesar, ahora puedo
        #   generar los enlaces para el crawler.py
        generarCsvParaCrawler(jobs, jobsProcesables)
    elif(args.salida == 'tiempos'):
        # Genera un listado de Jobs validos con tiempos de ejecucion:
        generarSalidaConTiempos(jobs)


if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(usage=globals()['__doc__'],
                                         version='1.0.0')
        parser.add_argument('--verbose',
                            action='store_true',
                            default=False,
                            help='verbose output')

        parser.add_argument('-i',
                            '--input',
                            action='store',
                            help='JSON File, RUMEN format',
                            required=True)

        parser.add_argument('-o',
                            '--output',
                            action='store',
                            default='resultados.csv',
                            help='Path to CSV File, comma separated')

        parser.add_argument('-g',
                            '--granularidad',
                            action='store',
                            default='attempts',
                            help='Level of granularity for processing')

        parser.add_argument('-s',
                            '--salida',
                            action='store',
                            default='crawler',
                            help='Formato de salida. Opciones disponibles (crawler, tiempos)')

        parser.add_argument('-l',
                            '--log',
                            action='store',
                            default=False,
                            help='Si el parametro es seteado, crea un log de la ejecucion.')

        parser.add_argument('-a',
                            '--validJobs',
                            action='store',
                            help='Hadoop Job Ids validos (Ignora los que no vengan en este archivo). Un job id por linea')

        args = parser.parse_args()
        if args.verbose:
            print time.asctime()
        main()
        if args.verbose:
            print time.asctime()
        if args.verbose:
            print 'TOTAL TIME IN MINUTES:',
        if args.verbose:
            print (time.time() - start_time) / 60.0
        sys.exit(0)
    except KeyboardInterrupt, e:  # Ctrl-C
        raise e
    except SystemExit, e:  # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)

#python ~/Dropbox/UNLu/Tesis/sources/scripts/procesamiento-resultados-hadoop/json-rumen-processor.py -i estadisticas-jobs-tesis/1Nodos/rumen/out.json -o salida.csv -s tiempos
