DATADIR="/home/tomas/Dropbox/UNLu/Tesis/Datos"
pathsc3=($DATADIR/C3/2N/2Nbis/out.json $DATADIR/C3/2N/2NF2/out.json $DATADIR/C3/2N/2NF2_2/out.json $DATADIR/C3/2N/2NFalt/out.json $DATADIR/C3/4Nbis/out.json $DATADIR/C3/6N/out.json $DATADIR/C3/6N/failedReRun/out.json )
paths=($DATADIR/estadisticas-jobs-tesis/1Nodos/rumen/out.json $DATADIR/estadisticas-jobs-tesis/2Nodos/rumen/out.json $DATADIR/estadisticas-jobs-tesis/4Nodos/rumen/out.json $DATADIR/estadisticas-jobs-tesis/6Nodos/rumen/out.json)

for path in ${pathsc3[@]}
do
    echo "Procesando "$path
    python json-rumen-processor.py -i $path -o $DATADIR/ganglia/salidaParaCrawlerC3.csv -a $DATADIR/jobs.data
done

for path in ${paths[@]}
do
    echo "Procesando "$path
    python json-rumen-processor.py -i $path -o $DATADIR/ganglia/salidaParaCrawlerC1C2.csv -a $DATADIR/jobs.data
done
