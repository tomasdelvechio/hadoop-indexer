#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    failed-task-counter.py [-h,--help] [-v,--verbose] [--version]

DESCRIPTION

    Script para procesar errores en un json RUMEN

EXAMPLES

    python failed-task-counter.py -i out.json | grep -v SUCCESS

AUTHOR

    Tomas Delvechio <tomasdelvechio17@gmail.com>

LICENSE

    GPLv3

VERSION

    0.1.0
"""

import sys
import os
import traceback
import argparse
import time
import re
import json


def parseJsonStream(stream):
    """
    Construye un generador que toma un stream de objetos json para ser
    iterados. Adecuado para procesar la salida json de RUMEN y obtener
    en cada iteracion un Job diferente.
    """
    decoder = json.JSONDecoder()
    while stream:
        obj, idx = decoder.raw_decode(stream)
        yield obj
        stream = stream[idx:].lstrip()


def getJobsFromJson(jsonFile):
    """
    De un archivo json en formato rumen, retorna los jobs en un dic
    """
    jobs = {}
    jobs_stream = open(jsonFile).read()
    for job in parseJsonStream(jobs_stream):
        jobs[job['jobID']] = job
    return jobs


def getErrors(job):
    """
    Procesa errores del job y los muestra por pantalla

    job -> outcome
    task -> taskStatus
    taskAttemp -> result

    Values to search KILLED, FAILED
    """
    line = "{jobId} - {failedMapperFraction};{taskId};{attemptId};{node};{result}"
    
    jobId = job['jobID']
    failedMapperFraction = job['failedMapperFraction']
    if not args.output:
        print job['jobID'] + ";" + job['failedMapperFraction']
    for task in job['mapTasks']:
        taskId = task['taskID']
        if not args.output:
            print "   " + task['taskID'] + ";" + task['taskStatus']
        for attempt in task['attempts']:
            attemptId = attempt['attemptID']
            node = attempt['hostName']
            result = attempt['result']
            if not args.output:
                print "      " + attemptId + ";" + node + ";" + result
            else:
                print line.format(  jobId=jobId,
                                    failedMapperFraction=failedMapperFraction,
                                    taskId=taskId,
                                    attemptId=attemptId,
                                    node=node,
                                    result=result)


def main():

    global args

    jobs = getJobsFromJson(args.input)
    if (len(jobs) > 0):
        for job in jobs:
            getErrors(jobs[job])


if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(usage=globals()['__doc__'],
                                         version='0.1.0')
        parser.add_argument('--verbose',
                            action='store_true',
                            default=False,
                            help='verbose output')

        parser.add_argument('-i',
                            '--input',
                            action='store',
                            help='JSON File, RUMEN format',
                            required=True)


        parser.add_argument('-o',
                            '--output',
                            default='csv',
                            action='store',
                            help='Output format, standar output',
                            required=False)


        args = parser.parse_args()
        if args.verbose:
            print time.asctime()
        main()
        if args.verbose:
            print time.asctime()
        if args.verbose:
            print 'TOTAL TIME IN MINUTES:',
        if args.verbose:
            print (time.time() - start_time) / 60.0
        sys.exit(0)
    except KeyboardInterrupt, e:  # Ctrl-C
        raise e
    except SystemExit, e:  # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
