#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    hadoop-local-log-processor.py [-h,--help] [-v,--verbose] [--version]

DESCRIPTION

    Script para procesar la salida de Hadoop local

EXAMPLES

    python hadoop-local-log-processor.py -i local-output.out [ -o resultados.csv ]

    python ~/Dropbox/UNLu/Tesis/sources/scripts/procesamiento-resultados-hadoop/hadoop-local-log-processor.py -i ~/Desktop/DatosTesis/estadisticas-jobs-tesis/0Nodos/nohup.out.local -o salida0N.csv -l log


AUTHOR

    Tomas Delvechio <tomasdelvechio17@gmail.com>

LICENSE

    GPLv3

VERSION

    0.1.0
"""

import sys
import os
import traceback
import argparse
import time
import re
import json
import logging
import datetime
from collections import OrderedDict


# Creacion de Logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.FileHandler('trace.log')
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

#{ 'regex': re.compile(), 'type': '' }
#campos = {  'nombre': { 'regex': re.compile('=* ((?:\w{0,2}-)+\w{0,2}) =*'), 'type': 'string' },
campos = OrderedDict()
campos['nombre'] = { 'regex': re.compile('((?:\w{0,2}-\w{0,2}){5})'), 'type': 'string' }
campos['id'] = { 'regex': re.compile('Running job: (job_local\d+_\d+)'), 'type': 'string' }
campos['start'] = { 'regex': re.compile('(\d{2}/\d{2}/\d{2} \d{2}:\d{2})+:\d{2} WARN'), 'type': 'date'}
campos['end'] = { 'regex': re.compile('(\d{2}/\d{2}/\d{2} \d{2}:\d{2})+:\d{2} INFO mapreduce.Job: Counters'), 'type': 'date' }
campos['duracion'] = { 'regex': re.compile(': (\d+) mili'), 'type': 'duracion' }
campos['cantidadMappers'] = { 'regex': re.compile('Starting task: (attempt_local\d*_\d*_m_\d*_\d*)'), 'type': 'cantidad' }
campos['cantidadReducers'] = { 'regex': re.compile('Starting task: (attempt_local\d*_\d*_r_\d*_\d*)'), 'type': 'cantidad' }
campos['estado'] = { 'regex': re.compile('(completed successfully)'), 'type': 'estado' }
campos['compress'] = { 'regex': re.compile('(?:\w{0,2}-){2}(SC|CC)(?:-\w{0,2}){3}'), 'type': 'string' }
campos['erroresJob'] = { 'regex': "", 'type': 'constant', 'value': 0}
campos['erroresTask'] = { 'regex': "", 'type': 'constant', 'value': 0}
campos['erroresAttempts'] = { 'regex': "", 'type': 'constant', 'value': 0}


def unixDateToHumanDate(unixDate, formato='%m/%d/%Y %H:%M'):
    return time.strftime(formato, time.localtime(unixDate))


def getValue(line, regex):
    value = regex.search(line)
    if value is not None:
        return value.group(1)
    else:
        return None


def isComplete(job):
    global campos
    completado = True
    for campo in campos.keys():
        completado = completado and (campo in job)
    return completado


def procesarValor(valor, tipo):
    if tipo == 'date':
        # Fecha tiene formato %Y/%m/%d %H:%M, debe ir %d/%m/%Y %H:%M
        return time.strftime('%d/%m/%Y %H:%M', time.strptime(valor, '%y/%m/%d %H:%M'))
        #return time.strftime(time.strptime(valor, '%y/%m/%d %H:%M'), '%d/%m/%Y %H:%M')
    elif tipo == 'duracion':
        # Duracion esta en milisegundos, hay que devolver en segundos
        return int(valor) / 1000.0
    return valor


def getJobsFromFile(inputPathFile):
    global campos, args
    jobs = []
    job = {}
    with open(inputPathFile, 'r') as inputFile:
        for line in inputFile:

            for campo in campos.keys():
                if campo == 'estado':
                    # Nunca aparecio un job con error asi que no esta clara la regex
                    value = 'SUCCESS'
                elif campos[campo]['type'] == 'constant':
                    value = campos[campo]['value']
                else:
                    value = getValue(line, campos[campo]['regex'])
                if value is not None:
                    value = procesarValor(value, campos[campo]['type'])
                    if campos[campo]['type'] == 'cantidad':
                        if campo in job:
                            job[campo] += 1
                        else:
                            job[campo] = 1
                    else:
                        job[campo] = value

            if isComplete(job):
                if args.log:
                    print job
                jobs.append(job)
                job = {}

    return jobs


def getOutputFileHandler():
    """
    Retorna un filehandler valido para generar la salida.

    Garantiza que una ejecución no borre un archivo existente generado
    previamente.
    """
    global args

    sufijo = 0
    candidateFilename = args.output
    while os.path.exists(candidateFilename):
        candidateFilename = args.output + "-" + str(sufijo)
        sufijo += 1

    return open(candidateFilename, 'w')


def main():

    global args, logger

    jobs = getJobsFromFile(args.input)

    with getOutputFileHandler() as outputFileHandler:
        #line = '{nombre};{id};{start};{end};{duracion};{cantidadMappers};{cantidadReducers};{estado};{compress};{erroresJob};{erroresTask};{erroresAttempts}\n'
        line = ';'.join("{{{0}}}".format(campo) for campo in campos.keys()) + '\n'
        print line
        outputFileHandler.write(line)
        if (len(jobs) > 0):
            for job in jobs:
                #inicio = unixDateToHumanDate(job['start'], '%d/%m/%Y %H:%M')
                #fin = unixDateToHumanDate(job['end'], '%d/%m/%Y %H:%M')
                outputFileHandler.write(line.format(**job))
                                                    #id=job['id'],
                                                    #start=inicio,
                                                    #end=fin,
                                                    #duracion=job['duracion'],
                                                    #cantidadMappers=job['cantidadMappers'],
                                                    #cantidadReducers=job['cantidadReducers'],
                                                    #estado=job['estado'],
                                                    #compress=job['compress'],
                                                    #erroresJob=job['erroresJob'],
                                                    #erroresTask=job['erroresTask'],
                                                    #erroresAttempts=job['erroresAttempts']))



if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(usage=globals()['__doc__'],
                                         version='1.0.0')
        parser.add_argument('--verbose',
                            action='store_true',
                            default=False,
                            help='verbose output')

        parser.add_argument('-i',
                            '--input',
                            action='store',
                            help='JSON File, RUMEN format',
                            required=True)

        parser.add_argument('-o',
                            '--output',
                            action='store',
                            default='resultados.csv',
                            help='Path to CSV File, comma separated')

        parser.add_argument('-s',
                            '--salida',
                            action='store',
                            default='crawler',
                            help='Formato de salida. Opciones disponibles (crawler, tiempos)')

        parser.add_argument('-l',
                            '--log',
                            action='store',
                            default=False,
                            help='Si el parametro es seteado, crea un log de la ejecucion.')

        args = parser.parse_args()
        if args.verbose:
            print time.asctime()
        main()
        if args.verbose:
            print time.asctime()
        if args.verbose:
            print 'TOTAL TIME IN MINUTES:',
        if args.verbose:
            print (time.time() - start_time) / 60.0
        sys.exit(0)
    except KeyboardInterrupt, e:  # Ctrl-C
        raise e
    except SystemExit, e:  # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
