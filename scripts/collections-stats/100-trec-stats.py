#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    trec-stats.py [-h,--help] [-v,--verbose] [--version] -f,--trec-file file.trec

DESCRIPTION

    Genera estadisticas de una colección Trec

AUTHOR

    Tomas Delvechio <tdelvechio@unlu.edu.ar>

LICENSE

    GPLv3

VERSION

    0.0.1
"""

import sys
import os
import traceback
import argparse
import time
import re

regexTrecDocument = r"<DOC>\s*<DOCNO>\s*(?P<docid>\w*)</DOCNO>(?P<content>.*?)</DOC>(*?)$"
buff = 10240  # buffer de 10kB


def getRegex():

    global regexTrecDocument

    return re.compile(regexTrecDocument, re.DOTALL)


def splitTrecDocuments(raw_trec_documents):

    regex = getRegex()
    return [m.groupdict() for m in regex.finditer(raw_documents)]


def getTrail(raw_documents):
    last_end_doc_idx = raw_documents.rfind('</DOC>') + len('</DOC>')
    return raw_documents[:last_end_doc_idx], raw_documents[last_end_doc_idx:]


def getDocuments(trecFile, trail):

    global buff

    # Recupera lo que falta para completar los 10kB de datos del archivo
    newBuffer = buff - len(trail)
    raw_documents = trail + trecFile.read(newBuffer)

    # Si existe contenido despues del ultimo </DOC> se guarda para la siguiente
    raw_documents, trail = getTrail(raw_documents)
    return raw_documents, trail


def buildToken(token):


def stopword(token):



def isValidToken(token):
    if not stopword(token) and validLenght(token) and token is not None:
        return True
    return False


def cleanText(content):
    cleanedText = []

    lowCaseContent = content.lower()
    regex = re.compile('\W')
    alphanumericContent = regex.sub(' ', lowCaseContent)
    candidateTokes = alphanumericContent.split()

    for token in candidateTokes:
        candidato = buildToken(token)
        if isValidToken(candidato):
            cleanedText.append(candidato)

    return " ".join(cleanedText)


def cleanDocuments(documents):
    """Toma una lista de dics de documents y la tokeniza y "limpia", doc por doc

    La lista contiene N dics con el siguiente formato:

        {
            "docid": 5,
            "content": "document content"
        }

    """

    for indice, document in enumerate(documents):
        documents[indice]['content'] = cleanText(document['content'])


def main ():

    global args

    trail = ""
    with open(args.trec_file) as trec:
        raw_documents, trail = getDocuments(trecFile, trail)
        while len(raw_documents + trail) > 0:

            documents = splitTrecDocuments(raw_documents)
            tokenized_documents = cleanDocuments(documents)

            raw_documents, trail = getDocuments(trecFile, trail)




if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(usage=globals()['__doc__'], version='1.0.0')
        parser.add_argument ('--verbose', action='store_true', default=False, help='verbose output')
        parser.add_argument('-f', '--trec-file', action='store', help='File in Trec format', required=True)
        args = parser.parse_args()
        if args.verbose: print time.asctime()
        main()
        if args.verbose: print time.asctime()
        if args.verbose: print 'TOTAL TIME IN MINUTES:',
        if args.verbose: print (time.time() - start_time) / 60.0
        sys.exit(0)
    except KeyboardInterrupt, e: # Ctrl-C
        raise e
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
