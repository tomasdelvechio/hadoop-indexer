#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

	trec-stats.py [-h,--help] [-v,--verbose] [--version] -f,--trec-file file.trec

DESCRIPTION

	Genera estadisticas de una colección Trec, asume un documento por linea

AUTHOR

	Tomas Delvechio <tdelvechio@unlu.edu.ar>

LICENSE

	GPLv3

VERSION

	0.0.1
"""

import sys
import os
import traceback
import argparse
import re
import operator
import time

regexTrecDocument = r"<DOC>\s*<DOCNO>\s*(?P<docid>\w*)</DOCNO>(?P<content>.*?)</DOC>(?P<trail>.*?)\n"
docLimit = 1000000 # Cantidad de documentos limites a procesar antes de guardar en disco resultados y sumarizar stats
MIN_WORD_SIZE = 3


class TrecDocument(object):

	def __init__(self, *initial_data, **kwargs):
		for dictionary in initial_data:
			for key in dictionary:
				setattr(self, key, dictionary[key])
		for key in kwargs:
			setattr(self, key, kwargs[key])

		# otros atributos
		self.tokens = []
		#self.coleccion = coleccion


	def cleanToken(self, token):
		return token


	def formatTokens(self):
		lowerContent = self.content.lower()
		regex = re.compile('\W')
		alphanumericContent = regex.sub(' ', lowerContent)
		candidateTokens = alphanumericContent.split()
		return candidateTokens


	def buildTokens(self):
		candidateTokens = self.formatTokens()
		for token in candidateTokens:
			candidato = self.cleanToken(token)
			if self.isValidToken(candidato):
				self.tokens.append(candidato)


	def stopword(self, token):
		return token in self.coleccion.stopwords


	def validLenght(self, token):
		return len(token) >= MIN_WORD_SIZE


	def isValidToken(self, token):
		if not self.stopword(token) and self.validLenght(token) and token is not None:
			return True
		return False


	def __len__(self):
		return len(self.tokens)


	def __iter__(self):
		return iter(self.tokens)


class TrecDocumentBuilder(object):

	def __init__(self, regex):
		self.regex = re.compile(regex)


	def buildDocument(self, documentString, coleccion):
		document = self.regex.match(documentString)
		trecDocument = TrecDocument(document.groupdict(), {'coleccion':coleccion})
		trecDocument.buildTokens()
		return trecDocument


class TrecCollection(object):

	def __init__(self, stats, stopwordsfile, limit=100):
		self.limit = limit
		self.documents = []
		self.stats = stats
		self.clean = False # Indica si deben limpiarse los documentos o no

		try:
			with open(stopwordsfile) as f:
				self.stopwords = f.read().splitlines()
		except IOError as e:
			self.stopwords = []


	def __iter__(self):
		return iter(self.documents)


	def toggleClean(self):
		self.clean = not self.clean


	def addDocument(self, trecDocument):
		self.documents.append(trecDocument)
		self.stats.addDocument()
		self.stats.addTokens(trecDocument)
		if len(self.documents) >= self.limit:
			self.sumarizar()


	def sumarizar(self):
		# Calcula las estadistica para los documentos que tiene cargados y las
		#   persiste en el objeto de estadisticas
		self.stats.frecuenciasTerminos(self)
		self.documents = []


	def saveStats(self):
		self.stats.save(self)


class TrecStats(object):

	def __init__(self):
		self.stats = { 'len': 0, # La cantidad de documentos de la coleccion
					   'tokens': 0, # Cantidad de tokens validos en la coleccion
					 }

	def addDocument(self):
		self.append('len', 1)


	def addTokens(self, trecDocument):
		self.append('tokens', len(trecDocument))


	def append(self, statName, data):
		if isinstance(self.stats[statName],int):
			self.stats[statName] += data


	def avgPorDocumento(self):
		self.stats['avgTknXdoc'] = self.stats['tokens'] / float(self.stats['len'])


	def frecuenciasTerminos(self, collection):
		"""
		Calcula la frecuencia de los terminos de la coleccion
		"""
		if not hasattr(self, 'frecuencias'):
			self.frecuencias = {}

		for document in collection:
			for term in document:
				if term in self.frecuencias:
					self.frecuencias[term] += 1
				else:
					self.frecuencias[term] = 1


	def diferentesTerminos(self):
		self.stats['diferentesTerminos'] = len(self.frecuencias.keys())


	def computarEstadisticas(self, collection):
		"""
		Este metodo se ejecuta al final del procesamiento para calcular stats
		globales que se deriven de otras recolectadas.
		"""

		# Computar Tokens promedio por documento
		self.avgPorDocumento()

		# Calcular histograma de frecuencias de terminos
		self.frecuenciasTerminos(collection)

		# Calcula cantidad de terminos diferentes en la coleccion
		self.diferentesTerminos()


	def save(self, collection):
		global args
		self.computarEstadisticas(collection)

		# Principales Stats
		fstat = open('100-estadisticas.txt', 'a')
		fstat.write('\n=== Archivo ===================')
		fstat.write('\n===============================\n')
		fstat.write("Nombre: %s" % os.path.basename(args.trec_file))
		fstat.write('\n')
		fstat.write("Tamaño (Bytes): %s" % os.stat(args.trec_file).st_size)
		fstat.write('\n')
		#import pdb; pdb.set_trace()
		fstat.write("Tamaño (GB): %s" % str(os.stat(args.trec_file).st_size / 1024.0**3))
		fstat.write('\n')
		fstat.write('\n=== Estadisticas ===================\n')
		fstat.write(repr(self.stats))

		# Top - 5, 10, 20, 50, 100
		sortedFreq = sorted(self.frecuencias.items(), key=operator.itemgetter(1))
		sortedFreq.reverse()
		fstat.write('\n=== Frecuencias ===================')
		fstat.write('\n=== TOP 5 ===================\n')
		fstat.write(repr(sortedFreq[:5]))
		fstat.write('\n=== TOP 10 ===================\n')
		fstat.write(repr(sortedFreq[:10]))
		fstat.write('\n=== TOP 20 ===================\n')
		fstat.write(repr(sortedFreq[:20]))
		fstat.write('\n=== TOP 50 ===================\n')
		fstat.write(repr(sortedFreq[:50]))
		fstat.write('\n=== TOP 100 ===================\n')
		fstat.write(repr(sortedFreq[:100]))
		fstat.close()


def main():
	global args

	stats = TrecStats()
	collection = TrecCollection(stats=stats, stopwordsfile=args.stopwords_file)
	docBuilder = TrecDocumentBuilder(regex=regexTrecDocument)
	docActual = 0
	with open(args.trec_file) as collectionFile:
		for document in collectionFile:
			trecDocument = docBuilder.buildDocument(document, collection)
			collection.addDocument(trecDocument)
			# Mostrar un resumen de stats cada 5% de documentos procesados
			docActual += 1
			if(docActual % 100 == 0):
				print("Procesando Documento: ", docActual)
	collection.saveStats()

if __name__ == '__main__':
	try:
		parser = argparse.ArgumentParser(usage=globals()['__doc__'], version='1.0.0')
		parser.add_argument ('--verbose', action='store_true', default=False, help='verbose output')
		parser.add_argument('-f', '--trec-file', action='store', help='File in OL Trec format', required=True)
		parser.add_argument('-s', '--stopwords-file', action='store', default=None, help='File of Stop Words', required=False)
		args = parser.parse_args()
		main()
		sys.exit(0)
	except KeyboardInterrupt, e: # Ctrl-C
		raise e
	except SystemExit, e: # sys.exit()
		raise e
	except Exception, e:
		print 'ERROR, UNEXPECTED EXCEPTION'
		print str(e)
		traceback.print_exc()
		os._exit(1)
