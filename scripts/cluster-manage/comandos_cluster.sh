
# Copiar archivos de configuracion entre slaves
scp /usr/local/hadoop/etc/hadoop/mapred-site.xml hduser@slave6:/usr/local/hadoop/etc/hadoop/mapred-site.xml
scp /usr/local/hadoop/etc/hadoop/masters hduser@slave6:/usr/local/hadoop/etc/hadoop/masters
scp /usr/local/hadoop/etc/hadoop/slaves hduser@slave6:/usr/local/hadoop/etc/hadoop/slaves
scp /usr/local/hadoop/etc/hadoop/yarn-site.xml hduser@slave6:/usr/local/hadoop/etc/hadoop/yarn-site.xml
scp /usr/local/hadoop/etc/hadoop/hdfs-site.xml hduser@slave6:/usr/local/hadoop/etc/hadoop/hdfs-site.xml
scp /usr/local/hadoop/etc/hadoop/core-site.xml hduser@slave6:/usr/local/hadoop/etc/hadoop/core-site.xml

# Copiar script
scp pruebas0N.sh hduser@slave1:/home/hduser/
scp pruebas0N.sh hduser@slave2:/home/hduser/
scp pruebas0N.sh hduser@slave3:/home/hduser/
scp pruebas0N.sh hduser@slave4:/home/hduser/
scp pruebas0N.sh hduser@slave5:/home/hduser/
scp pruebas0N.sh hduser@slave6:/home/hduser/

# Copiar jar file
scp Tesis.jar hduser@slave6:/home/hduser/
scp Tesis.jar hduser@slave5:/home/hduser/
scp Tesis.jar hduser@slave4:/home/hduser/
scp Tesis.jar hduser@slave3:/home/hduser/
scp Tesis.jar hduser@slave2:/home/hduser/
scp Tesis.jar hduser@slave1:/home/hduser/

# Copiar lib
scp /home/hduser/lib/JavaFastPFOR-0.1.7.jar hduser@slave1:/home/hduser/lib/
scp /home/hduser/lib/JavaFastPFOR-0.1.7.jar hduser@slave2:/home/hduser/lib/
scp /home/hduser/lib/JavaFastPFOR-0.1.7.jar hduser@slave3:/home/hduser/lib/
scp /home/hduser/lib/JavaFastPFOR-0.1.7.jar hduser@slave4:/home/hduser/lib/
scp /home/hduser/lib/JavaFastPFOR-0.1.7.jar hduser@slave5:/home/hduser/lib/
scp /home/hduser/lib/JavaFastPFOR-0.1.7.jar hduser@slave6:/home/hduser/lib/

# Eliminar logs viejos y datos
rm /usr/local/hadoop/hadoop_store/tmp/* -rf
rm /usr/local/hadoop/hadoop_store/hdfs/namenode/* -rf
rm /usr/local/hadoop/hadoop_store/hdfs/datanode/* -rf
rm /usr/local/hadoop/logs/* -rf
