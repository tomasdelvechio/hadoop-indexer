#!/usr/bin/env python
import os
import sys

# Este script toma como input un trec file "comun" y lo devuelve formateado con un documento por linea

inputTrecFilename = sys.argv[1]
outputTrecFilename = sys.argv[2]

START_DOCUMENT_TREC_TAG = '<DOC>'
END_DOCUMENT_TREC_TAG = '</DOC>'
STAR_DOCUMENT_ID_TAG = '<DOCNO>'
END_DOCUMENT_ID_TAG = '</DOCNO>'

CREADO = 0
ABIERTO = 1
CERRADO = 2


class Documento:
    __lastDocumentId = -1

    def __init__(self):
        self.__trecTags = [START_DOCUMENT_TREC_TAG, END_DOCUMENT_TREC_TAG, '\r']
        self.__estado = self.__getEstadoInicial()
        self.__contenido = None

    def __getEstadoInicial(self):
        return CREADO

    def iniciarDocumento(self):
        if self.__estaCreado():
            self.__estado = self.__getSiguienteEstado('iniciar')
            self.__contenido = []

    def __estaCreado(self):
        return self.__estado == CREADO

    def agregarContenido(self, contenido):
        if self.__estaAbierto():
            contenidoLimpio = self.__limpiarContenido(contenido)
            self.__contenido.append(contenidoLimpio)
        else:
            raise Exception('No se puede agregar contenido a este documento porque no fue iniciado correctamente')

    def __estaAbierto(self):
        return self.__estado == ABIERTO

    def __limpiarContenido(self, contenido):
        contenido = self.__removeTags(contenido)
        contenido = self.__removeWhitespaces(contenido)
        return contenido

    def __removeTags(self, contenido):
        for tag in self.__trecTags:
            contenido = contenido.replace(tag," ")
        return contenido

    def __removeWhitespaces(self, contenido):
        return contenido.strip() + " "

    def cerrarDocumento(self):
        if self.__estaAbierto():
            self.__estado = self.__getSiguienteEstado('cerrar')

    # Maquina de estados
    def __getSiguienteEstado(self, accion):
        if self.__estaCreado() and accion == 'iniciar':
            return ABIERTO
        elif self.__estaAbierto() and accion == 'cerrar':
            return CERRADO

    def getDocumento(self):
        if self.__estaCerrado():
            contenido = " ".join(self.__contenido)
            contenido = self.__removeDocIdContent(contenido)
            return "<DOC><DOCNO>%s</DOCNO>%s</DOC>\n" % (self.__nextDocumentId(), contenido.strip())

    def __estaCerrado(self):
        return self.__estado == CERRADO

    def __nextDocumentId(self):
        Documento.__lastDocumentId += 1
        return Documento.__lastDocumentId

    def __removeDocIdContent(self, contenido):
        startDocIdContent = contenido.find(STAR_DOCUMENT_ID_TAG)
        endDocIdContent = contenido.find(END_DOCUMENT_ID_TAG) + len(END_DOCUMENT_ID_TAG)
        return contenido[:startDocIdContent] + contenido[endDocIdContent:]

def grabarDocumento(destinationFile, documento):
    destinationFile.write(documento)

def comienzaDocumento(line):
    return containTag(line, START_DOCUMENT_TREC_TAG) and line.startswith(START_DOCUMENT_TREC_TAG)

def containTag(line, tag):
    return False if line.find(tag) == -1 else True


with open(inputTrecFilename) as sourceFile, open(outputTrecFilename, 'w') as destinationFile:

    documento = None

    for line in sourceFile:

        if comienzaDocumento(line):
            if documento is not None:
                documento.cerrarDocumento()
                grabarDocumento(destinationFile, documento.getDocumento())
            documento = Documento()
            documento.iniciarDocumento()

        documento.agregarContenido(line)

