#!/usr/bin/env python

import os
import sys

if len(sys.argv) < 2:
    sys.exit("Falta path a archivo trec a renumerar")

path = sys.argv[1]
filename = os.path.basename(path)
outputFile = open('output_'+filename, 'w')
corteDeControl = 10000
nextDocId = -1

template = "<DOC><DOCNO>{id}</DOCNO>{document}</DOC>\n"

def getStartDocument(line):
    return line.find("</DOCNO>") + len("</DOCNO>")

def getEndDocument(line):
    return line.find("</DOC>\n")

def getDocumentIndex(line):
    start = getStartDocument(line)
    end = getEndDocument(line)
    return start, end

def getDocument(line):
    start, end = getDocumentIndex(line)
    return line[start:end]

if os.path.exists(path):
    with open(path) as file:
        for line in file:
            document = getDocument(line)
            nextDocId += 1
            outputFile.write(template.format(id=nextDocId, document=document))

            # Feedback al usuario
            if nextDocId % corteDeControl == 0:
                print("Procesados {cantidad} documentos".format(cantidad=nextDocId))



