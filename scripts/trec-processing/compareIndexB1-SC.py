#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
SYNOPSIS

    python compareIndexB1-SC.py <vocabularyText> <indexTextDoc> <indexTextFreq> <vocabularyBinary> <indexBinaryDoc> <indexBinaryFreq>

DESCRIPTION

    Script que compara dos indices que deberian ser identicos, pero que
    se encuentran en formato texto y binario. El Indice en formato texto
    permite el debug de humanos, y el binario es tedioso. Este script
    intenta recorrer ambos indices y demostrar que contienen la misma
    informacion.

AUTHOR

    Tomas Delvechio <tomasdelvechio17@gmail.com>

LICENSE

    GPLv3.

VERSION

    0.1
"""

import sys, os, traceback, optparse
import time
import re

class Indice():

    def __init__(self):
        self.voc = None
        self.doc = None
        self.freq = None
        self.limit = None
        self.procesados = 0

    def setVocabulary(self, vocabularyPath):
        self.voc = open(vocabularyPath)
        return self

    def setDocument(self, documentPath):
        self.doc = open(documentPath)
        return self

    def setFrequency(self, frequencyPath):
        self.freq = open(frequencyPath)
        return self

    def setLimit(self, limit):
        self.limit = limit
        return self


def procesarIndices(indiceText, indiceBinary):

    # Recoger siguiente registro de ambos indices
    nextTermText = indiceText.
    # Comparar cantidad de DocIds de ambos
    # Comparar cantidad de frecuencias



if __name__ == '__main__':

    # Cantidad de registros a comparar, es para evitar recorrer indices
    #   largos de forma completa. -1 para recorrer todos.
    limitToCompare = 10

    if len(sys.argv) < 3:
        sys.exit("Faltan parametros")

    indiceText = Indice()
    indiceText = indiceText.setVocabulary(sys.argv[1])
                           .setDocument(sys.argv[2])
                           .setFrequency(sys.argv[3])
                           .setLimit(limitToCompare)

    indiceBinary = Indice()
    indiceBinary = indiceBinary.setVocabulary(sys.argv[4])
                               .setDocument(sys.argv[5])
                               .setFrequency(sys.argv[6])
                               .setLimit(limitToCompare)

    procesarIndices(indiceText, indiceBinary)

