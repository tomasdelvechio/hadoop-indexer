#!/usr/bin/env python
import os
import sys
import fileinput

trecFilePath = None
try:
    trecFilePath = sys.argv[1]
except IndexError:
    pass

targetsToCount = ['<DOC>', '</DOC>', '\n', '\r', '<DOCNO>', '</DOCNO>']
numberOfLines = 0
histogramOfTargets = {}
documentosConTagsIncorrecto = {}

def isPresent(stringToSearch, line):
    return False if line.find(stringToSearch) == -1 else True

def recuperarDocID(line):
    start = line.find("<DOCNO>") + len("<DOCNO>")
    end = line.find("</DOCNO>")
    return line[start:end].strip()

def processingLine(line):
    global numberOfLines
    numberOfLines += 1

    for target in targetsToCount:
        if isPresent(target, line):
            numberOfTagPresentOnLine = line.count(target)
            try:
                histogramOfTargets[target] += numberOfTagPresentOnLine
            except KeyError:
                histogramOfTargets[target] = numberOfTagPresentOnLine

            # Esto solo funciona en One Line Trec, Comentar si el doc es diferente
            if numberOfTagPresentOnLine > 1:
                #docId = recuperarDocID(line)
                # Si el doc tiene problemas, agrego el numero de linea
                try:
                    documentosConTagsIncorrecto[target].append(numberOfLines)
                except KeyError:
                    documentosConTagsIncorrecto[target] = [numberOfLines]

if trecFilePath is not None:
    with open(trecFilePath) as fileDescriptor:
        for line in fileDescriptor:
            processingLine(line)
else:
    for line in fileinput.input():
        processingLine(line)

print "Cantidad de lineas: ", numberOfLines
for target in histogramOfTargets.keys():
    print "Cantidad de tags %s: %s" % (target, histogramOfTargets[target])
print "Docs con Tags incorrectos", documentosConTagsIncorrecto

