#!/usr/bin/env python
import os
import sys
import fileinput

trecFilePath = None
try:
    trecFilePath = sys.argv[1]
except IndexError:
    pass

numberOfLines = 0
problemasEncontrados = {}

def processingLine(line):
    global numberOfLines
    numberOfLines += 1

    if not line.startswith('<DOC>'):
        agregarProblema('DocNoIniciaConDocTag', numberOfLines)

    if not line.endswith('</DOC>\n'):
        agregarProblema('DocNoTerminaCon/DocTag', numberOfLines)

    if not (isPresent('<DOCNO>', line) or isPresent('</DOCNO>', line)):
        agregarProblema('DocIdMalFormado', numberOfLines)
    else:
        docNoStartIndex = line.find('<DOCNO>') + len('<DOCNO>')
        docNoEndIndex = line.find('</DOCNO>')
        docNoContent = line[docNoStartIndex:docNoEndIndex].strip()
        if not docNoContent.isdigit():
            agregarProblema('DocNoContentConContenidoIncorrecto', numberOfLines)

def agregarProblema(tipoDeProblema, numeroDeLineaConProblema):
    try:
        problemasEncontrados[tipoDeProblema].append(numeroDeLineaConProblema)
    except KeyError, e:
        problemasEncontrados[tipoDeProblema] = [numeroDeLineaConProblema]

def isPresent(stringToSearch, line):
    return False if line.find(stringToSearch) == -1 else True

if trecFilePath is not None:
    with open(trecFilePath) as fileDescriptor:
        for line in fileDescriptor:
            processingLine(line)
else:
    for line in fileinput.input():
        processingLine(line)

print "Cantidad de lineas: ", numberOfLines
print problemasEncontrados

