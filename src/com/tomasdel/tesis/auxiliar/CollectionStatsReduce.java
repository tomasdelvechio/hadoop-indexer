/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.auxiliar;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author tomas
 */
class CollectionStatsReduce extends Reducer<IntWritable, IntWritable, Text, IntWritable> {
    
    Integer cantidadDocumentos;
    Integer cantidadTotalTerminos;
    private TreeMap<Integer, Integer> top20;
    
    @Override
    public void setup(Context context) throws IOException, InterruptedException {
        top20 = new TreeMap<>();
        cantidadDocumentos = 0;
        cantidadTotalTerminos = 0;
    }
    
    @Override
    public void reduce(IntWritable documentId, Iterable<IntWritable> cantidadTerminos, Context context)
      throws IOException, InterruptedException {
        ++cantidadDocumentos;
        Integer docFreq = 0;
        for (IntWritable termino : cantidadTerminos) {
            docFreq += termino.get();
        }
        cantidadTotalTerminos += docFreq;
        if(top20.size() > 0 && top20.firstKey() < docFreq)
            top20.remove(top20.firstKey());
        
        if(top20.size() <= 20)
            top20.put(docFreq, documentId.get());
    }
    
    @Override
    public void cleanup(Context context) throws IOException, InterruptedException {
        Integer mediaTerminosPorDocumento = cantidadTotalTerminos / cantidadDocumentos ;
        context.write(new Text("Media Terminos por Documento"), new IntWritable(mediaTerminosPorDocumento));
        
        context.write(new Text("Top 20 Documentos/Terminos"), new IntWritable(0));
        for (Map.Entry<Integer, Integer> entry : top20.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            
            context.write(new Text(value.toString()), new IntWritable(key));
        }
    }
    
}
