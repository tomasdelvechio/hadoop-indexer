/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.auxiliar;

import com.tomasdel.tesis.indexer.commons.TrecOneLineDocument;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author tomas
 */
class CollectionStatsMap extends Mapper<LongWritable, Text, IntWritable, IntWritable> {
    
    @Override
    public void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, IntWritable>.Context context) throws IOException, InterruptedException {
        TrecOneLineDocument document = new TrecOneLineDocument(value.toString());
        if (document.isParsed()) {
            Integer cantidadTerminos = 0;
            IntWritable docId = new IntWritable(document.getDocId());
            while (document.tokenizer.hasMoreTokens()) {
                String term = document.tokenizer.nextToken();
                if (term != null) {
                    cantidadTerminos++;
                }
            }
            IntWritable cantidad = new IntWritable(cantidadTerminos);
            context.write(docId, cantidad);
        }
    }
}
