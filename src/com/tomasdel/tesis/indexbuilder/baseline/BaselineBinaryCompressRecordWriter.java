/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.baseline;

import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import com.tomasdel.tesis.indexer.commons.structs.BlockCompress;
import com.tomasdel.tesis.indexer.commons.structs.BlockOutOfBoundsException;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class BaselineBinaryCompressRecordWriter extends BaselineRecordWriter {
    private BlockCompress block = new BlockCompress();
    private String postingElementSeparator;
    
    public BaselineBinaryCompressRecordWriter(FSDataOutputStream postingFileOutputStream, FSDataOutputStream freqFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) {
        super(postingFileOutputStream, freqFileOutputStream, vocabularyFileOutputStream, tac);
        
        postingElementSeparator = ",";
    }
    
    /**
     * Implement a sequence of tasks to write the whole index
     * @param term A term to be write
     * @param postingList A list with doc id and frequence by term
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        writeVocabulary(term, postingList.size());
        writePosting(postingList);
    }
    
    /**
     * Write a text-based, uncompressed, persistent posting list
     * 
     * Posting format: Two files. DocId file and Frequence file.
     * 
     * DocId File
     * 
     *  <docId1><PostingSeparator><docId2><PostingSeparator><docId3><PostingListSeparator>
     * 
     * Where:
     *  <docIdN> are a pointer to collection's document.
     *  <PostingSeparator>
     *  <PostingListSeparator>
     * @param postingList A list with doc id and frequence by term
     * @throws IOException
     */
    @Override
    protected void writePosting(PostingList postingList) throws IOException {
        this.block.clear();
        for (Map.Entry<Integer, Integer> postingEntry : postingList.getEntrySet()) {
            Integer docId = postingEntry.getKey();
            Integer freq = postingEntry.getValue();
            while(true) {
                try {
                    block.addPosting(docId, freq);
                    break;
                } catch (BlockOutOfBoundsException e) {
                    writeBlock();
                    this.block.clear();
                }
            }
        }
        writeBlock(); // Escribe el ultimo bloque
    }

    @Override
    protected void writeVocabulary(Text termino, Integer postingListSize) throws IOException {
        vocabularyOutput.writeBytes(termino.toString() + 
                                    this.postingFieldSeparator +
                                    postingListSize.toString() +
                                    this.postingFieldSeparator +
                                    postingOutput.getPos() +
                                    this.postingElementSeparator +
                                    freqOutput.getPos() +
                                    this.postingListSeparator);
    }
    
    /**
     * Persiste el bloque de forma adecuada respetando el metodo de persistencia
     * 
     * @throws IOException 
     */
    private void writeBlock() throws IOException {
        writeDocIds();
        writeFreqs();
    }

    private void writeDocIds() throws IOException {
        int[] docIds = this.block.getDocIds();
        // Header
        postingOutput.writeInt(docIds.length);
        // Data
        for (int docId : docIds) {
            postingOutput.writeInt(docId);
        }
    }

    private void writeFreqs() throws IOException {
        int[] freqs = this.block.getFreqs();
        // Header
        freqOutput.writeInt(freqs.length);
        // Data
        for (int freq : freqs) {
            freqOutput.writeInt(freq);
        }
    }
    
}
