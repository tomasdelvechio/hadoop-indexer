/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.baseline;

import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class BaselineRecordWriter extends RecordWriter<Text, PostingList> {
    protected FSDataOutputStream postingOutput;
    protected FSDataOutputStream vocabularyOutput;
    protected FSDataOutputStream freqOutput;
    protected Integer N;
    protected Integer taskId;
    protected Integer localTermCounter;
    protected Integer termId;
    protected final String postingListSeparator;
    protected final String postingFieldSeparator;
    
    public BaselineRecordWriter(FSDataOutputStream postingFileOutputStream, FSDataOutputStream freqFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) {
        this.postingOutput = postingFileOutputStream;
        this.freqOutput = freqFileOutputStream;
        this.vocabularyOutput = vocabularyFileOutputStream;
        
        // Set parameters to build TermId
        this.N = Integer.parseInt(tac.getConfiguration().get("mapred.reduce.tasks"));
        this.taskId = tac.getTaskAttemptID().getTaskID().getId();
        this.localTermCounter = 0;
        
        // Set parameters of posting format
        this.postingFieldSeparator = ":";
        this.postingListSeparator = "\n";
    }
    /**
     * Implement a basic sequence of tasks to write the whole index
     * @param term A term to be write
     * @param postingList A list with doc id and frequence by term
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        Integer postingListLenght = postingList.size();
        writePosting(postingList);
        writeVocabulary(term, postingListLenght);
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        postingOutput.close();
        freqOutput.close();
        vocabularyOutput.close();
    }

    protected void writeVocabulary(Text term, Integer postingListLenght) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected void writePosting(PostingList postingList) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
