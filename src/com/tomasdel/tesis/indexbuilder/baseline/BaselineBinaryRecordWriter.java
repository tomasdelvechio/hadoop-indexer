/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.baseline;

import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 * Especifica un RecordWriter en modo Binario para un indice invertido
 * 
 * IndexRecordWriter ofrece una salida que construye un indice invertido con 
 * las siguientes caracteristicas:
 * 
 *  - Archivo de postings con compresion delta-gap para documents ID
 *  - Archivo de frecuencias sin compresion
 *  - Archivo de vocabulario con formato <term>:<termId>
 * 
 * El <termId> es global y unico para cada termino.
 * 
 * @author tomas
 */
class BaselineBinaryRecordWriter extends BaselineRecordWriter {
    private long postingStartByte;
    
    public BaselineBinaryRecordWriter(FSDataOutputStream postingFileOutputStream, FSDataOutputStream freqFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) {
        super(postingFileOutputStream, freqFileOutputStream, vocabularyFileOutputStream, tac);
    }
    
    /**
     * Write a text-based, uncompressed, persistent posting list
     * 
     * Posting format: Two files. DocId file and Frequence file.
     * 
     * DocId File
     * 
     *  <docId1><PostingSeparator><docId2><PostingSeparator><docId3><PostingListSeparator>
     * 
     * Where:
     *  <docIdN> are a pointer to collection's document.
     *  <PostingSeparator>
     *  <PostingListSeparator>
     * @param postingList A list with doc id and frequence by term
     * @throws IOException
     */
    @Override
    protected void writePosting(PostingList postingList) throws IOException {
        this.postingStartByte = postingOutput.getPos();
        for (Map.Entry<Integer, Integer> postingEntry : postingList.getEntrySet()) {
            Integer docActual = postingEntry.getKey();
            Integer freqActual = postingEntry.getValue();
            postingOutput.writeInt(docActual);
            freqOutput.writeInt(freqActual);
        }
    }

    @Override
    protected void writeVocabulary(Text termino, Integer postingListSize) throws IOException {
        vocabularyOutput.writeBytes(termino.toString() + 
                                    this.postingFieldSeparator + 
                                    postingListSize.toString() +
                                    this.postingFieldSeparator + 
                                    this.postingStartByte +
                                    this.postingListSeparator);
    }
}
