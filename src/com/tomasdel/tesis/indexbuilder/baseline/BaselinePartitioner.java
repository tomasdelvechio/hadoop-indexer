/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.baseline;

import com.tomasdel.tesis.indexer.commons.structs.KeyPair;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 *
 * @author tomas
 */
public class BaselinePartitioner extends Partitioner<KeyPair, IntWritable> {

    @Override
    public int getPartition(KeyPair key, IntWritable value, int numeroParticiones) {
        return (key.getTerm().hashCode() & Integer.MAX_VALUE) % numeroParticiones;
    }
    
}
