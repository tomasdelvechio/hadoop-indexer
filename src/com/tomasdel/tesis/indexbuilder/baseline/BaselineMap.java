/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.baseline;

import com.tomasdel.tesis.indexer.commons.TrecOneLineDocument;
import com.tomasdel.tesis.indexer.commons.structs.KeyPair;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author tomas
 */
public class BaselineMap extends Mapper<LongWritable, Text, KeyPair, IntWritable> {
        
    private TreeMap<KeyPair, Integer> partialPosting ;
    
    @Override
    public void setup(Mapper.Context context) throws IOException, InterruptedException {
        partialPosting = new TreeMap<>();
    }
    
    @Override
    public void map(LongWritable key, Text value, Mapper<LongWritable, Text, KeyPair, IntWritable>.Context context) throws IOException, InterruptedException {
        TrecOneLineDocument document = new TrecOneLineDocument(value.toString());
        if (document.isParsed()) {
            while (document.tokenizer.hasMoreTokens()) {
                String term = document.tokenizer.nextToken();
                if (term != null) {
                    KeyPair kp = new KeyPair(term, document.getDocId());
                    Integer oldFreq = partialPosting.get(kp);
                    if (oldFreq == null)
                        partialPosting.put(kp, 1);
                    else
                        partialPosting.put(kp, oldFreq+1);
                }
            }
            for(Map.Entry<KeyPair, Integer> posting : partialPosting.entrySet()) {                
                context.write(posting.getKey(), new IntWritable(posting.getValue()));
            }
            partialPosting = new TreeMap<>();
        }
    }
}
