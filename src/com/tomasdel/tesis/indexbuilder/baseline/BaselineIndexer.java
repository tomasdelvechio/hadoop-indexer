/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.baseline;

import com.tomasdel.tesis.indexer.commons.structs.KeyPair;
import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Indexador de referencia que soporta operaciones sencillas e implementa el 
 * indice que posee estructura tratada en la literatura basica de recuperacion
 * de informacion.
 * 
 * @author tomas
 */
public class BaselineIndexer extends Configured implements Tool {
    
    protected Job job;
    
    /**
     * The main method for execute the Index Builder.
     * 
     * @param args String[] The parameters passed to application
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int res;
        res = ToolRunner.run(new Configuration(), new BaselineIndexer(), args);
        System.exit(res);
    }
    
    @Override
    public int run(String[] args) throws Exception {
        
        Configuration conf = this.getConf();
        
        job = Job.getInstance(conf);
        
        /* Recuperar parametros */
        Boolean compressBlock;
        compressBlock = Boolean.valueOf(job.getConfiguration().get("indexer.struct.block.compress", "false"));
        Boolean binaryFormat;
        binaryFormat = Boolean.valueOf(job.getConfiguration().get("indexer.file.format.binary", "false"));
        
        job.setJarByClass(BaselineIndexer.class);
        
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(PostingList.class);
        
        job.setInputFormatClass(TextInputFormat.class);
        
        if(compressBlock && binaryFormat) {
            job.setOutputFormatClass(BaselineBinaryCompressOutputFormat.class);
        } else if (compressBlock) {
            job.setOutputFormatClass(BaselineTextCompressOutputFormat.class);
        } else if (binaryFormat) {
            job.setOutputFormatClass(BaselineBinaryOutputFormat.class);
        } else {
            job.setOutputFormatClass(BaselineTextOutputFormat.class);
        }
        
        job.setMapOutputKeyClass(KeyPair.class);
        job.setMapOutputValueClass(IntWritable.class);
        
        job.setPartitionerClass(BaselinePartitioner.class);
        
        job.setMapperClass(BaselineMap.class);
        job.setReducerClass(BaselineReduce.class);
        
        FileInputFormat.addInputPath(job, new Path(args[0]));
        
        FileSystem fs = FileSystem.get(conf);
        if(fs.exists(new Path(args[1]))){
           /*If exist delete the output path*/
           fs.delete(new Path(args[1]),true);
        }
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
