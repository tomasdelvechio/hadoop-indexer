/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.blockmax;

import com.tomasdel.tesis.indexer.commons.IndexOutputFormat;
import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class BlockMaxTextOutputFormat extends IndexOutputFormat {
    @Override
    public RecordWriter<Text, PostingList> getRecordWriter(TaskAttemptContext tac) throws IOException, InterruptedException {
        FSDataOutputStream indexFileOutputStream = buildFileOutput(tac, "indexFile");
        FSDataOutputStream vocabularyFileOutputStream = buildFileOutput(tac, "vocabulary");
        return new BlockMaxTextRecordWriter(indexFileOutputStream, vocabularyFileOutputStream, tac);
    }
}
