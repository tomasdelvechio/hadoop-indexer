/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.blockmax;

import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import com.tomasdel.tesis.indexer.commons.structs.BlockCompress;
import com.tomasdel.tesis.indexer.commons.structs.BlockOutOfBoundsException;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class BlockMaxTextCompressRecordWriter extends RecordWriter<Text, PostingList> {

    private final FSDataOutputStream indexFileOutputStream;
    private final FSDataOutputStream vocabularyFileOutputStream;
    private final String postingFieldSeparator;
    private final String postingListSeparator;
    private final String postingElementSeparator;
    private final String postingBlockSeparator;
    private BlockCompress block = new BlockCompress();

    public BlockMaxTextCompressRecordWriter(FSDataOutputStream indexFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) {
        this.indexFileOutputStream = indexFileOutputStream;
        this.vocabularyFileOutputStream = vocabularyFileOutputStream;
        
        postingFieldSeparator = ":";
        postingListSeparator = "\n";
        postingElementSeparator = ",";
        postingBlockSeparator = ";";
    }
    
    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        Integer postingListLenght = postingList.size();
        writePosting(postingList);
        writeVocabulary(term, postingListLenght);
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        indexFileOutputStream.close();
        vocabularyFileOutputStream.close();
    }

    protected void writePosting(PostingList postingList) throws IOException {
        this.block.clear();
        for (Map.Entry<Integer, Integer> postingEntry : postingList.getEntrySet()) {
            Integer docId = postingEntry.getKey();
            Integer freq = postingEntry.getValue();
            while(true) {
                try {
                    this.block.addPosting(docId, freq);
                    break;
                } catch (BlockOutOfBoundsException e) {
                    writeBlock();
                    writeBlockSeparator();
                    this.block.clear();
                }
            }
        }
        writeBlock(); // Escribe el ultimo bloque
        writePostingListSeparator();
    }

    protected void writeVocabulary(Text termino, Integer postingListSize) throws IOException {
        vocabularyFileOutputStream.writeBytes(termino.toString() + 
                                    this.postingFieldSeparator + 
                                    postingListSize.toString() +
                                    this.postingListSeparator);
    }

    private void writeBlock() throws IOException {
        writeBlockMetadata();
        writeDocIds();
        indexFileOutputStream.writeBytes(postingFieldSeparator);
        writeFreqs();
    }

    private void writeBlockSeparator() throws IOException {
        indexFileOutputStream.writeBytes(postingBlockSeparator);
    }

    private void writePostingListSeparator() throws IOException {
        indexFileOutputStream.writeBytes(postingListSeparator);
    }
    
    private void writeBlockMetadata() throws IOException {
        int maxDocId = this.block.getMaxDocId();
        indexFileOutputStream.writeBytes(Integer.toString(maxDocId) + postingElementSeparator);
        int maxFreq = this.block.getMaxFreq();
        indexFileOutputStream.writeBytes(Integer.toString(maxFreq) + postingFieldSeparator);
    }

    private void writeDocIds() throws IOException {
        int[] docIds = this.block.getDocIds();
        String postingSeparator = "";
        for (int docId : docIds) {
            indexFileOutputStream.writeBytes(postingSeparator + Integer.toString(docId));
            postingSeparator = postingElementSeparator;
        }
    }

    private void writeFreqs() throws IOException {
        int[] freqs = this.block.getFreqs();
        String postingSeparator = "";
        for (int freq : freqs) {
            indexFileOutputStream.writeBytes(postingSeparator + Integer.toString(freq));
            postingSeparator = postingElementSeparator;
        }
    }
    
}
