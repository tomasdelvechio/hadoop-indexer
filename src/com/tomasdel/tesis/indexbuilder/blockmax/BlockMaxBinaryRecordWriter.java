/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.blockmax;

import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import com.tomasdel.tesis.indexer.commons.structs.Block;
import com.tomasdel.tesis.indexer.commons.structs.BlockOutOfBoundsException;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class BlockMaxBinaryRecordWriter extends RecordWriter<Text, PostingList> {

    private final FSDataOutputStream indexFileOutputStream;
    private final FSDataOutputStream vocabularyFileOutputStream;
    private final String postingFieldSeparator;
    private final String postingListSeparator;
    private final String postingElementSeparator;
    private Block block;

    public BlockMaxBinaryRecordWriter(FSDataOutputStream indexFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) {
        this.indexFileOutputStream = indexFileOutputStream;
        this.vocabularyFileOutputStream = vocabularyFileOutputStream;
        
        postingFieldSeparator = ":";
        postingListSeparator = "\n";
        postingElementSeparator = ",";
    }
    
    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        Integer postingListLenght = postingList.size();
        writeVocabulary(term, postingListLenght);
        writePosting(postingList);
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        indexFileOutputStream.close();
        vocabularyFileOutputStream.close();
    }

    protected void writePosting(PostingList postingList) throws IOException {
        this.block = new Block();
        for (Map.Entry<Integer, Integer> postingEntry : postingList.getEntrySet()) {
            Integer docId = postingEntry.getKey();
            Integer freq = postingEntry.getValue();
            while(true) {
                try {
                    this.block.addPosting(docId, freq);
                    break;
                } catch (BlockOutOfBoundsException e) {
                    writeBlock();
                    this.block = new Block();
                }
            }
        }
        writeBlock(); // Escribe el ultimo bloque
    }

    protected void writeVocabulary(Text termino, Integer postingListSize) throws IOException {
        vocabularyFileOutputStream.writeBytes(termino.toString() + 
                                    this.postingFieldSeparator + 
                                    postingListSize.toString() +
                                    this.postingListSeparator);
    }

    private void writeBlock() throws IOException {
        writeBlockMetadata();
        writeDocIds();
        writeFreqs();
    }
    
    private void writeBlockMetadata() throws IOException {
        int maxDocId = this.block.getMaxDocId();
        indexFileOutputStream.writeInt(maxDocId);
        int maxFreq = this.block.getMaxFreq();
        indexFileOutputStream.writeInt(maxFreq);
    }

    private void writeDocIds() throws IOException {
        int[] docIds = this.block.getDocIds();
        for (int docId : docIds) {
            indexFileOutputStream.writeInt(docId);
        }
    }

    private void writeFreqs() throws IOException {
        int[] freqs = this.block.getFreqs();
        for (int freq : freqs) {
            indexFileOutputStream.writeInt(freq);
        }
    }
    
}
