/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.treap;

import com.tomasdel.tesis.indexer.commons.structs.BlockCompress;
import com.tomasdel.tesis.indexer.commons.structs.BlockOutOfBoundsException;
import com.tomasdel.tesis.indexer.dg.main.treap.PostingListTreap;
import java.io.IOException;
import java.util.Enumeration;
import lib.org.order.Ordered;
import lib.org.order.Treap;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class TreapBinaryCompressRecordWriter extends RecordWriter<Text, PostingListTreap> {

    private final FSDataOutputStream indexFileStream;
    private final FSDataOutputStream vocabularyFileStream;
    private Treap treap;

    public TreapBinaryCompressRecordWriter(FSDataOutputStream indexFileOutput, FSDataOutputStream vocabularyFileOutput) {
        this.indexFileStream = indexFileOutput;
        this.vocabularyFileStream = vocabularyFileOutput;
    }
    
    @Override
    public void write(Text term, PostingListTreap postingList) throws IOException, InterruptedException {
        this.treap = postingList.getPosting();
        writeVocabulary(term);
        writeIndex(term);
    }
    
    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        indexFileStream.close();
        vocabularyFileStream.close();
    }
    
    private void writeVocabulary(Text term) throws IOException {
        vocabularyFileStream.writeBytes(term.toString());
        vocabularyFileStream.writeBytes(":");
        vocabularyFileStream.writeBytes(String.valueOf(treap.size()));
        vocabularyFileStream.writeBytes(":");
        vocabularyFileStream.writeBytes(String.valueOf(indexFileStream.getPos()));
        vocabularyFileStream.writeBytes("\n");
    }
    
    private void writeIndex(Text term) throws IOException {
        // Posting Format
        //  <bp structure><docId list><freq list>
        
        // Print <bp structure> section
        treap.writeStructure(indexFileStream);
        
        // block sections
        BlockCompress block = new BlockCompress();
        
        for (Enumeration treeKeys = treap.keys(true); treeKeys.hasMoreElements();) {
            Ordered key = (Ordered) treeKeys.nextElement();
            Integer freq = treap.get(key);
            Integer docId = Integer.parseInt(key.toString());
            try {
                block.addPosting(docId, freq);
            }catch (BlockOutOfBoundsException E) {
                writeBlock(block);
                block = new BlockCompress();
            }
        }
        writeBlock(block);
    }
    
    /**
     * Persiste el bloque de forma adecuada respetando el metodo de persistencia
     * 
     * @throws IOException 
     */
    private void writeBlock(BlockCompress block) throws IOException {
        int[] docIds = block.getDocIds();
        int[] freqs = block.getFreqs();
        
        //Block header
        indexFileStream.writeInt(docIds.length);
        indexFileStream.writeInt(freqs.length);
                
        // Block data
        writeElements(docIds);
        writeElements(freqs);
    }

    private void writeElements(int[] elements) throws IOException {
        for (int elem : elements) {
            indexFileStream.writeInt(elem);
        }
    }
    
}
