/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.treap;

import com.tomasdel.tesis.indexer.commons.structs.KeyPair;
import com.tomasdel.tesis.indexer.commons.structs.PostingList;
import com.tomasdel.tesis.indexer.dg.main.treap.PostingListTreap;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author tomas
 */
class TreapReduce extends Reducer<KeyPair, IntWritable, Text, PostingListTreap> {
    
    Text terminoAnterior ;
    Text term ;
    PostingListTreap posting ;
    
    @Override
    public void setup(Context context) throws IOException, InterruptedException {
        terminoAnterior = new Text();
        term = null;
        posting = new PostingListTreap();
    }
    
    @Override
    public void reduce(KeyPair key, Iterable<IntWritable> frequencies, Context context)
      throws IOException, InterruptedException {
        term = key.getTerm();
        if (terminoAnterior.getLength() != 0 && !terminoAnterior.equals(term)) {
            context.write(terminoAnterior, posting);
            posting.reset();
        }
        Integer freq = 0;
        for (IntWritable f : frequencies) {
            freq += f.get();
        }
        posting.addPosting(key.getDocId().get(), freq);
        terminoAnterior.set(term);
    }
    
    @Override
    public void cleanup(Context context) throws IOException, InterruptedException {
        context.write(terminoAnterior, posting);
    }
}
