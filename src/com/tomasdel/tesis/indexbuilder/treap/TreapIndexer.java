/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.treap;

import com.tomasdel.tesis.indexer.commons.structs.KeyPair;
import com.tomasdel.tesis.indexer.dg.main.treap.PostingListTreap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * Indexador que construye un indice en formato Treap
 * 
 * @author tomas
 */
public class TreapIndexer extends Configured implements Tool {
    
    protected Job job;
    
    /**
     * The main method for execute the Index Builder.
     * 
     * @param args String[] The parameters passed to application
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int res;
        res = ToolRunner.run(new Configuration(), new TreapIndexer(), args);
        System.exit(res);
    }
    
    @Override
    public int run(String[] args) throws Exception {
        
        Configuration conf = this.getConf();
        
        job = Job.getInstance(conf);
        
        /* Recuperar parametros */
        Boolean compressBlock;
        compressBlock = Boolean.valueOf(job.getConfiguration().get("indexer.struct.block.compress", "false"));
        Boolean binaryFormat;
        binaryFormat = Boolean.valueOf(job.getConfiguration().get("indexer.file.format.binary", "false"));
        
        /* Job Configuration */
        job.setJarByClass(TreapIndexer.class);
        
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(PostingListTreap.class);
        
        job.setInputFormatClass(TextInputFormat.class);
        
        if(compressBlock && binaryFormat) {
            job.setOutputFormatClass(TreapBinaryCompressOutputFormat.class);
        } else if (compressBlock) {
            job.setOutputFormatClass(TreapTextCompressOutputFormat.class);
        } else if (binaryFormat) {
            job.setOutputFormatClass(TreapBinaryOutputFormat.class);
        } else {
            job.setOutputFormatClass(TreapTextOutputFormat.class);
        }
        
        job.setMapOutputKeyClass(KeyPair.class);
        job.setMapOutputValueClass(IntWritable.class);
        
        job.setPartitionerClass(TreapPartitioner.class);
        
        job.setMapperClass(TreapMap.class);
        job.setReducerClass(TreapReduce.class);
        
        FileInputFormat.addInputPath(job, new Path(args[0]));
        
        FileSystem fs = FileSystem.get(conf);
        if(fs.exists(new Path(args[1]))){
           /*If exist delete the output path*/
           fs.delete(new Path(args[1]),true);
        }
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
