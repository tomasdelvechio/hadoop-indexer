/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.treap;

import com.tomasdel.tesis.indexer.dg.main.treap.PostingListTreap;
import java.io.IOException;
import lib.org.order.Treap;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class TreapTextRecordWriter extends RecordWriter<Text, PostingListTreap> {
    private FSDataOutputStream indexFileStream;
    private FSDataOutputStream vocabularyFileStream;
    private Treap treap;
    private final String postingRegisterSeparator;
    private final String postingFieldSeparator;
    
    public TreapTextRecordWriter(FSDataOutputStream indexFileOutput, FSDataOutputStream vocabularyFileOutput) {
        this.indexFileStream = indexFileOutput;
        this.vocabularyFileStream = vocabularyFileOutput;
        
        this.postingRegisterSeparator = "\n";
        this.postingFieldSeparator = ";";
    }
    
    @Override
    public void write(Text term, PostingListTreap postingList) throws IOException, InterruptedException {
        this.treap = postingList.getPosting();
        writeVocabulary(term);
        writeIndex(term);
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        indexFileStream.close();
        vocabularyFileStream.close();
    }
    
    private void writeVocabulary(Text term) throws IOException {
        vocabularyFileStream.writeBytes(term.toString());
        vocabularyFileStream.writeBytes(postingFieldSeparator);
        vocabularyFileStream.writeBytes(String.valueOf(treap.size()));
        vocabularyFileStream.writeBytes(postingRegisterSeparator);
    }
    
    private void writeIndex(Text term) throws IOException {
        // Posting Format
        //  <docId list>;<freq list>;<bp structure>\n

        // Print <docId list> section
        treap.printKeys(indexFileStream);
        indexFileStream.writeBytes(postingFieldSeparator);
        
        // Print <freq list> section
        treap.printPriorities(indexFileStream);
        indexFileStream.writeBytes(postingFieldSeparator);
        
        // Print <bp structure> section
        treap.printBPStructure(indexFileStream);
        indexFileStream.writeBytes(postingRegisterSeparator);
    }
}
