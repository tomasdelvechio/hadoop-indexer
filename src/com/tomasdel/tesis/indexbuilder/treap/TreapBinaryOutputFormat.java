/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexbuilder.treap;

import com.tomasdel.tesis.indexer.dg.main.treap.PostingListTreap;
import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class TreapBinaryOutputFormat extends IndexTreapOutputFormat {

    @Override
    public RecordWriter<Text, PostingListTreap> getRecordWriter(TaskAttemptContext tac) throws IOException, InterruptedException {        
        FSDataOutputStream indexFileOutput = buildFileOutput(tac, "index");
        FSDataOutputStream vocabularyFileOutput = buildFileOutput(tac, "vocabulary");
        return new TreapBinaryRecordWriter(indexFileOutput, vocabularyFileOutput);
    }
}
