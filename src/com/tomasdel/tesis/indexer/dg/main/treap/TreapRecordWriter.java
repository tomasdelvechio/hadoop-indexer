/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.dg.main.treap;

import java.io.IOException;
import lib.org.order.Treap;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
public class TreapRecordWriter extends RecordWriter<Text, PostingListTreap> {
    private FSDataOutputStream indexFileStream;
    private FSDataOutputStream vocabularyFileStream;
    private Treap treap;

    public TreapRecordWriter(FSDataOutputStream indexFileOutputStream, FSDataOutputStream vocabularyFileStream) {
        this.indexFileStream = indexFileOutputStream;
        this.vocabularyFileStream = vocabularyFileStream;
    }

    @Override
    public void write(Text term, PostingListTreap postingList) throws IOException, InterruptedException {
        writeVocabulary(term);
        this.treap = postingList.getPosting();
        writeIndex(term);
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        indexFileStream.close();
        vocabularyFileStream.close();
    }
    
    private void writeVocabulary(Text term) throws IOException {
        vocabularyFileStream.writeBytes(term.toString());
        vocabularyFileStream.writeBytes("\n");
    }
    
    private void writeIndex(Text term) throws IOException {
        // Posting Format
        //  <term>;<docId list>;<freq list>;<bp structure>\n
        
        // Print <term> section
        indexFileStream.writeBytes(term.toString());
        indexFileStream.writeBytes(";");
        
        // Print <docId list> section
        treap.printKeys(indexFileStream);
        indexFileStream.writeBytes(";");
        
        // Print <freq list> section
        treap.printPriorities(indexFileStream);
        indexFileStream.writeBytes(";");
        
        // Print <bp structure> section
        treap.printBPStructure(indexFileStream);
        indexFileStream.writeBytes("\n");
    }
    
}
