/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.dg.main.blockmax;

import com.tomasdel.tesis.indexer.commons.PostingList;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
class BlockMaxRecordWriter extends RecordWriter<Text, PostingList> {
    private FSDataOutputStream indexFileStream;
    private FSDataOutputStream vocabularyFileStream;
    private FSDataOutputStream blockMaxIdxFileStream;
    private Map<Integer,Integer> blockMax;
    
    public BlockMaxRecordWriter(FSDataOutputStream indexFileOutput, FSDataOutputStream vocabularyFileOutput, FSDataOutputStream blockMaxIdxFileOutput) {
        this.indexFileStream = indexFileOutput;
        this.vocabularyFileStream = vocabularyFileOutput;
        this.blockMaxIdxFileStream = blockMaxIdxFileOutput;
    }

    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        writeVocabulary(term);
        this.blockMax = postingList.getPosting();
        writeIndex(term);
    }
    
    private void writeVocabulary(Text term) throws IOException {
        vocabularyFileStream.writeBytes(term.toString());
        vocabularyFileStream.writeBytes("\n");
    }
    
    private void writeIndex(Text term) throws IOException {
        // Posting Format
        //  <termId>;<bloque1>;<bloque2>...
        //
        //  <bloque> = <docId,freq>;<docId,freq>...
        
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        indexFileStream.close();
        vocabularyFileStream.close();
        blockMaxIdxFileStream.close();
    }
    
}
