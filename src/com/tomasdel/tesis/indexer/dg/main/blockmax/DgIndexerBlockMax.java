/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.dg.main.blockmax;

import com.tomasdel.tesis.indexer.commons.Indexer;
import com.tomasdel.tesis.indexer.commons.PostingList;
import com.tomasdel.tesis.indexer.dg.main.DgMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.ToolRunner;

/**
 * DgIndexer implements a Indexer build tool with the Dean and Ghemawat paper
 * <p>
 * The MapReduce original article is the base of the Hadoop MapReduce Framework.
 * Dean and Ghemawat present in 2004 the main concepts about MR. In the same
 * article, to be proposed an approach to build Inverted Indexes with MapReduce.
 * <p>
 * The OutputFormat is based on Suel & Ding Block-Max approach.
 * 
 * @version 1
 * @since 1
 * @author tomas
 */
public class DgIndexerBlockMax extends Indexer {
    /**
     * The main method for execute the Index Builder.
     * @param args String[] The parameters passed to application
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int res;
        res = ToolRunner.run(new Configuration(), new DgIndexerBlockMax(), args);
        System.exit(res);
    }
    
    @Override
    public int run(String[] args) throws Exception {
 
        super.run(args);
        
        job.setJarByClass(DgIndexerBlockMax.class);
 
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(PostingList.class);
        
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        
        job.setMapperClass(DgMap.class);
        job.setReducerClass(DgReduceBlockMax.class);
        
        job.setOutputFormatClass(BlockMaxOutputFormat.class);
        
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
