/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.dg.binary;

import com.tomasdel.tesis.indexer.commons.PostingList;
import java.io.IOException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import static org.apache.hadoop.mapreduce.lib.output.FileOutputFormat.getOutputPath;
import static org.apache.hadoop.mapreduce.lib.output.FileOutputFormat.getUniqueFile;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

/**
 *
 * @author tomas
 */
class BinaryIndexOutputFormat extends SequenceFileOutputFormat {
    
    @Override
    public RecordWriter<Text, PostingList> getRecordWriter(TaskAttemptContext tac) throws IOException, InterruptedException {
        FSDataOutputStream postingFileOutputStream = buildFileOutput(tac, "postingFile");
        FSDataOutputStream vocabularyFileOutputStream = buildFileOutput(tac, "vocabulary");
        return new BinaryIndexRecordWriter(postingFileOutputStream, vocabularyFileOutputStream, tac);
    }
    
    private FSDataOutputStream buildFileOutput(TaskAttemptContext tac, String name) throws IOException {
        Path outputDirectoryPath = getOutputPath(tac);
        String filename = getUniqueFile(tac, name, ".txt");
        Path outputFilePath = new Path(outputDirectoryPath, filename);
        FileSystem fs = outputDirectoryPath.getFileSystem(tac.getConfiguration());
        return fs.create(outputFilePath, tac);
    }
}
