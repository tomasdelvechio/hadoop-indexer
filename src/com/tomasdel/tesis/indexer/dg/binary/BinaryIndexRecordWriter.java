/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.dg.binary;

import com.tomasdel.tesis.indexer.commons.Block;
import com.tomasdel.tesis.indexer.commons.PostingList;
import com.tomasdel.tesis.indexer.commons.structs.BlockOutOfBoundsException;
import com.tomasdel.tesis.indexer.commons.structs.PostingFileNotSetException;
import com.tomasdel.tesis.indexer.commons.structs.PostingListBlock;
import static com.tomasdel.tesis.indexer.commons.structs.PostingListBlockBuilder.postingListBlockBuilder;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
public class BinaryIndexRecordWriter extends RecordWriter<Text, PostingList> {
    private final FSDataOutputStream postingFileOutputStream;
    private final FSDataOutputStream vocabularyFileOutputStream;
    private final int N;
    private final int taskId;
    private int localTermCounter;
    public final Integer blockSize;
    private long postingStart;
    private long postingLong;
    private final String saveMode;
    public final boolean compressBlock;
    private final int blocksInMemory;

    public BinaryIndexRecordWriter(FSDataOutputStream postingFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) throws IOException {
        this.postingFileOutputStream = postingFileOutputStream;
        this.vocabularyFileOutputStream = vocabularyFileOutputStream;
        
        // Set parameters to build TermId
        this.N = Integer.parseInt(tac.getConfiguration().get("mapred.reduce.tasks"));
        this.taskId = tac.getTaskAttemptID().getTaskID().getId();
        this.localTermCounter = 0;
        
        this.blockSize = Integer.parseInt(tac.getConfiguration().get("indexer.struct.block.size", "128"));
        this.blocksInMemory = Integer.parseInt(tac.getConfiguration().get("indexer.struct.posting.lenghtInMemory", "2"));
        this.saveMode = tac.getConfiguration().get("indexer.write.mode", "binary");
        this.compressBlock = Boolean.parseBoolean(tac.getConfiguration().get("indexer.struct.block.compress", "true"));
        this.postingStart = 0;
        this.postingLong = 0;
    }

    @Override
    public void write(Text term, PostingList values) throws IOException, InterruptedException {
        this.postingStart = postingFileOutputStream.getPos();
        this.writeVocabulary(term);
        Map<Integer, Integer> postingValues = values.getPosting();
        PostingListBlock postingBlock = postingListBlockBuilder()
                                            .withBlockSize(blockSize)
                                            .withBlocksInMemory(blocksInMemory)
                                            .withCompressPosting(compressBlock)
                                            .withBinaryMode(saveBinaryMode())
                                            .withOutputStream(postingFileOutputStream)
                                            .build();
        for (Map.Entry<Integer, Integer> postingValue : postingValues.entrySet()) {
            try {
                postingBlock.addPosting(postingValue.getKey(), postingValue.getValue());
            } catch (BlockOutOfBoundsException ex) {
                Logger.getLogger(BinaryIndexRecordWriter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PostingFileNotSetException ex) {
                Logger.getLogger(BinaryIndexRecordWriter.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        try {
            // Escribe la ultima posting si es que esta incompleta
            postingBlock.flush();
        } catch (PostingFileNotSetException ex) {
            Logger.getLogger(BinaryIndexRecordWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Calcula la longitud de la posting en bytes
        this.postingLong = postingFileOutputStream.getPos() - this.postingStart;
        
        // Termina de construir el Vocabulario con la posicion de comienzo de la
        //  posting y la longitud de la misma.
        vocabularyFileOutputStream.writeBytes(  ":" + this.postingStart + 
                                                ":" + this.postingLong + 
                                                ":" + this.blockSize +
                                                ":" + postingBlock.getBlockCount().toString() +
                                                '\n');
        
        // Se reinician las variables para contabilizar el siguiente termino
        this.postingStart = 0;
        this.postingLong = 0;
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        this.postingFileOutputStream.close();
        this.vocabularyFileOutputStream.close();
    }
    
    /**
     * Genera un ID unico y global de todo el proceso de indexado.
     * 
     * Idea original: http://stackoverflow.com/a/27868861/3792059
     * @return Integer a global, unique Job id
     */
    private Integer getUniqueTermId() {
        return localTermCounter++*N+taskId;
    }

    private void writeVocabulary(Text term) throws IOException {
        Integer termId = getUniqueTermId();
        vocabularyFileOutputStream.writeBytes(termId.toString());
        vocabularyFileOutputStream.writeBytes(':' + term.toString());
    }

    public void writeBlock(Block block) throws IOException {
        if (this.saveMode.equals("binary")) {
            writeBlockBinary(block);
        } else if (this.saveMode.equals("text")) {
            writeBlockText(block);
        }
    }
    
    public void writeBlockBinary(Block block) throws IOException {
        // Header del bloque: Cantidad de enteros para DocId y Freq,
        //  los cuales pueden o no estar comprimidos y por ende no ser fijos.
        postingFileOutputStream.writeInt(block.docIdLenght());
        postingFileOutputStream.writeInt(block.freqLenght());
        
        // Area de datos: El listado de docIds y freqs del bloque
        for (Integer docId : block.getDocIds()) {
            postingFileOutputStream.writeInt(docId);
        }
        for (Integer freq : block.getFreqs()) {
            postingFileOutputStream.writeInt(freq);
        }
    }
    
    // Version en modo Texto principalmente para proposito de debug
    public void writeBlockText(Block block) throws IOException {
        // Header del bloque: Cantidad de enteros para DocId y Freq,
        //  los cuales pueden o no estar comprimidos y por ende no ser fijos.
        Integer docIdLenght = block.docIdLenght();
        postingFileOutputStream.writeBytes(docIdLenght.toString()+":");
        Integer freqLenght = block.freqLenght();
        postingFileOutputStream.writeBytes(freqLenght.toString()+":");
        
        // Area de datos: El listado de docIds y freqs del bloque
        for (Integer docId : block.getDocIds()) {
            postingFileOutputStream.writeBytes(docId.toString()+",");
        }
        postingFileOutputStream.writeBytes(":");
        for (Integer freq : block.getFreqs()) {
            postingFileOutputStream.writeBytes(freq.toString()+",");
        }
        postingFileOutputStream.writeBytes("\n");
    }
    
    private Boolean saveBinaryMode() {
        return saveMode.equals("binary");
    }
    
}
