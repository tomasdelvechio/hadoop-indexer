/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.dg.binary;

import com.tomasdel.tesis.indexer.commons.Block;
import com.tomasdel.tesis.indexer.commons.PostingList;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

/**
 *
 * @author tomas
 */
public class AlternativeIndexRecordWriter extends RecordWriter<Text, PostingList> {
    private final FSDataOutputStream postingFileOutputStream;
    private final FSDataOutputStream vocabularyFileOutputStream;
    
    public AlternativeIndexRecordWriter(FSDataOutputStream postingFileOutputStream, FSDataOutputStream vocabularyFileOutputStream, TaskAttemptContext tac) {
        this.postingFileOutputStream = postingFileOutputStream;
        this.vocabularyFileOutputStream = vocabularyFileOutputStream;
        
    }
    
    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        Map<Integer, Integer> posting = postingList.getPosting();
        vocabularyFileOutputStream.writeBytes(term.toString());
        for (Iterator<Map.Entry<Integer, Integer>> postingIterator = posting.entrySet().iterator(); postingIterator.hasNext();) {
            Map.Entry<Integer, Integer> postingEntry = postingIterator.next();
            
            postingFileOutputStream.writeInt(postingEntry.getKey());
            postingFileOutputStream.writeInt(postingEntry.getValue());
        }
        
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        this.postingFileOutputStream.close();
        this.vocabularyFileOutputStream.close();
    }
}
