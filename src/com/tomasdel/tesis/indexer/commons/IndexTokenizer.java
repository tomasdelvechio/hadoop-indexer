/*
 * Copyright (C) 2014 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Pattern;

/**
 * This class receive a string of chars and apply the tokenize policy
 * <p>
 * The class is instantiated and could be used many times for tokenize several lines.
 * @author tomas
 */
public class IndexTokenizer implements Enumeration<String>{
    int minLongTerm;
    List<String> tokens;
    private int nextTokenIndex;
    StopWords stopWord;
    Stemmer stemmer;
    private static final Pattern REGEX_PATTERN = Pattern.compile("[^\\p{Alnum}]+");

    public IndexTokenizer(String textToTokenize) throws IOException {
        this.minLongTerm = 3;
        this.stopWord = new StopWords();
        this.stemmer = new Stemmer();
        this.nextTokenIndex = -1;
        this.tokens = new ArrayList<String>();
        this.buildTokens(textToTokenize);
    }
    
    private void buildTokens(String textToTokenize) {
        textToTokenize = textToTokenize.toLowerCase();
        textToTokenize = REGEX_PATTERN.matcher(textToTokenize).replaceAll(" ");
        String[] candidateTokens = textToTokenize.split(" ");
        
        for (String candidateToken : candidateTokens) {
            candidateToken = this.buildToken(candidateToken);
            if(this.isValidToken(candidateToken)) {
                try {
                    this.tokens.add(candidateToken);
                } catch (NullPointerException e) {
                    throw new NullPointerException("Token Nulo: " + candidateToken);
                }
                
            }
        }
    }
    
    /**
     * This method return the next token.
     * @return  term Text The next valid token
     */
    public String nextToken() {
        return this.nextElement();
    }
    
    /**
     * Method for consult if more tokens are present, skipping others not valid tokens.
     * @return boolean return True if some token is present, False in other case
     */
    public boolean hasMoreTokens() {
        return this.hasMoreElements();
    }
    
    /**
     * Check if token is valid for this tokenizer
     * @param token String The token to be evaluated
     * @return boolean The evaluation of several criteria
     */
    public boolean isValidToken(String token) {
        boolean isStopWord = this.stopWord.isStopWord(token);
        boolean isValidLong = minLongTerm <= token.length();
        boolean isNull = (token.getClass() == null); // This is right for compare with null?
        return (!isStopWord && isValidLong && !isNull);
    }
    
    public String buildToken(String candidateToken) {
        candidateToken = this.stemmer.stemmToken(candidateToken.trim());
        return candidateToken;
    }
    
    public int countTokens() {
        return this.tokens.size();
    }

    @Override
    public boolean hasMoreElements() {
        return this.nextTokenIndex < this.countTokens()-1;
    }

    @Override
    public String nextElement() {
        try {
            return this.tokens.get(++this.nextTokenIndex);
        } catch (IndexOutOfBoundsException e) {
            throw new NoSuchElementException("No hay mas tokens para recuperar. " + e);
        }
    }
}
