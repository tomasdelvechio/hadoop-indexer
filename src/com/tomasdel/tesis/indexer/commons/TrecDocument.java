/*
 * Copyright (C) 2014 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons;

import java.io.IOException;

/**
 * Parser of documents in OneLine Trec formatting
 * <p>
 * This class receives a Trec Document without \r nor \n characters. The class
 * will be parser the document, and expose the minimal methods for retrieve the 
 * basic data: Doc Id and Doc content.
 * 
 * @author tomas
 */
public class TrecDocument {
    
    // important Tags
    public static String startDocTag = "<DOC>";
    public static String endDocTag = "</DOC>";
    public static String startDocIdTag = "<DOCNO>";
    public static String endDocIdTag = "</DOCNO>";
    
    public static int minLongDoc = 27; // Longitud minima de un doc
    
    public String docId ;
    public int documentLength ;
    public IndexTokenizer tokenizer;

    /**
     * This getter return the DocId of a Trec Document.
     * <p>
     * This method store the value extracted between \<DOCNO\> and \</DOCNO\> tags.
     * 
     * @return String docId Document Id.
     */
    public Integer getDocId() {
        return Integer.parseInt(this.docId);
    }
    
    /**
     * This getter return the Doc content of a Trec Document.
     * <p>
     * This method store the value extracted between \</DOCNO\> and \</DOC\> tags.
     * 
     * @return String docContent Content of document.
     */
    /*public String getDocumentContent() {
        return this.documentContent;
    }*/
    
    /**
     * Constructor of the class
     * @param trecLine String The Trec Document in one line
     * @throws java.io.IOException
     */
    public TrecDocument(String trecLine) throws IOException {
        this.tokenizer = new IndexTokenizer(trecLine);
    }
    
    /**
     * This method return True if document was parsed successfully and False in other case.
     * 
     * @return boolean 
     */
    public boolean isParsed() {
        return !(this.docId == null);
    }
    
    public Integer lenghtInBytes() {
        return this.documentLength ;
    }
    
    public int countWords() {
        return this.tokenizer.countTokens();
    }
    
}
