/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons.structs;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

/**
 *
 * @author tomas
 */
public class KeyPair implements Writable, WritableComparable<KeyPair> {
    
    private Text term;
    private IntWritable docId;
    
    public KeyPair() {
        this.term = new Text();
        this.docId = new IntWritable();
    }
    
    public KeyPair(String term, Integer docId) {
        this.term = new Text(term);
        this.docId = new IntWritable(docId);
    }

    @Override
    public void write(DataOutput output) throws IOException {
        term.write(output);
        docId.write(output);
    }

    @Override
    public void readFields(DataInput input) throws IOException {
        term.readFields(input);
        docId.readFields(input);
    }

    @Override
    public int compareTo(KeyPair anotherPairedKey) {
        int compareValue = this.term.compareTo(anotherPairedKey.getTerm());
        if (compareValue == 0)
            compareValue = docId.compareTo(anotherPairedKey.getDocId());
        return compareValue;
    }
    
    public Text getTerm() { return this.term; }
    
    public IntWritable getDocId() { return this.docId; }
    
    /**
     *
     * @param anotherKeyPair
     * @return
     */
    @Override
    public boolean equals(Object anotherKeyPair) {
        if (this == anotherKeyPair) return true;
        if (!(anotherKeyPair instanceof KeyPair)) return false;
        KeyPair kp = (KeyPair) anotherKeyPair;
        return  term == kp.getTerm() && docId == kp.getDocId();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.term);
        hash = 17 * hash + Objects.hashCode(this.docId);
        return hash;
    }
    
    
    
}
