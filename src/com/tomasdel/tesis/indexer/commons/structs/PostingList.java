/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons.structs;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author tomas
 */
public class PostingList {
    Map<Integer,Integer> postingList;
    
    public PostingList() {
        this.postingList = new TreeMap<>();
    }
    
    /**
     * Add a posting to the list, assume frequency 1.
     * @param docId Document Id to add in the posting.
     */
    public void addPosting(Integer docId) {
        this.addPosting(docId, 1);
    }
    
    /**
     * Generic method to add a document to the posting list, with arbitrary 
     * frequency.
     * @param documentId Document identifier
     * @param freq Number of occurrences of term in Document
     */
    public void addPosting(Integer documentId, Integer freq) {
        int oldFreq = 0;
        if (this.postingList.containsKey(documentId))
            oldFreq = this.postingList.get(documentId);
        this.postingList.put(documentId, oldFreq+freq);
    }
    
    public int size() {
        return this.postingList.size();
    }
    
    public void reset() {
        this.postingList = new TreeMap<>();
    }
    
    public Set<Map.Entry<Integer, Integer>> getEntrySet() {
        return this.postingList.entrySet();
    }
}
