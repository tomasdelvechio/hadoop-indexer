/*
 * Copyright (C) 2016 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons.structs;

import java.io.IOException;
import java.util.Arrays;
import me.lemire.integercompression.Composition;
import me.lemire.integercompression.FastPFOR;
import me.lemire.integercompression.VariableByte;
import me.lemire.integercompression.IntWrapper;
import me.lemire.integercompression.S16;

/**
 * Genera un bloque de postings
 * 
 * El bloque se almacenara y sera comprimido al momento de ser pedido con el
 * getter correspondiente.
 * 
 * No se permite inyeccion del objeto de compresion. Los DocIds se comprimen
 * con PForDelta y las frecuencias con Simple16.
 * 
 * @author tomas
 */
public class BlockCompress {

    private int blockSize;
    private int[] docIdBlock;
    private int[] frequenceBlock;
    private int cursor;
    private Composition pfdcodec;
    
    public BlockCompress(int blockSize) {
        this.blockSize = blockSize;
        this.docIdBlock = new int[this.blockSize];
        this.frequenceBlock = new int[this.blockSize];
        this.cursor = 0;
        this.pfdcodec = new Composition(new FastPFOR(), new VariableByte());
    }
    
    public BlockCompress() {
        this.blockSize = 128;
        this.docIdBlock = new int[this.blockSize];
        this.frequenceBlock = new int[this.blockSize];
        this.cursor = 0;
        this.pfdcodec = new Composition(new FastPFOR(), new VariableByte());
    }
    
    public void addPosting(Integer docId, Integer freq) throws IOException, BlockOutOfBoundsException {
        try {
            this.addDocId(docId);
            this.addFreq(freq);
            ++cursor;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new BlockOutOfBoundsException("El bloque alcanzo el limite.");
        }    
    }
    
    private void addDocId(Integer docId) {
        this.docIdBlock[cursor] = docId;
    }
    
    private void addFreq(Integer freq) {
        this.frequenceBlock[cursor] = freq;
    }
    
    public int[] getDocIds() {
        int[] compressDocIdBlock = new int[blockSize+1];
        IntWrapper outpos = new IntWrapper(0);
        pfdcodec.compress(  docIdBlock, 
                            new IntWrapper(0), 
                            cursor, 
                            compressDocIdBlock, 
                            outpos);
        return Arrays.copyOfRange(compressDocIdBlock, 0, outpos.get());
    }
    
    public int[] getFreqs() {
        int[] frequenceEffectiveBlock = Arrays.copyOfRange(frequenceBlock, 0, cursor);
        int compressFreqBloqLenght = S16.estimatecompress(frequenceEffectiveBlock, 0, frequenceEffectiveBlock.length);
        int[] compressFreqBloq = new int[compressFreqBloqLenght];
        S16.compress(   frequenceEffectiveBlock,
                        0,
                        frequenceEffectiveBlock.length,
                        compressFreqBloq,
                        0);
        return Arrays.copyOfRange(compressFreqBloq, 0, compressFreqBloqLenght);
    }
    
    public int getMaxFreq() {
        int actualMax = 0;
        for (int freqElement = 0; freqElement < frequenceBlock.length; freqElement++) {
            if (actualMax < frequenceBlock[freqElement])
                actualMax = frequenceBlock[freqElement];
        }
        return actualMax;
    }

    public int getMaxDocId() {
        int actualMax = 0;
        for (int docIdElement = 0; docIdElement < docIdBlock.length; docIdElement++) {
            if (actualMax < docIdBlock[docIdElement])
                actualMax = docIdBlock[docIdElement];
        }
        return actualMax;
    }
    
    public int size() {
        return this.cursor;
    }
    
    public void clear() {
        this.docIdBlock = new int[this.blockSize];
        this.frequenceBlock = new int[this.blockSize];
        this.cursor = 0;
    }

}
