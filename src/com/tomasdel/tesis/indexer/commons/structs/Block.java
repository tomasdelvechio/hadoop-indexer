/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons.structs;

import com.kamikaze.pfordelta.PForDelta;
import java.io.IOException;
import java.util.Arrays;
import org.apache.hadoop.fs.FSDataOutputStream;

/**
 * Un bloque es una sub-lista de pares docId,freq que integra una PostingList.
 * 
 * Existen diversas ventajas en no manejar una PostingList como una lista de 
 * todos los pares docId,freq que la componen, sino subdividirla a su vez en 
 * bloques. Permite ir aplicando de forma incremental operaciones de compresion,
 * busqueda, indexacion secundaria (Como en Block-Max, p.e.).
 * 
 * Esta clase esta muy acoplada a la logica de almacenamiento en HDFS.
 * 
 * @author tomas
 */
public class Block {
    private int[] docIdBlock;
    private final int[] frequenceBlock;
    private int cursor;
    private final Boolean compress;
    private Boolean binaryMode;
    
    /**
     * Construye un Bloque.
     * 
     * @param blockSize Indica la cantidad de pares docId,freq que se pueden 
     * cargar en el bloque
     * @param compressBlock Determina si el bloque debe ser comprimido antes de
     * ser almacenado
     * @param binaryMode Indica si el modo de almacenamiento en disco debe ser
     * binario (produccion) o texto (debug)
     */
    public Block(Integer blockSize, Boolean compressBlock, Boolean binaryMode) {
        this.docIdBlock = new int[blockSize];
        this.frequenceBlock = new int[blockSize];
        this.cursor = 0;
        this.compress = compressBlock;
        this.binaryMode = binaryMode;
    }
    
    /**
     * Construye un Bloque.
     * 
     * Asume que debe almacenarse en modo binario.
     * 
     * @param blockSize Indica la cantidad de pares docId,freq que se pueden 
     * cargar en el bloque
     * @param compressBlock Determina si el bloque debe ser comprimido antes de
     * ser almacenado
     */
    public Block(Integer blockSize, Boolean compressBlock) {
        this.docIdBlock = new int[blockSize];
        this.frequenceBlock = new int[blockSize];
        this.cursor = 0;
        this.compress = compressBlock;
        this.binaryMode = true;
    }
    
    /**
     * Construye un Bloque.
     * 
     * Asume que debe ser comprimido y debe almacenarse en modo binario.
     * 
     * @param blockSize Indica la cantidad de pares docId,freq que se pueden 
     * cargar en el bloque
     */
    public Block(Integer blockSize) {
        this.docIdBlock = new int[blockSize];
        this.frequenceBlock = new int[blockSize];
        this.cursor = 0;
        this.compress = true;
        this.binaryMode = true;
    }
    
    /**
     * Construye un Bloque.
     * 
     * Asume que el tamaño sera 128 pares, que debe ser comprimido y debe 
     * almacenarse en modo binario.
     */
    public Block() {
        this.docIdBlock = new int[128];
        this.frequenceBlock = new int[128];
        this.cursor = 0;
        this.compress = true;
        this.binaryMode = true;
    }
    
    /**
     * Determina si un bloque alcanzo el limite maximo (si el blockSize fue 
     * alcanzado).
     * 
     * @return true si el bloque alcanzo limite. false en caso contrario.
     */
    public Boolean isFull() {
        return this.cursor == this.docIdBlock.length;
    }
    
    public void addPosting(Integer docId, Integer freq) throws IOException, BlockOutOfBoundsException {
        if (isFull())
            throw new BlockOutOfBoundsException("El bloque alcanzo el limite. Error.");
        this.addDocId(docId);
        this.addFreq(freq);
        ++cursor;
    }
    
    // Agrega el docId calculando el delta gap
    private void addDocId(Integer docId) {
        this.docIdBlock[cursor] = docId - docIdAnterior();
    }
    
    private void addFreq(Integer freq) {
        this.frequenceBlock[cursor] = freq;
    }
    
    private Integer docIdAnterior() {
        if(cursor==0)
            return 0;
        else
            return this.docIdBlock[cursor-1];
    }
    
    public void write(FSDataOutputStream postingFile) throws IOException {
        if(compress)
            compressBlock();
        writeHeader(postingFile);
        writeData(postingFile);
    }
    
    public void writeHeader(FSDataOutputStream postingFile) throws IOException {
        if(binaryMode) {
            writeHeaderBinary(postingFile);
        } else {
            writeHeaderText(postingFile);
        }
    }
    
    public void writeHeaderBinary(FSDataOutputStream postingFile) throws IOException {
        postingFile.writeInt(getDocIdBlockLenght());
        postingFile.writeInt(getFreqBlockLenght());
    }
    
    public void writeHeaderText(FSDataOutputStream postingFile) throws IOException {
        postingFile.writeBytes(getDocIdBlockLenght().toString());
        postingFile.writeBytes(getFreqBlockLenght().toString());
    }
    
    public void writeData(FSDataOutputStream postingFile) throws IOException {
        if(binaryMode) {
            writeDataBinary(postingFile);
        } else {
            writeDataText(postingFile);
        }
    }
    
    private void writeDataBinary(FSDataOutputStream postingFile) throws IOException {
        for (Integer docId : docIdBlock) {
            postingFile.writeInt(docId);
        }
        for (Integer freq : frequenceBlock) {
            postingFile.writeInt(freq);
        }
    }
    
    private void writeDataText(FSDataOutputStream postingFile) throws IOException {
        for (Integer docId : docIdBlock) {
            postingFile.writeBytes(docId.toString());
        }
        for (Integer freq : frequenceBlock) {
            postingFile.writeBytes(freq.toString());
        }
    }
    
    private void compressBlock() {
        docIdBlock = PForDelta.compressOneBlockOpt(docIdBlock, cursor);
    }
    
    private Integer getDocIdBlockLenght() {
        if(compress) {
            return docIdBlock.length;
        } else {
            return cursor;
        }
    }
    
    private Integer getFreqBlockLenght() {
        return cursor;
    }

    public int[] getDocIds() {
        return Arrays.copyOfRange(docIdBlock, 0 ,cursor);
    }

    public int[] getFreqs() {
        return Arrays.copyOfRange(frequenceBlock, 0 ,cursor);
    }

    public int getMaxFreq() {
        int actualMax = 0;
        for (int freqElement = 0; freqElement < frequenceBlock.length; freqElement++) {
            if (actualMax < frequenceBlock[freqElement])
                actualMax = frequenceBlock[freqElement];
        }
        return actualMax;
    }

    public int getMaxDocId() {
        int actualMax = 0;
        for (int docIdElement = 0; docIdElement < docIdBlock.length; docIdElement++) {
            if (actualMax < docIdBlock[docIdElement])
                actualMax = docIdBlock[docIdElement];
        }
        return actualMax;
    }
}
