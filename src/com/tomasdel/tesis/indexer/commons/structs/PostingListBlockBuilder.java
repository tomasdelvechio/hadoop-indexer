/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons.structs;

import org.apache.hadoop.fs.FSDataOutputStream;

/**
 * Builder para la estructura PostingList con Bloques.
 * 
 * Utiliza el patron fluent interface para el manejo de parametros
 * (Ver http://stackoverflow.com/a/40324/3792059)
 * 
 * @author tomas
 */
public class PostingListBlockBuilder {
    public Integer blockSize;
    public Integer blocksInMemory;
    public Boolean compressPosting;
    public Boolean binaryMode;
    public FSDataOutputStream postingFile;
    
    public static PostingListBlockBuilder postingListBlockBuilder() {
        return new PostingListBlockBuilder();
    }
    
    public PostingListBlockBuilder withBlockSize(Integer aBlockSize) {
        this.blockSize = aBlockSize;
        return this;
    }
    
    public PostingListBlockBuilder withBlocksInMemory(Integer aBlocksInMemory) {
        this.blocksInMemory = aBlocksInMemory;
        return this;
    }
    
    public PostingListBlockBuilder withCompressPosting(Boolean compressPosting) {
        this.compressPosting = compressPosting;
        return this;
    }
    
    public PostingListBlockBuilder withBinaryMode(Boolean binaryMode) {
        this.binaryMode = binaryMode;
        return this;
    }
    
    public PostingListBlockBuilder withOutputStream(FSDataOutputStream postingFile) {
        this.postingFile = postingFile;
        return this;
    }
    
    public PostingListBlock build() {
        return new PostingListBlock(this);
    }
}
