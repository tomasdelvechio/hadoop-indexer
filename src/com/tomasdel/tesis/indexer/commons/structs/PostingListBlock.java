/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons.structs;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.hadoop.fs.FSDataOutputStream;

/**
 * Una posting list es una lista de valores asociados a un termino.
 * 
 * Esta posting list se divide internamente en bloques.
 * 
 * Los valores en cuestion son de interes para el proceso de recuperacion de 
 * informacion.
 * 
 * @author tomas
 */
public class PostingListBlock {
    private ArrayList<Block> blocks;
    private Block currentBlock;
    private Integer blockSize;
    private Boolean compressPosting;
    private Boolean binaryMode;
    private FSDataOutputStream postingFile;
    private Integer blocksInMemory;
    private Integer blockCount;
    
    /**
     * Constructor de Posting Lists con bloques.
     * 
     * Se construye via un Builder especifico que se encarga del manejo de 
     * parametros. Esto permite escalar la cantidad de parametros a ser 
     * seteados en este objeto.
     * @param builder a PostingListBlockBuilder instance
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public PostingListBlock(PostingListBlockBuilder builder) {
        // Check parameters
        if(builder.blockSize == null) 
            throw new NullPointerException("No se ha definido blockSize en el builder");
        if(builder.compressPosting == null) 
            throw new NullPointerException("No se ha definido compressPosting en el builder");
        if(builder.binaryMode == null) 
            throw new NullPointerException("No se ha definido binaryMode en el builder");
        if(builder.postingFile == null)
            throw new NullPointerException("No se ha definido postingFile en el builder");
        if(builder.blocksInMemory == null)
            throw new NullPointerException("No se ha definido blocksInMemory en el builder");
        
        // Set parameters
        this.blockSize = builder.blockSize;
        this.compressPosting = builder.compressPosting;
        this.binaryMode = builder.binaryMode;
        this.postingFile = builder.postingFile;
        this.blocksInMemory = builder.blocksInMemory;
        
        // Set vars
        this.blockCount = 0;
        
        // Create empty posting list
        this.createEmptyPostingList(this);
        
    }
    
    private void createEmptyPostingList(PostingListBlock aPostingList) {
        aPostingList.blocks = new ArrayList<Block>();
        addNewBlock(aPostingList);
    }
    
    public void addPosting(Integer docId, Integer freq) throws IOException, BlockOutOfBoundsException, PostingFileNotSetException {
        try {
            currentBlock.addPosting(docId, freq);
        } catch (BlockOutOfBoundsException e) {
            if(fullInMemoryBlocks())
                flush();
            currentBlock.addPosting(docId, freq);
        }
    }
    
    private Boolean fullInMemoryBlocks() {
        return blocks.size() >= blocksInMemory;
    }
    
    private void addNewBlock() {
        Block block = new Block(blockSize, compressPosting, binaryMode);
        this.blocks.add(block);
        this.currentBlock = block;
        this.blockCount++;
    }
    
    private void addNewBlock(PostingListBlock aPostingList) {
        Block block = new Block(blockSize, compressPosting, binaryMode);
        aPostingList.blocks.add(block);
        aPostingList.currentBlock = block;
        this.blockCount++;
    }
    
    // Write header of posting and send message to a block for write block
    private void write() throws IOException, PostingFileNotSetException {
        for (Block block : blocks ) {
            block.write(postingFile);
        }
    }
    
    public Integer getBlockCount() {
        return this.blockCount;
    }
    
    public void flush() throws IOException, PostingFileNotSetException {
        write();
        createEmptyPostingList(this);
    }
}
