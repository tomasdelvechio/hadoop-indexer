/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons;

import com.tomasdel.tesis.indexer.dg.binary.BinaryIndexRecordWriter;
import java.io.IOException;
import com.kamikaze.pfordelta.PForDelta;
import java.util.Arrays;

/**
 *
 * @author tomas
 */
public class Block {
    private int[] docIdBlock;
    private int[] frequenceBlock;
    private final BinaryIndexRecordWriter rw;
    private int docIdBlockLastIndex;
    private int frequenceBlockLastIndex;
    private boolean blockCompressed;
    
    public Block(BinaryIndexRecordWriter rw) {
        this.docIdBlock = new int[rw.blockSize];
        this.frequenceBlock = new int[rw.blockSize];
        docIdBlockLastIndex = -1;
        frequenceBlockLastIndex = -1;
        blockCompressed = false;
        this.rw = rw;
        
    }

    public Block() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void addPosting(Integer docId, Integer freq) throws IOException {
        this.writeBlockIfFull();
        if (this.docIdBlockLastIndex >= 0) {
            // Compute D-Gap
            int docIdAnt = this.docIdBlock[docIdBlockLastIndex];
            this.docIdBlock[++docIdBlockLastIndex] = docId - docIdAnt;
        } else {
            this.docIdBlock[++docIdBlockLastIndex] = docId;
        }
        this.frequenceBlock[++frequenceBlockLastIndex] = freq;
        
    }
    
    private void buildEmptyBlock() {
        this.docIdBlock = new int[rw.blockSize];
        this.frequenceBlock = new int[rw.blockSize];
        docIdBlockLastIndex = -1;
        frequenceBlockLastIndex = -1;
        blockCompressed = false;
    }
    
    private void writeBlockIfFull() throws IOException {
        if(isFull()) {
            this.comprimir();
            this.write();
            this.buildEmptyBlock();
        }
    }
    
    private boolean isFull() {
        return this.docIdBlockLastIndex == rw.blockSize - 1;
    }
    
    private void comprimir() {
        blockCompressed = true;
        if(rw.compressBlock)
            this.docIdBlock = PForDelta.compressOneBlockOpt(this.docIdBlock, this.docIdBlockLastIndex);
        /*Simple9 simple9Compressor = new Simple9();
        simple9Compressor.compress(docIdBlock, null, docIdBlockLastIndex, docIdBlock, null);*/
    }
    
    private void write() throws IOException {
        rw.writeBlock(this);
    }

    public int[] getDocIds() {
        return this.docIdBlock;
    }

    public int[] getFreqs() {
        return Arrays.copyOfRange(this.frequenceBlock, 0, frequenceBlockLastIndex);
    }

    public void flush() throws IOException {
        if (!isEmpty()) {
            this.comprimir();
            this.write();
        }
    }
    
    public boolean isEmpty() {
        return docIdBlockLastIndex == 0;
    }
    
    public int docIdLenght() {
        if (blockCompressed) {
            return this.docIdBlock.length;
        } else {
            return this.docIdBlockLastIndex;
        }
    }
    
    public int freqLenght() {
        return this.frequenceBlockLastIndex;
    }
}
