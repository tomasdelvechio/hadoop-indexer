/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import com.tomasdel.tesis.indexer.commons.structs.PostingList;


/**
 * Especifica un RecordWriter en modo Texto para un indice invertido
 * 
 * IndexRecordWriter ofrece una salida que construye un indice invertido con 
 * las siguientes caracteristicas:
 * 
 *  - Archivo de postings con compresion delta-gap para documents ID
 *  - Archivo de frecuencias sin compresion
 *  - Archivo de vocabulario con formato <term>:<termId>
 * 
 * El <termId> es global y unico para cada termino.
 * 
 * @author tomas
 */
public class IndexRecordWriter extends RecordWriter<Text, PostingList> {
    private DataOutputStream postingOutput;
    private DataOutputStream vocabularyOutput;
    private DataOutputStream freqOutput;
    private Integer N;
    private Integer taskId;
    private Integer localTermCounter;
    private Integer termId;

    public IndexRecordWriter(DataOutputStream postingFile, DataOutputStream freqFile, DataOutputStream vocabulary, TaskAttemptContext tac) {
        this.postingOutput = postingFile;
        this.freqOutput = freqFile;
        this.vocabularyOutput = vocabulary;
        
        // Set parameters to build TermId
        this.N = Integer.parseInt(tac.getConfiguration().get("mapred.reduce.tasks"));
        this.taskId = tac.getTaskAttemptID().getTaskID().getId();
        this.localTermCounter = 0;
    }

    /**
     * Implement a basic sequence of tasks to write the whole index
     * @param term A term to be write
     * @param postingList A list with doc id and frequence by term
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void write(Text term, PostingList postingList) throws IOException, InterruptedException {
        writeVocabulary(term);
        writePosting(postingList);
    }
    
    /**
     * Write a text-based, docId compressed, persistent posting list
     * 
     * Posting format: Two files. DocId file and Frequence file.
     * 
     * DocId File
     * 
     *  <TermId>:<docId1>,<docId2>,<docId3>\n
     * 
     * Where:
     *  <TermId> is a unique id build by writeVocabulary method.
     *  <docIdN> are a pointer to collection's document. That list is compressed 
     *           by delta-gap encoding without blocks.
     * @param postingList A list with doc id and frequence by term
     * @throws IOException
     */
    private void writePosting(PostingList postingList) throws IOException {
        Integer docIdAnt = 0;
        Integer docActual ;
        String delim = "";
        
        postingOutput.writeChars(this.termId + ":");
        freqOutput.writeChars(this.termId + ":");
        
        for (Map.Entry<Integer, Integer> postingEntry : postingList.getEntrySet()) {
            docActual = postingEntry.getKey();
            Integer freqActual = postingEntry.getValue();
            postingOutput.writeChars(delim + (docActual - docIdAnt));
            freqOutput.writeChars(delim + freqActual);
            docIdAnt = docActual;
            delim = ","; // After first iteration, delimitation is inserted. Last record don't set the delim.
        }
        
        postingOutput.writeChars("\n");  
        freqOutput.writeChars("\n");  
    }

    @Override
    public void close(TaskAttemptContext tac) throws IOException, InterruptedException {
        postingOutput.close();
        freqOutput.close();
        vocabularyOutput.close();
    }

    private void writeVocabulary(Text k) throws IOException {
        this.termId = getUniqueTermId();
        vocabularyOutput.writeBytes(k.toString() + ":" + this.termId.toString() + "\n");
    }
    
    /**
     * Genera un ID unico y global de todo el proceso de indexado.
     * 
     * Idea original: http://stackoverflow.com/a/27868861/3792059
     * @return Integer a global, unique Job id
     */
    private Integer getUniqueTermId() {
        return localTermCounter++*N+taskId;
    }
}
