/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.commons;

import java.io.IOException;

/**
 *
 * @author tomas
 */
public class TrecOneLineDocument extends TrecDocument {

    public TrecOneLineDocument(String trecLine) throws IOException {
        super(trecLine);
        
        if (trecLine.length() >= TrecOneLineDocument.minLongDoc) {
            
            // Obtener DocId
            int startDocIdIdx = trecLine.indexOf(TrecOneLineDocument.startDocIdTag) + TrecOneLineDocument.startDocIdTag.length();
            int endDocIdIdx = trecLine.indexOf(TrecOneLineDocument.endDocIdTag);
            
            try {
                this.docId = trecLine.substring(startDocIdIdx, endDocIdIdx).trim();
            } catch (StringIndexOutOfBoundsException e) {
                throw new StringIndexOutOfBoundsException("Trec Line: " + trecLine);
            }
            
            // Recuperar contenido del Documento
            int startDocContentIdx = trecLine.indexOf(TrecOneLineDocument.endDocIdTag) + TrecOneLineDocument.endDocIdTag.length();
            int endDocContentIdx = trecLine.indexOf(TrecOneLineDocument.endDocTag);
            
            this.documentLength = trecLine.substring(startDocContentIdx, endDocContentIdx).length();
        } else {
            this.docId = null;
            this.documentLength = 0;
        }
    }
    
}
