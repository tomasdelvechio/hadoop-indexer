/*
 * Copyright (C) 2014 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.optimized;

import com.tomasdel.tesis.indexer.commons.CastingTypes;
import com.tomasdel.tesis.indexer.commons.InvertedIndex;
import com.tomasdel.tesis.indexer.commons.PostingList;
import com.tomasdel.tesis.indexer.commons.TrecOneLineDocument;
import java.io.IOException;
import java.util.Map;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Mapper class for the D&G approach
 * @author tomas
 */
public class OptimizedMap extends Mapper<LongWritable, Text, Text, Text> {
    private InvertedIndex localIndex;
    
    @Override
    public void setup(Mapper.Context context) {
        this.localIndex = new InvertedIndex();
    }
    
    @Override
    public void map(LongWritable key, Text value, Mapper.Context context) throws IOException, InterruptedException {
        TrecOneLineDocument document = new TrecOneLineDocument(value.toString());
        if (document.isParsed()) {
            while (document.tokenizer.hasMoreTokens()) {
                String term = document.tokenizer.nextToken();
                if (term != null) {
                    this.localIndex.addPosting(new Text(term), document.getDocId(), CastingTypes.one);
                }
            }
        }
    }
    
    @Override
    public void cleanup(Mapper<LongWritable, Text, Text, Text>.Context context) throws IOException, InterruptedException {
        for (Text term : this.localIndex.getVocabulary()) {
            PostingList posting = this.localIndex.getPosting(term);
            //System.out.println("DEBUGINDEXER: Termino: " + term + "; Posting: " + posting);
            for (Map.Entry<Integer, Integer> entry : posting.getEntrySet()) {
                String docId = entry.getKey().toString();
                String freq = entry.getValue().toString();
                Text localPosting = new Text(docId + ":" + freq);
                context.write(term, localPosting);
            }
            
        }
    }
}
