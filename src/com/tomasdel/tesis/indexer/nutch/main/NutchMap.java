/*
 * Copyright (C) 2014 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.indexer.nutch.main;

import com.tomasdel.tesis.indexer.commons.CastingTypes;
import com.tomasdel.tesis.indexer.commons.TrecOneLineDocument;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Mapper class for the Nutch approach
 * @author tomas
 */
public class NutchMap extends Mapper<LongWritable, Text, IntWritable, MapWritable> {
    MapWritable documentAnalyzed;
    
    @Override
    public void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, MapWritable>.Context context) throws IOException, InterruptedException {
        TrecOneLineDocument document = new TrecOneLineDocument(value.toString());
        documentAnalyzed = new MapWritable();
        if (document.isParsed()) {
            //this.tokenizer.tokenize(document.getDocumentContent());
            //this.tokenizer = new IndexTokenizer(document.getDocumentContent());
            while (document.tokenizer.hasMoreTokens()) {
                IntWritable counter = CastingTypes.zero;
                String newTerm = document.tokenizer.nextToken();
                Text term = new Text(newTerm);
                if (documentAnalyzed.containsKey(term)) {
                    counter = CastingTypes.strToIntWr(documentAnalyzed.get(term).toString());
                }
                documentAnalyzed.put(term, CastingTypes.intToIntWr(counter.get()+1));
            }
            if ( ! documentAnalyzed.isEmpty()) {
                context.write(new IntWritable(document.getDocId()), documentAnalyzed);
            }
        }
    }
}

