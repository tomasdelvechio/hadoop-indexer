/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.coleccion;

import com.tomasdel.tesis.indexer.commons.TrecOneLineDocument;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author tomas
 */
class LongDocumentTestMap extends Mapper<LongWritable, Text, IntWritable, Text> {
    int wordsInDocument;
    int bytesInDocument;
    
    public LongDocumentTestMap() throws IOException {
        this.wordsInDocument = 0;
        this.bytesInDocument = 0;
        
    }
    
    @Override
    public void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, Text>.Context context) throws IOException, InterruptedException {
        TrecOneLineDocument document = new TrecOneLineDocument(value.toString());
        if (document.isParsed()) {
            this.bytesInDocument = document.lenghtInBytes();
            this.wordsInDocument = document.countWords();
            String outputValue;
            outputValue =   Integer.toString(this.bytesInDocument) + "\t" + 
                            Integer.toString(this.wordsInDocument);
            context.write(new IntWritable(document.getDocId()), new Text(outputValue));
        }
    }
    
}
