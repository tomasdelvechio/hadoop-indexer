/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.coleccion;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * Aplicacion MapReduce que genera un documento de resumen de una coleccion Trec
 * <p>
 * La App recibe un documento subido a HDFS en formato Trec One Line. Genera una
 * salida que consiste en:
 * 
 *  <DocId>\t<LongitudDocumentoBytes>\t<LongitudDocumentoPalabras>\n
 * 
 * Hay una linea por documento. "Palabra" se define como un token valido para la 
 * clase com.tomasdel.tesis.indexer.commons.IndexTokenizer.
 * 
 * @author tomas
 */
class LongDocumentTestReduce extends Reducer<IntWritable, Text, Text, Text> {
    
    @Override
    public void reduce(IntWritable docId, Iterable<Text> Stats, Context context)
      throws IOException, InterruptedException {
        Text val = null;
        for (Text stat : Stats) {
            val = stat;
        }
        context.write(new Text(docId.toString()), new Text(val));
    }
}
