/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.tesis.examples.binary;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.ByteWritable;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author tomas
 */
public class ReduceBinary extends Reducer<Text, IntWritable, BytesWritable, BytesWritable> {
        
    @Override
    public void reduce(Text key, Iterable<IntWritable> values, Context context)
      throws IOException, InterruptedException {
        byte[] data = new byte[2] ;
        byte sum = 0;
        byte count = 0;
        for (IntWritable val : values) {
            sum += val.get();
            count++;
        }
        data[0] = sum;
        data[1] = count;
        BytesWritable dataWritable = new BytesWritable();
        dataWritable.set(data, dataWritable.getLength(), data.length);
        
        context.write(dataWritable, new BytesWritable());
    }
}
