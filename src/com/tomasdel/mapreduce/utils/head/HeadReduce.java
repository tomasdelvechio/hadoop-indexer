/*
 * Copyright (C) 2015 tomas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.tomasdel.mapreduce.utils.head;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author tomas
 */
class HeadReduce extends Reducer<Text, NullWritable, Text, NullWritable> {
    
    public void reduce(Text line, NullWritable nullData, Context context)
      throws IOException, InterruptedException {
        
        context.write(new Text(line), nullData);
    }
    
    @Override
    public void run(Reducer<Text, NullWritable, Text, NullWritable>.Context context) throws IOException, InterruptedException {
        setup(context);
        
        Configuration conf = context.getConfiguration();
        String numberOfLinesStr = conf.get("NumberOfLines");
        
        int numberOfLines = Integer.parseInt(numberOfLinesStr);
        
        int rows = 0;
        while(context.nextKeyValue())
        {
            if(rows++ >= numberOfLines)
            {
                break;
            }
            reduce(new Text(context.getCurrentKey()), NullWritable.get(), context);
        }
        
        cleanup(context);
    }
}
