package lib.org.order;

import com.tomasdel.tesis.indexer.commons.structs.BlockCompress;
import com.tomasdel.tesis.indexer.commons.structs.BlockOutOfBoundsException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Stack;
import org.apache.hadoop.fs.FSDataOutputStream;

/**
 * An implentation of an ordered dictionary data structure, based
 * on the randomized search trees (treaps) by Seidel and Aragon.
 * <blockquote>
 * R. Seidel and C. R. Aragon. Randomized Binary Search Trees.
 * <em>Algorithmica</em>, 16(4/5):464-497, 1996.
 * </blockquote>
 *
 * This class implements an ordered dictionary that maps keys to values.
 * Any non-null object can be used as a value, but the keys must be
 * non-null objects that implement the <code>Ordered</code> interface.
 * The methods are similar to the ones in <code>java.util.Hashtable</code>.
 * In addition, efficient methods for finding the minimum and maximum
 * keys and their values are included. The enumeration returns the keys
 * in sorted order. <p>
 * 
 * Most methods run in <math>O(log n)</math> randomized time, where
 * <math>n</math> is the number of keys in the treap; the exceptions
 * are <code>clone()</code> and <code>toString()</code> that run in time
 * proportional to the size of their output.
 *
 * @author Stefan.Nilsson@hut.fi
 * @see Ordered
 * @version 1.0, 22 April 1997
 */
public class Treap implements Cloneable
{
    private Tree tree;
    private int size;

    /* If during an <code>insert()</code> or <code>delete()</code> it is
     * found that the key is present in the tree, <code>keyFound</code>
     * will be <code>true</code> and <code>prevValue</code> will contain
     * the previous value assosiacted with the key before the update.
     */
    private boolean keyFound;
    private Integer prevValue;
    
    /* Size of structure list, representation of Balanced parenthesis in
        list of integers. */
    public static int lenghtBPnumbers = 0;

    /**
     * Constructs a new empty treap.
     */
    public Treap() {
        tree = null;
        size = 0;
    }

    /**
     * Maps the key to the specified value in this treap.
     * Neither the key, nor the value can be <code>null</code>.
     * @param key
     * @param value
     * @return the previous value to which the key was mapped,
     * or <code>null</code> if the key did not have a previous
     * mapping in the treap.
     */
    public synchronized Object put(Ordered key, int value) {
        if (key == null)
            throw new NullPointerException();

        Tree node = new Tree();
        node.key = key;
        node.value = value;
        node.prio = value;
        keyFound = false;
        tree = insert(node, tree);
        if (keyFound)
            return prevValue;
        else {
            size++;
            return null;
        }
    }

    /**
     * Gets the object associated with the specified key in the treap.
     * @param key
     * @return the value to which the key is mapped in the treap, or
     * <code>null</code> if the key is not mapped to any value in
     * this treap.
     */
    public synchronized Integer get(Ordered key) {
        Tree t = tree;
        int comp;

        while (t != null && (comp = key.compareTo(t.key)) != 0)
            if (comp < 0)
                t = t.left;
            else
                t = t.right;
        return t != null ? t.value : null;
    }
    
    /**
     * Gets the priority associated with the specified key in the treap.
     * @param key
     * @return the prio to which the key is mapped in the treap, or
     * <code>null</code> if the key is not mapped to any value in
     * this treap.
     */
    public synchronized Object getPriority(Ordered key) {
        Tree t = tree;
        int comp;

        while (t != null && (comp = key.compareTo(t.key)) != 0)
            if (comp < 0)
                t = t.left;
            else
                t = t.right;
        return t != null ? t.prio : null;
    }

    /**
     * Returns the minimum key of the treap.
     * @return 
     * @exception NoSuchElementException if the treap is empty.
     */
    public synchronized Ordered getMinKey() {
        Tree t = tree;

        if (t == null)
            throw new NoSuchElementException("Treap");
        while (t.left != null)
            t = t.left;
        return t.key;
    }

    /**
     * Returns the maximum key of the treap.
      * @return 
     * @exception NoSuchElementException if the treap is empty.
     */
    public synchronized Ordered getMaxKey() {
        Tree t = tree;

        if (t == null)
            throw new NoSuchElementException("Treap");
        while (t.right != null)
            t = t.right;
        return t.key;
    }

    /**
     * Returns the value to which the minimum key of the treap is mapped.
      * @return 
     * @exception NoSuchElementException if the treap is empty.
     */
    public synchronized Object getMinValue() {
        Tree t = tree;

        if (t == null)
            throw new NoSuchElementException("Treap");
        while (t.left != null)
            t = t.left;
        return t.value;
    }

    /**
     * Returns the value to which the maximum key of the treap is mapped.
     * @return 
     * @exception NoSuchElementException if the treap is empty.
     */
    public synchronized Object getMaxValue() {
        Tree t = tree;

        if (t == null)
            throw new NoSuchElementException("Treap");
        while (t.right != null)
            t = t.right;
        return t.value;
    }

    /**
     * Returns an ordered enumeration of the keys in this treap.
     * The enumeration will list the keys in ascending order.
     * @return 
     * @see Enumeration
     */
    public synchronized Enumeration keys() {
        return new TreapEnumeration(tree, true, true);
    }

    /**
     * Returns an ordered enumeration of the keys in this treap.
     * The enumeration will list the keys in ascending order
     * if <code>ascending</code> is <code>true</code>, otherwise the
     * keys will be listed in descending order.
     * @param ascending
     * @return 
     * @see Enumeration
     */
    public synchronized Enumeration keys(boolean ascending) {
        return new TreapEnumeration(tree, true, ascending);
    }

    /**
     * Returns an enumeration of the elemets in this treap.
     * The enumeration will list the elements in ascending order
     * (according to the corresponding keys).
     * @return 
     * @see Enumeration
     */
    public synchronized Enumeration elements() {
        return new TreapEnumeration(tree, false, true);
    }

    /**
     * Returns an enumeration of the elemets in this treap.
     * The enumeration will list the elements in ascending order
     * (according to the corresponding keys) if <code>ascending</code>
     * is <code>true</code>, otherwise the elements will be listed
     * in descending order.
     * @param ascending
     * @return 
     * @see Enumeration
     */
    public synchronized Enumeration elements(boolean ascending) {
        return new TreapEnumeration(tree, false, ascending);
    }

    /**
     * Returns <code>true</code> if this treap contains no mappings.
     * @return 
     */
    public boolean isEmpty() {
        return tree == null;
    }

    /**
     * Removes tke key (and its corresponding value) from this treap.
     * This method does nothing if the key is not in the treap.
     * @param key
     * @return the value to which the key had been mapped in this treap,
     * or <code>null</code> if the key did not have a mapping.
     */
    public synchronized Object remove(Ordered key) {
        keyFound = false;
        tree = delete(key, tree);
        if (keyFound) {
            size--;
            return prevValue;
        } else
            return null;
    }

    /**
     * Removes tke minimum key (and its corresponding value) from
     * this treap. This method does nothing if the treap is empty.
     * @return the value to which the minimum key had been mapped in
     * this treap, or <code>null</code> if the treap was empty.
     */
    public synchronized Object removeMin() {
        Tree t = tree, tprev;

        if (tree == null)
            return null;
        if (t.left == null)
            tree = t.right;
        else {
            do {
                tprev = t;
                t = t.left;
            } while (t.left != null);
            tprev.left = t.right;
        }
        size--;
        return t.value;
    }

    /**
     * Removes tke maximum key (and its corresponding value) from
     * this treap. This method does nothing if the treap is empty.
     * @return the value to which the maximum key had been mapped in
     * this treap, or <code>null</code> if the treap was empty.
     */
    public synchronized Object removeMax() {
        Tree t = tree, tprev;

        if (t == null)
            return null;
        if (t.right == null)
            tree = t.left;
        else {
            do {
                tprev = t;
                t = t.right;
            } while (t.right != null);
            tprev.right = t.left;
        }
        size--;
        return t.value;
    }

    /**
     * Clear the treap so that it contains no mappings.
     */
    public synchronized void clear() {
        tree = null;
        size = 0;
    }

    /**
     * Returns the number of keys in this treap.
     * @return 
     */
    public int size() {
        return size;
    }

   /**
    * Returns a string representation of this treap.
    * This routine is inefficient and primarily intended for
    * debugging. To access the elements in the treap in sorted order
    * use the <code>keys()</code> and <code>elements()</code> methods.
     * @return 
    * @see Treap#keys
    * @see Treap#elements
    */
   @Override
   public synchronized String toString() {
      StringBuilder strbuf = new StringBuilder();

      strbuf.append("{");
      if (tree != null)
         strbuf.append(tree.toString());
      if (strbuf.length() > 1)
         strbuf.setLength(strbuf.length() - 2);  // remove last ", "
      strbuf.append("}");
      return strbuf.toString();
   }

   /**
    * Creates a shallow copy of this treap.
    * The keys and the values themselves are not cloned.
    * @return a clone of this treap.
     * @throws java.lang.CloneNotSupportedException
    */
   @Override
   public synchronized Object clone() throws CloneNotSupportedException {
      try {
         Treap treap = (Treap) super.clone();

         treap.tree = (tree != null) ? (Tree) tree.clone() : null;
         return treap;
      } catch (CloneNotSupportedException e) {
         // Cannot happen
         throw new InternalError(e.toString());
      }
   }

   /* Inserts a node into tree and returns the updated treap */
   private Tree insert(Tree node, Tree tree) {
      if (tree == null) return node;
      int comp = node.key.compareTo(tree.key);
      if (comp < 0) {
         tree.left = insert(node, tree.left);
         if (tree.prio < tree.left.prio)
            tree = tree.rotateRight();
      } else if (comp > 0) {
         tree.right = insert(node, tree.right);
         if (tree.prio < tree.right.prio)
            tree = tree.rotateLeft();
      } else {
         keyFound = true;
         prevValue = tree.value;
         tree.value = node.value;
      }
      return tree;
   }

   /* Searches for a node with this key. If found, deletes this
    * node and returns the updated treap.
    */
   private Tree delete(Ordered key, Tree t) {
      if (t == null) return null;
      int comp = key.compareTo(t.key);
      if (comp < 0)
         t.left = delete(key, t.left);
      else if (comp > 0)
         t.right = delete(key, t.right);
      else {
         keyFound = true;
         prevValue = t.value;
         t = t.deleteRoot();
      }
      return t;
   }

   /**
    * Prints the treap on stderr, displaying the tree structure
    * and the priority numbers.
    */
   public synchronized void printDebug() {
      System.err.println("size: " + size);
      if (tree != null) tree.printDebug(0);
   }
   
   public synchronized void printStructure() {
      //System.err.println("size: " + size);
      if (tree != null) tree.printStructurePre(0);
   }
   
    public synchronized String getBPStructure() {
        if (tree != null) return tree.getBPStructure(0);
                else return "";
    }
    
    public synchronized void printKeys(FSDataOutputStream output) throws IOException {
        if (tree != null) tree.printKeys(0, output);
    }
    
    public synchronized void writeBinaryKeys(FSDataOutputStream output) throws IOException {
        if (tree != null) tree.writeBinaryKeys(0, output);
    }
    
    public synchronized void writeCompressKeys(FSDataOutputStream output) throws IOException {
        BlockCompress docIdBlock = new BlockCompress();
        if (tree != null) tree.writeCompressKeys(0, output, docIdBlock);
    }
    
    public synchronized void printPriorities(FSDataOutputStream output) throws IOException {
        if (tree != null) tree.printPriorities(0, output);
    }
    
    public synchronized void writeBinaryPriorities(FSDataOutputStream output) throws IOException {
        if (tree != null) tree.writeBinaryPriorities(0, output);
    }
    
    public synchronized void printBPStructure(FSDataOutputStream output) throws IOException {
        if (tree != null) tree.printBPStructure(0, output);
    }

    public void writeStructure(FSDataOutputStream indexFileStream) throws IOException {
        if (tree != null) tree.writeStructure(0, indexFileStream);
    }
    
    public void writeCompressStructure(FSDataOutputStream indexFileStream) throws IOException, BlockOutOfBoundsException {
        if (tree != null) tree.writeCompressStructure(0, indexFileStream);
    }

    public void writeBinaryBPStructure(FSDataOutputStream indexFileStream) throws IOException {
        if (tree != null) tree.writeBinaryBPStructure(0, indexFileStream);
    }
}

/* An object of this class represents a node in a binary tree */
class Tree implements Cloneable
{
   Ordered key;
   Integer value;
   Integer prio;
   Tree left, right;
   static String structureBlock = "";
   BlockCompress structureCompressBlock = new BlockCompress();

   /* Rotate this tree to the left
    */
   final Tree rotateLeft() {
      Tree temp = right;
      right = right.left;
      temp.left = this;
      return temp;
   }

   /* Rotate this tree to the right
    */
   final Tree rotateRight() {
      Tree temp = left;
      left = left.right;
      temp.right = this;
      return temp;
   }

   /* If one of the children is an empty tree, remove the root
    * and put the other child in its place. If both children
    * are nonempty, rotate the tree at the root so that the child
    * with the smallest prio-number comes to the top, then delete
    * the root from the other subtee.
    */
   final Tree deleteRoot() {
      Tree temp;
      
      if (left == null)
         return right;
      if (right == null)
         return left;
      if (left.prio < right.prio) {
         temp = rotateRight();
         temp.right = deleteRoot();
      } else {
         temp = rotateLeft();
         temp.left = deleteRoot();
      }
      return temp;
   }
   
   @Override
   public String toString() {
      StringBuilder strbuf = new StringBuilder();
      
      if (left != null)
         strbuf.append(left.toString());
      strbuf.append(key).append("=").append(value).append(", ");
      if (right != null)
         strbuf.append(right.toString());

      return strbuf.toString();
   }

   /* Creates a shallow copy of this tree.
    * The keys and the values themselves are not cloned.
    */
   @Override
   protected Object clone() throws CloneNotSupportedException {
      try {
         Tree tree = (Tree) super.clone();

         tree.left = (left != null) ? (Tree) left.clone() : null;
         tree.right = (right != null) ? (Tree) right.clone() : null;
         return tree;
      } catch (CloneNotSupportedException e) {
	// Cannot happen
         throw new InternalError(e.toString());
      }
   }

    /* Print in sorted order, displaying the tree structure
     * and the priority numbers.
     */
    void printDebug(int level) {
        if (left != null)
            left.printDebug(level +1 );
        for (int i = 0; i < level; i++)
            System.err.print("  ");
        System.err.println(key + "=" + value + " (" + prio + ")");
        if (right != null)
            right.printDebug(level + 1);
   }
   
    void printStructure(int level) {
        if (left != null)
            left.printStructure(level +1 );
        else
            System.err.print("(");
        if (right != null)
            right.printStructure(level + 1);
        System.err.print(")");
   }
    
    void printStructurePre(int level) {
        System.err.print("(");
        if (left != null)
            left.printStructurePre(level + 1);
        if (right != null)
            right.printStructurePre(level + 1);
        System.err.print(")");
    }
    
    void printStructureIn(int level) {
        if (left != null)
            left.printStructureIn(level + 1);
        System.err.print("(");
        if (right != null)
            right.printStructureIn(level + 1);
        System.err.print(")");
    }
    
    /**
     * This method return the pre-order balanced parenthesis representation
     * 
     * @param level
     * @return balanced parenthesis representation of the Treap
     */
    String getBPStructure(int level) {
        String bpRepresentation = "";
        bpRepresentation += "(";
        if (left != null)
            bpRepresentation += left.getBPStructure(level + 1);
        if (right != null)
            bpRepresentation += right.getBPStructure(level + 1);
        bpRepresentation += ")";
        return bpRepresentation;
    }
    
    /**
     * This method print the pre-order balanced parenthesis representation
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @return balanced parenthesis representation of the Treap
     */
    void printBPStructure(int level) {
        System.out.println("(");
        if (left != null)
            left.printBPStructure(level + 1);
        if (right != null)
            right.printBPStructure(level + 1);
        System.out.println(")");
    }
    
    /**
     * This method print the pre-order balanced parenthesis representation
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @return balanced parenthesis representation of the Treap
     */
    void printBPStructure(int level, FSDataOutputStream output) throws IOException {
        output.writeBytes("(");
        if (left != null)
            left.printBPStructure(level + 1, output);
        if (right != null)
            right.printBPStructure(level + 1, output);
        output.writeBytes(")");
    }
    
    /**
     * This method print the ordered string of keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @return balanced parenthesis representation of the Treap
     */
    void printKeys(int level, FSDataOutputStream output) throws IOException {
        if (left != null)
            left.printKeys(level + 1, output);
        else // Este caso es el primer item de la lista, no tiene separador
            output.writeBytes(key.toString());
        if (right != null)
            right.printKeys(level + 1, output, ",");
    }
    
    /**
     * This method print the ordered binary of keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     */
    void writeBinaryKeys(int level, FSDataOutputStream output) throws IOException {
        if (left != null)
            left.writeBinaryKeys(level + 1, output);
        else // Este caso es el primer item de la lista, no tiene separador
            output.writeInt(Integer.parseInt(key.toString()));
        if (right != null)
            right.writeBinaryKeys(level + 1, output);
    }
    
    /**
     * This method print the ordered string of keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @param separator The character separator among keys
     * @return balanced parenthesis representation of the Treap
     */
    void printKeys(int level, FSDataOutputStream output, String separator) throws IOException {
        if (left != null)
            left.printKeys(level + 1, output, separator);
        output.writeBytes(separator + key.toString());
        if (right != null)
            right.printKeys(level + 1, output, separator);
    }
    
    /**
     * This method print the ordered string of keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @return balanced parenthesis representation of the Treap
     */
    void printPriorities(int level, FSDataOutputStream output) throws IOException {
        if (left != null)
            left.printPriorities(level + 1, output);
        else // Este caso es el primer item de la lista, no tiene separador
            output.writeBytes(prio.toString());
        if (right != null)
            right.printPriorities(level + 1, output, ",");
    }
    
    /**
     * This method print the ordered string of keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @param separator The character separator among priorities
     * @return balanced parenthesis representation of the Treap
     */
    void printPriorities(int level, FSDataOutputStream output, String separator) throws IOException {
        if (left != null)
            left.printPriorities(level + 1, output, ",");
        output.writeBytes(separator + prio.toString());
        if (right != null)
            right.printPriorities(level + 1, output, ",");
    }
    
    /**
     * This method print the ordered string of compressed keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @return balanced parenthesis representation of the Treap
     */
    void writeCompressKeys(int level, FSDataOutputStream output, BlockCompress block) throws IOException {
        if (left != null)
            left.writeCompressKeys(level + 1, output, block);
        else // Este caso es el primer item de la lista, no tiene separador
            //output.writeBytes(key.toString());
        if (right != null)
            right.writeCompressKeys(level + 1, output, block, ",");
    }
    
    /**
     * This method print the ordered string of compressed keys
     * 
     * @param level
     * @param output The Output Stream to write the structure
     * @param separator The character separator among keys
     * @return balanced parenthesis representation of the Treap
     */
    void writeCompressKeys(int level, FSDataOutputStream output, BlockCompress block, String separator) throws IOException {
        if (left != null)
            left.writeCompressKeys(level + 1, output, block, separator);
        output.writeBytes(separator + key.toString());
        if (right != null)
            right.writeCompressKeys(level + 1, output, block, separator);
    }
    
    /**
     * 
     * "(" = 1
     * 
     * ")" = 0
     * 
     * @param level
     * @param indexFileStream 
     */
    void writeStructure(int level, FSDataOutputStream indexFileStream) throws IOException {
        if (structureBlock.length() == 63) {
            int encodeStructureBlock = Integer.parseInt(structureBlock, 2);
            indexFileStream.writeByte(encodeStructureBlock);
            indexFileStream.writeBytes(",");
            structureBlock = "";
        }
        structureBlock += "1";
        if (left != null)
            left.writeStructure(level + 1, indexFileStream);
        if (right != null)
            right.writeStructure(level + 1, indexFileStream);
        structureBlock += "0";
        if (level == 0) {
            int encodeStructureBlock = Integer.parseInt(structureBlock, 2);
            indexFileStream.writeBytes(String.valueOf(encodeStructureBlock));
            structureBlock = "";
        }
    }
    
    /**
     * 
     * "(" = 1
     * 
     * ")" = 0
     * 
     * @param level
     * @param indexFileStream 
     */
    void writeCompressStructure(int level, FSDataOutputStream indexFileStream) throws IOException, BlockOutOfBoundsException {
        if (structureBlock.length() == 63) {
            int encodeStructureBlock = Integer.parseInt(structureBlock, 2);
            try {
                this.structureCompressBlock.addPosting(encodeStructureBlock, null);
            } catch (BlockOutOfBoundsException e) {
                int[] compressDocIds = this.structureCompressBlock.getDocIds();
                // pseudo-header con la cantidad de elementos del bloque actual
                indexFileStream.writeInt(compressDocIds.length);
                for (int compressNumber : compressDocIds) {
                    indexFileStream.writeInt(compressNumber);
                }
                this.structureCompressBlock = new BlockCompress();
                this.structureCompressBlock.addPosting(encodeStructureBlock, null);
            }
            structureBlock = "";
        }
        structureBlock += "1";
        if (left != null)
            left.writeStructure(level + 1, indexFileStream);
        if (right != null)
            right.writeStructure(level + 1, indexFileStream);
        structureBlock += "0";
        // Maneja la ultima parte
        // Puede ocurrir que este numero se agregue sin problema al bloque, 
        //  entonces lo agrega, comprime y escribe a disco
        // Si no lo puede agregar, es porque el bloque llego al limite
        //  Debe escribir el viejo bloque, y generar el bloque con el nuevo
        //  elemento, comprimirlo y tambien escribirlo.
        if (level == 0) {
            int encodeStructureBlock = Integer.parseInt(structureBlock, 2);
            try {
                this.structureCompressBlock.addPosting(encodeStructureBlock, null);
            } catch (BlockOutOfBoundsException e) {
                int[] compressDocIds = this.structureCompressBlock.getDocIds();
                // pseudo-header con la cantidad de elementos del bloque actual
                indexFileStream.writeInt(compressDocIds.length);
                for (int elem : compressDocIds) {
                    indexFileStream.writeInt(elem);
                }
                this.structureCompressBlock = new BlockCompress();
                this.structureCompressBlock.addPosting(encodeStructureBlock, null);
            }
            // Ultimo bloque por escribir
            int[] compressDocIds = this.structureCompressBlock.getDocIds();
            // pseudo-header con la cantidad de elementos del bloque actual
            indexFileStream.writeInt(compressDocIds.length);
            for (int elem : this.structureCompressBlock.getDocIds()) {
                indexFileStream.writeInt(elem);
            }
            this.structureCompressBlock = new BlockCompress();
            structureBlock = "";
        }
    }

    void writeBinaryPriorities(int level, FSDataOutputStream output) throws IOException {
        if (left != null)
            left.writeBinaryPriorities(level + 1, output);
        else // Este caso es el primer item de la lista, no tiene separador
            output.writeInt(value);
        if (right != null)
            right.writeBinaryPriorities(level + 1, output);
    }

    void writeBinaryBPStructure(int level, FSDataOutputStream output) throws IOException {
        if (structureBlock.length() == 63) {
            int compressStructureBlock = Integer.parseInt(structureBlock, 2);
            Treap.lenghtBPnumbers++;
            output.writeInt(compressStructureBlock);
            structureBlock = "";
        }
        structureBlock += "1";
        if (left != null)
            left.writeBinaryBPStructure(level + 1, output);
        if (right != null)
            right.writeBinaryBPStructure(level + 1, output);
        structureBlock += "0";
        if (level == 0) {
            int compressStructureBlock = Integer.parseInt(structureBlock, 2);
            Treap.lenghtBPnumbers++;
            output.writeInt(compressStructureBlock);
            structureBlock = "";
        }
    }
    
}

/**
 * A treap enumeration class that returns the keys or elements
 * of a treap in sorted order.
 * This class should not be seen by the client.
 */
class TreapEnumeration implements Enumeration
{
   private Stack<Tree> stack;
   private boolean keys;
   private boolean ascending;

   TreapEnumeration(Tree t, boolean keys, boolean ascending) {
      stack = new Stack<Tree>();

      this.keys = keys;
      this.ascending = ascending;
      if (ascending)
         for ( ; t != null; t = t.left)
            stack.push(t);
      else // descending
         for ( ; t != null; t = t.right)
            stack.push(t);
   }

   @Override
   public boolean hasMoreElements() {
      return !stack.empty();
   }

   @Override
   public Object nextElement() {
      if (stack.empty())
         throw new NoSuchElementException("TreapEnumeration");

      Tree node = (Tree) stack.peek();
      if (ascending)
         if (node.right == null) {
            Tree t = (Tree) stack.pop();
            while (!stack.empty() &&
                   ((Tree) stack.peek()).right == t)
               t = (Tree) stack.pop();
         } else
            for (Tree t = node.right; t != null; t = t.left)
               stack.push(t);
      else // descending
         if (node.left == null) {
            Tree t = (Tree) stack.pop();
            while (!stack.empty() &&
                   ((Tree) stack.peek()).left == t)
               t = (Tree) stack.pop();
         } else
            for (Tree t = node.left; t != null; t = t.right)
               stack.push(t);

      return keys ? node.key : node.value;
   }
}