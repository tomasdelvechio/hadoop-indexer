package lib.org.order;

public class Key implements Ordered
{
    public int key;
    public Key(int y) { 
       key = y; 
    }
   
    @Override
    public int compareTo(Ordered y) {
        if (key > ((Key) y).key)
            return 1;
        else if (key < ((Key) y).key)
            return -1;
        else
            return 0;
    }
   
    @Override
    public String toString() {
        return Integer.toString(key);
    }
}