Trabajo Final de Licenciatura
=============================

Construcción de Índices para Recuperación de Información en contexto de Datos Masivos utilizando la plataforma Hadoop.

Implementación utilizando unicamente MapReduce sobre YARN (Hadoop versión 2)

Este repositorio contiene:

 * [El codigo completo de los algoritmos propuestos en el trabajo](https://gitlab.com/tomasdelvechio/hadoop-indexer/tree/master/src/com/tomasdel/tesis/indexbuilder). Es el codigo ejecutado en el cluster y con el cual se generaron los resultados analizados.
 * Codigo de ejemplo MapReduce y YARN.
 * [Los fuentes en Latex](https://gitlab.com/tomasdelvechio/hadoop-indexer/tree/master/doc/informe) del informe presentado y aprobado por el jurado evaluador.
 * [Scripts](https://gitlab.com/tomasdelvechio/hadoop-indexer/tree/master/scripts) utilizados para el procesamiento de logs y generacion de resultados y graficos.

El informe completo se puede encontrar [en linea](http://www.labredes.unlu.edu.ar/sites/www.labredes.unlu.edu.ar/files/site/data/TFL-TomasDelvechio-VersionFinal.pdf).